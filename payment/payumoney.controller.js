const payuMoneyService = require('./payumoney.service');

exports.generatePayUMoneyHash = (req, res) => {
    let data = {};
    let body = req.body;
    data['productinfo'] = body['productinfo'];
    data['amount'] = body['amount'];
    data['email'] = body['email'];
    data['phone'] = body['mobile'];
    data['lastname'] = body['last_name'];
    data['firstname'] = body['first_name'];
    payuMoneyService.createPaymentHash(data).then(response => {
        resObj = {
            status:true,
            "response":{
                "data": response,
                "message":"Successfully Hash generated"
            }
        };
        res.send(resObj);
    })
}