const paymentService = require('./payment.service');

exports.generateRazorpay = (req, res) => {
    let resObj;
    let cartId=req.body.cart_id;
    let grandTotal=req.body.grand_total;
    paymentService.createRazorpay(cartId,grandTotal).then(data => {
        resObj = {
        status: true,
            response: {
                data: data,
                message: "Successfully Razorpay generate",
            },
        };
        res.send(resObj);
    }).catch((err) => {
        resObj = {
        status: false,
            response: {
                message: err.message,
            },
        };
        res.status(400).send(resObj);
    });
}