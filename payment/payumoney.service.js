const payumoney = require('payumoney-node');
const payumoney_config = require('../config/payment.config.json').payumoney;
const crypto = require('crypto');

exports.makePayment = (paymentData) => {
    return new Promise((resolve, reject) => {
        paymentData['txnid'] = new Date().getTime();
        payumoney.makePayment(paymentData, (error, response) => {
            if (error) {
              reject("payment error");
            } else {
              // Payment redirection link
              console.log(response);
              resolve(response);
            }
          });
    })
}

exports.createPaymentHash = (data) => {
  return new Promise((resolve, reject) => {
      data['order_id'] = new Date().getTime();
      data['productinfo'] = 'yummafood';
      if(!data['udf5']) {
        data['udf5'] = '';
      }
      var cryp = crypto.createHash('sha512');
		  var text = payumoney_config.MERCHANT_KEY+'|'+data.order_id+'|'+data.amount+'|'+data.productinfo+'|'+data.firstname+'|'+data.email+'|||||'+data.udf5+'||||||'+payumoney_config.MERCHANT_SALT;
		  cryp.update(text);
      var hash = cryp.digest('hex');
      data['hash'] = hash;
      data['merchant_key'] = payumoney_config.MERCHANT_KEY;
      data['merchant_salt'] = payumoney_config.MERCHANT_SALT;
      resolve(data);
  })
}

exports.doRefund = (paymentId, amount) => {
  payumoney.refundPayment("paymentId", "amount", function(error, response) {
    if (error) {
      // Some error
    } else {
      console.log(response);
    }
  });
}