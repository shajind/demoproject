const model = require("../models");
const YummaCommissionModel = model.YummaCommission;


exports.saveYummaCommission = (data) => {
    return new Promise((resolve, reject) => {
        YummaCommissionModel.create(data).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });
};

exports.findAllYummaCommission = () => {
    return new Promise((resolve, reject) => {
        YummaCommissionModel.findAll().then((res) => {
            resolve(res);
        }).catch((err) => {
            reject(err);
        });
    });
};
