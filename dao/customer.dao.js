const model = require("../models");
const Customer = model.customers;
const User = model.users;
const State = model.state;
const City = model.city;

exports.findCustomerByUserId = (userId) => {
  return new Promise((resolve, reject) => {
    Customer.findOne({ where: { user_id: userId } }).then((res) => {
      if(!response){
        throw new Error("Customer is not valid");
      }
      resolve(res);
    }).catch((err) => {
      reject(err);
    });
  });
};

exports.fetchCustomer = (customerId) => {
  return findCustomerById(customerId);
};

function findCustomerById(customerId) {
  return new Promise((resolve, reject) => {
    Customer.findOne({ where: { cust_id: customerId} })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
}

exports.updateCustomer = (customerId, data) => {
    return new Promise((resolve, reject) => {
      Customer.update(data, { where: { cust_id: customerId } }).then(res => {
        return findCustomerById(customerId);
      }).then(data => {
        resolve(data);
      })
      .catch(err => {
        reject(err);
      })
    });
};

exports.saveCustomer = (data) => {
  return new Promise((resolve, reject) => {
    Customer.create(data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  });
};

exports.findCustomerByUserId = (userId) => {
    return new Promise((resolve, reject) => {
        Customer.findOne({ where: { user_id: userId },include: [{model: User,
        as: "usersData",attributes: ["user_email","user_mobile","status"]},
        {model: State,as: "stateData"},{model: City,as: "cityData"}] }).then((data) => {
          console.log((data));
          resolve(data);
        },
        (err) => {
          reject({ error: err });
        }
      );
    });
};