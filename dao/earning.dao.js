const model = require("../models");
const ChefEarning = model.ChefEarning;
const weekDate = model.WeekDate;
const ChefEarningHistory = model.ChefEarningHistory;
const sequelize = require('sequelize');
const {Op} = require('sequelize');
const async = require('async');

//SELECT sum(`item_earned_amount`) AS `total_amount` FROM `chef_earning_history` AS `chef_earning_history` 
//GROUP BY MONTH('month', `createdAt`), WEEK('week', `createdAt`);

//GROUP BY YEAR(t.summaryDateTime), MONTH(t.summaryDateTime);
exports.sumTotalEarnings = () => {    
    return new Promise((resolve, reject) => {
        ChefEarningHistory.findAll({
            attributes: [
                //[ sequelize.fn('date', sequelize.col('createdAt')), 'date'],
                //[ sequelize.fn('week', sequelize.col('createdAt')), 'weekly'],
                //[ sequelize.fn('month', sequelize.col('createdAt')), 'monthly'],
                [ sequelize.fn('year', sequelize.col('createdAt')), 'year'],
                [ sequelize.fn('count', sequelize.col('createdAt')), 'count'],
                [ sequelize.fn('sum', sequelize.col('item_earned_amount')), 'total_amount']
            ],
            group: [
                //[sequelize.fn('DATE', sequelize.col('createdAt'))],
                //[sequelize.fn('WEEK', sequelize.col('createdAt')),'week'],
                //[sequelize.fn('MONTH', sequelize.col('createdAt')),'montly'],
                [sequelize.fn('YEAR', sequelize.col('createdAt'))]
            ],
            raw: true
        }).then((data) => {
            console.log('source is ' + data);
            resolve(data);
        }).catch((err) => {
            console.log('error i is ' + err);
            reject(err);
        })
    });
};

exports.sumTotalEarningInDate = (earnedID) => {    
    return new Promise((resolve, reject) => {
        ChefEarningHistory.findAll({where:{earned_id:earnedID},
            attributes: [
                [ sequelize.fn('date', sequelize.col('createdAt')), 'date'],
                [ sequelize.fn('count', sequelize.col('createdAt')), 'count'],
                [ sequelize.fn('sum', sequelize.col('item_earned_amount')), 'total_amount']
            ],
            group: [
                [sequelize.fn('DATE', sequelize.col('createdAt'))]
            ],
            raw: true
        }).then((data) => {
            console.log('source is ' + data);
            resolve(data);
        }).catch((err) => {
            console.log('error i is ' + err);
            reject(err);
        })
    });
};


exports.sumTotalEarningInWeek = (earnedID) => {
    return new Promise((resolve, reject) => {
        weekDate.findAll({}, {raw: true}).then((response) => {
            let weekCountData = [];
            async.forEach(response, function (item, callback){ 
                //console.log(item); // print the key
                let weekNo = item['weekNum'];
                ChefEarningHistory.findOne({where:{earned_id:earnedID, week_no: weekNo},
                    attributes: [
                        [ sequelize.fn('sum', sequelize.col('item_earned_amount')), 'total_amount'],
                        [ sequelize.fn('count', sequelize.col('createdAt')), 'count'],

                    ],
                    raw: true
                }).then(data => {
                    if(data['total_amount']){
                        result={};
                        result['weekNo']=item['weekNum'];
                        result['fromDate']=item['fromDate'];
                        result['toDate']=item['toDate'];
                        result['label']=new Date(item['fromDate']).toString().split(' ').splice(1,3).join(' ')+" - "+new Date(item['toDate']).toString().split(' ').splice(1,3).join(' ');
                        result['count']=data['count'];
                        result['total_amount']=data['total_amount'];
                        weekCountData.push(result);
                    }                                        
                    // tell async that that particular element of the iterator is done
                    callback();
                })
            }, function(err) {
                //console.log('iterating done');
                resolve(weekCountData);
            });  
        }).catch(err => {
            reject(err);
        });
    });
};

exports.sumTotalEarningInMontly = (earnedID) => {
    return new Promise((resolve, reject) => {
        ChefEarningHistory.findAll({where:{earned_id:earnedID},
            attributes: [
                [ sequelize.fn('month', sequelize.col('createdAt')), 'monthly'],
                [ sequelize.fn('year', sequelize.col('createdAt')), 'year'],
                [ sequelize.fn('count', sequelize.col('createdAt')), 'count'],
                [ sequelize.fn('sum', sequelize.col('item_earned_amount')), 'total_amount']
            ],
            group: [
                [sequelize.fn('MONTH', sequelize.col('createdAt')),'montly'],
                [sequelize.fn('YEAR', sequelize.col('createdAt')),'year']
            ],
            raw: true
        }).then((data) => {
            if(data){
                data.map((row) => {
                    //var monthStartDay = new Date(2021, 5, 1);
                    //var monthEndDay = new Date(2021, 5 + 1, 0);
                    row['fromDate']=new Date(row.year, row.monthly-1, 1);
                    row['toDate']=new Date(row.year, row.monthly, 0);
                    let fromDate=new Date(row['fromDate']).toString().split(' ').splice(1,3).join(' ');
                    let toDate=new Date(row['toDate']).toString().split(' ').splice(1,3).join(' ');

                    row['label']=fromDate+" - "+toDate;
                   
                });
            }
            resolve(data);
        }).catch((err) => {
            reject(err);
        })
    });
};

exports.sumTotalEarningInYear = (earnedID) => {
    return new Promise((resolve, reject) => {
        ChefEarningHistory.findAll({where:{earned_id:earnedID},
            attributes: [
                [ sequelize.fn('year', sequelize.col('createdAt')), 'year'],
                [ sequelize.fn('count', sequelize.col('createdAt')), 'count'],
                [ sequelize.fn('sum', sequelize.col('item_earned_amount')), 'total_amount']
            ],
            group: [
                [sequelize.fn('YEAR', sequelize.col('createdAt')), 'year']
            ],
            raw: true
        }).then((data) => {
            if(data){
                data.map((row) => {
                    row['fromDate']=new Date(row.year,0);
                    let year=row['fromDate'].getFullYear();
                    if(row['year']==year){
                        row['toDate']=new Date();
                    }else{
                        row['toDate']=new Date(row.year, 12, 0);
                    }
                    let fromDate=new Date(row['fromDate']).toString().split(' ').splice(1,3).join(' ');
                    let toDate=new Date(row['toDate']).toString().split(' ').splice(1,3).join(' ');
                    row['label']=fromDate+" - "+toDate;                   
                });
            }
            resolve(data);
        }).catch((err) => {
            reject(err);
        })
    });
};

exports.sumTotalEarning = (earnedID) => {
    return new Promise((resolve, reject) => {
        ChefEarningHistory.findAll({where:{earned_id:earnedID},
            attributes: [
                [ sequelize.fn('count', sequelize.col('createdAt')), 'count'],
                [ sequelize.fn('sum', sequelize.col('item_earned_amount')), 'total_amount']
            ],            
            raw: true
        }).then((data) => {
            resolve(data[0]);
        }).catch((err) => {
            reject(err);
        })
    });
};


exports.findEarningChef = (chefId) => {
    return new Promise((resolve, reject) => {
        ChefEarning.findOne({where: {chef_id: chefId}}).then((response) => {
            if (response) {
                resolve(response);
                return;
            }
            let payload = {};
            payload['chef_id'] = chefId;
            return ChefEarning.create(payload);
        }).then(data => {
            resolve(data);
        }).catch(err => {
            reject(err);
        });
    });
};

exports.fetchEarningChef = (chefId) => {
    return new Promise((resolve, reject) => {
        ChefEarning.findOne({where: {chef_id: chefId}}).then((response) => {
            if (!response) {
                throw new Error("This chef no earning list");
            }
            resolve(response);
        }).catch(err => {
            reject(err);
        });
    });
};

exports.bulkCreateEarningChef = (payload) => {
    return new Promise((resolve, reject) => {
        ChefEarningHistory.bulkCreate(payload).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

exports.createWeekDate = (payload) => {
    return new Promise((resolve, reject) => {
        weekDate.bulkCreate(payload).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}


exports.fetchweek = (weekNum) => {
    return new Promise((resolve, reject) => {
        weekDate.findOne({where: {weekNum: weekNum}}).then((response) => {
            if (!response) {
                throw new Error("This week not valid");
            }
            resolve(response);
        }).catch(err => {
            reject(err);
        });
    });
};

exports.fetchChefEarningWeekHistory = (startDate,endDate,earnedId) => {    
    return new Promise((resolve, reject) => {
        ChefEarningHistory.findAll({where:{ earned_id: earnedId,
            createdAt:{[Op.gte]:startDate,[Op.lte]:endDate}
        }})
        .then((res) => {
            resolve(res);
        })
        .catch((err) => {
            reject(err);
        });
    });
};