const model = require('../models');
const UserType = model.user_role;

exports.createUserType = (user_type_data) => {    
    return new Promise((resolve, reject) => {
        UserType.create(user_type_data).then(response => {                  
            console.log(response);
        	resolve(response);
        }).catch((err) => {
            console.log("saveUser_Type");
            console.log(err);                  
        	reject({error:err});
        });
    });
};

exports.findAllUserTypes = () =>{    
    return new Promise((resolve,reject) => {               
        UserType.findAll().then(data =>{
            console.log(data);            
            resolve(data);            
        },err => {                    
        	reject({error:err});
        });
    });
};

exports.findUserTypeByCode = (code) =>{    
    return new Promise((resolve,reject) => {               
        UserType.findOne({where:{user_type_code:code}}).then(data =>{
            console.log("user_type by code ================", data['user_type_id']);          
            resolve(data);            
        },err => {                    
        	reject({error:err});
        });
    });
};

exports.findUserType = (userTypeId) => {    
    return new Promise((resolve,reject) => {               
        userType.findOne({where:{user_type_id:userTypeId}}).then(data =>{         
            resolve(data);            
        },err => {                    
        	reject({error:err});
        });
    });
};