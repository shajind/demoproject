const model = require('../models');
const OrderStatus = model.OrderStatus;

exports.createOrderStatus = (payload) =>{    
    return new Promise((resolve,reject) => {               
        OrderStatus.create(payload).then(data =>{         
            resolve(data);            
        }).catch(err => {           
        	reject(err);
        });
    });
};

exports.getAllOrderStatus = () => {    
    return new Promise((resolve,reject) => {               
        OrderStatus.findAll().then(data =>{         
            resolve(data);            
        }).catch(err => {                    
        	reject({error:err});
        });
    });
};