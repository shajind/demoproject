const model = require('../models');
const couponModel = model.coupons;


exports.createCoupon = (coupondata) => {    
    return new Promise((resolve, reject) => {        
        couponModel.create(coupondata).then(response => {
        	resolve(response);
        },err => {                    
        	reject({error:err});
        });
    });
};

exports.getCoupon = () =>{    
    return new Promise((resolve,reject) => {               
        couponModel.findAll().then(data =>{
            resolve(data);            
        },err => {                    
        	reject({error:err});
        });
    });
};


exports.fetchCouponCode = (coupon_code) =>{    
    return new Promise((resolve,reject) => {               
        couponModel.findOne({where:{offer_code:coupon_code ,offer_status:"active"}}).then(data =>{           
            resolve(data);            
        },err => {                    
        	reject({error:err});
        });
    });
};