const model = require('../models');
const Role = model.role;

exports.findRoleByCode = (code) =>{    
    return new Promise((resolve,reject) => {               
        Role.findOne({where:{role_code:code}}).then(data =>{
            if(!data) {
                throw new Error("Role not found");
            }
            console.log("user_type by code ================", data['role_id']);          
            resolve(data);            
        }).catch(err => {           
        	reject(err);
        });
    });
};

exports.findRoleById = (roleId) => {    
    return new Promise((resolve,reject) => {               
        Role.findOne({where:{role_id: roleId}}).then(data =>{
            resolve(data);            
        },err => {                    
        	reject(err);
        });
    });
};

exports.createRole = (data) => {    
    return new Promise((resolve,reject) => {               
        Role.create(data).then(res =>{         
            resolve(res);            
        },err => {                    
        	reject(err);
        });
    });
};

exports.findAll = () => {    
    return new Promise((resolve,reject) => {               
        Role.findAll().then(data =>{         
            resolve(data);            
        },err => {                    
        	reject(err);
        });
    });
};