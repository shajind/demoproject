const model = require("../models");
const Cart = model.Cart;
const CartItem = model.CartItem;
const CartCustomer = model.CartCustomer;
const CartAvailability = model.cartAvailability;
const CartDeliveryTimeSlot = model.CartDeliveryTimeSlot;
const CartAddonModel = model.CartAddon;



exports.createOrGetCart = (payload, customer_id) => {
  return new Promise((resolve, reject) => {
    Cart.findOne({ where: { isActive: true, customerId: customer_id } })
      .then((response) => {
        if (response) {
          resolve(response);
          return;
        }
        let cart = {};
        cart['customerId'] = customer_id;
        cart['deliveryDate'] = payload['delivery_date'];
        cart['delivery_time_slot'] = payload['delivery_time_slot'];
        return Cart.create(cart);
      })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

exports.createCart = (payload, customer_id) => {
  return new Promise((resolve, reject) => {
    let cart = {};
    cart['customerId'] = customer_id;
    cart['deliveryDate'] = payload['delivery_date'];
    cart['delivery_time_slot'] = payload['delivery_time_slot'];
    Cart.create(cart).then((res) => {
        resolve(res);
      }).catch((err) => {
        reject(err);
      });
  });
};

exports.findById = (cartId) => {
  return new Promise((resolve, reject) => {
    Cart.findOne({ where: { cartId: cartId} }).then((res) => {      
      if(res == null) {
        throw new Error("Cart not found");
      }
      resolve(res);
    }).catch((err) => {
      reject(err);
    });
  });
};

exports.deactivateCart = (cart_id) => {
    return new Promise((resolve, reject) => {
      Cart.findOne({ where: { cartId : cart_id} })
          .then((res) => {
            res['isActive'] = false;
            return Cart.update(res.dataValues, { where: { cartId: cart_id } })
          }).then(result => {
            resolve(result);
          })
          .catch((err) => {
            console.log(err);
            reject(err);
          });
      });
};

exports.saveCart = (data) => {
  return new Promise((resolve, reject) => {
    Cart.create(data, { include: [ db.CartItem ]})
        .then((cart) => {
          resolve(cart);
        })
        .catch((err) => {
          reject(err);
        });
    });
};

exports.saveCartItems = (data) => {
  return new Promise((resolve, reject) => {
    CartItem.createBulk(data)
        .then((res) => {
          resolve(res);
        }).catch((err) => {
          reject(err);
        });
    });
};

exports.updateCartItems = (cartitem_id,data) => {
  return new Promise((resolve, reject) => {
    CartItem.update(data,{where: {cartItemId: cartitem_id}}).then((res) => {
      resolve(res);
    }).catch((err) => {
      reject(err);
    });
  });
};

exports.updateCart = (cartId, data) => {
  return new Promise((resolve, reject) => {
    Cart.update(data, {where: {cartId: cartId}}).then((cart) => {
      return Cart.findOne({where: {cartId: cartId},
        include: [
          {model: CartItem,as: 'cart_items', include: ['cart_addon']},
          'cart_customer']})
    }).then(data => {
      resolve(data);
    }).catch((err) => {
      reject(err);
    });
  });
};

exports.findCartById = (cart_id) => {
  return new Promise((resolve, reject) => {
    Cart.findOne({where: {cartId: cart_id}, include: ['cart_items',"cart_addon"]})
        .then((cart) => {
          if(!cart) {
            throw new Error("Cart not found");
          }
          resolve(cart);
        })
        .catch((err) => {
          reject(err);
        });
    });
};

exports.findCustomerActiveCart = (cart_id) => {
  return new Promise((resolve, reject) => {
    Cart.findOne({where: {cartId: cart_id, isActive: true}, include: ['cart_items','cart_customer']})
    .then((cart) => {
      if(!cart){
        throw new Error("Cart is Empty");
      }
      resolve(cart);
    })
    .catch((err) => {
      reject(err);
    });
  });
};

exports.findActiveCartList = (customerId,cart_id) => {
  let whereClause;
  if(cart_id){
    whereClause={customerId: customerId,cartId:cart_id, isActive: true};
  }else{
    whereClause={customerId: customerId, isActive: true};
  }
  return new Promise((resolve, reject) => {
    Cart.findAll({where:whereClause , include: [{model: CartItem,as: 'cart_items', 
      include: ['cart_addon']},'cart_customer']})
        .then((data) => {
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
};

exports.findCartItemByCartIdAndMenuId = (whereClause) => {
  return new Promise((resolve, reject) => {
    CartItem.findOne({where: whereClause})
        .then((cart) => {
          resolve(cart);
        })
        .catch((err) => {
          reject(err);
        });
    });
};

exports.saveCartCustomer = (cartId, customer) => {
  return new Promise((resolve, reject) => {
    CartCustomer.findOne({where: {cart_id:cartId}})
    .then(cart => {
      if(!cart){
        return CartCustomer.create(customer)
      }else{
        return cart;
      }
    })
    //CartCustomer.create(customer)
    .then((data) => {
      console.log(data)
        resolve(data)
    }).catch((err) => {
      reject(err);
    });
  });
};

exports.findCartItemById = (cartItemId) => {
  return new Promise((resolve, reject) => {
    findCartItemByCartItemId(cartItemId).then((cart) => {
      resolve(cart);
    }).catch((err) => {
      reject(err);
    });
  });
}

function findCartItemByCartItemId(cartItemId) {
  return new Promise((resolve, reject) => {
    CartItem.findOne({where: {cartItemId: cartItemId}})
        .then((cart) => {
          resolve(cart);
        })
        .catch((err) => {
          reject(err);
        });
    });
}

exports.updateCartItem = (item_id, data) => {
  return new Promise((resolve, reject) => {
    CartItem.update(data, {where: {cartItemId: item_id}})
        .then((cart) => {
          return findCartItemByCartItemId(item_id);
        }).then(data => {
          resolve(data);
        }).catch((err) => {
          reject(err);
        });
    });
};

exports.createCartItem = (data) => {
  return new Promise((resolve, reject) => {
    CartItem.create(data)
        .then((cartItem) => {
          return findCartItemByCartItemId(cartItem['cartItemId']);
        }).then(data => {
          resolve(data);
        }).catch((err) => {
          reject(err);
        });
    });
};

exports.deleteCartItem = (cartItem_id,cart_id) => {
  return new Promise((resolve, reject) => {
    CartItem.destroy({where: {cartId: cart_id,cartItemId:cartItem_id}})
        .then((data) => {
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
};

exports.deleteCart = (cart_id) => {
  return new Promise((resolve, reject) => {
    Cart.destroy({where: {cartId: cart_id}})
        .then((data) => {
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
};

exports.saveCartAvailability = (data) => {
  return new Promise((resolve, reject) => {
    CartAvailability.bulkCreate(data).then((res) => {
      resolve(res);
    }).catch((err) => {
      reject(err);
    });
  });
};

exports.findCartAvailability = (cart_id) => {
  return new Promise((resolve, reject) => {
    CartAvailability.findAll({where: {cartId: cart_id}}).then((res) => {
      resolve(res);
    }).catch((err) => {
      reject(err);
    });
  });
};

exports.saveCartDeliveryTimeSlot = (data) => {
  return new Promise((resolve, reject) => {
    CartDeliveryTimeSlot.bulkCreate(data).then((res) => {
      resolve(res);
    }).catch((err) => {
      reject(err);
      });
  });
};

exports.findCartDeliveryTimeSlot = (cart_id) => {
  return new Promise((resolve, reject) => {
    CartDeliveryTimeSlot.findAll({where: {cartId: cart_id}}).then((res) => {
      resolve(res);
    }).catch((err) => {
      reject(err);
      });
  });
};

exports.createEmptyCart = (cartType, customer_id) => {
  return new Promise((resolve, reject) => {
    Cart.findOne({ where: { isActive: true, customerId: customer_id, cartType: 'checkout' } })
      .then(response => {
        let cart = {};
        cart['customerId'] = customer_id;
        cart['cartType'] = cartType;
        cart['delivery_charge'] = 39;
        if(cartType == 'order_now') {          
          return Cart.create(cart);
        }else if(response) {
          resolve(response);
          return;
        }        
        return Cart.create(cart);
    }).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    });
  });
};

exports.createCartAddons = (data) => {
  return new Promise((resolve, reject) => {
    CartAddonModel.bulkCreate(data).then((res) => {
      resolve(res);
    }).catch((err) => {
      reject(err);
      });
  });
};

exports.deleteCartItemAddons = (cart_item_id,cart_id) => {
  return new Promise((resolve, reject) => {
    CartAddonModel.destroy({where: {cartItemId: cart_item_id,cartId:cart_id}}).then((res) => {
      resolve(res);
    }).catch((err) => {
      reject(err);
      });
  });
};


exports.deleteCartItemAddonByCartAddonId = (cartAddonId,cart_id) => {
  return new Promise((resolve, reject) => {
    CartAddonModel.destroy({where: {cart_addon_id: cartAddonId,cartId:cart_id}}).then((res) => {
      resolve(res);
    }).catch((err) => {
      reject(err);
      });
  });
};


exports.findAllCartAddons = (cart_item_id,) => {
  return new Promise((resolve, reject) => {
    CartAddonModel.findAll({where: {cartItemId: cart_item_id}}).then((res) => {
      resolve(res);
    }).catch((err) => {
      reject(err);
      });
  });
};
