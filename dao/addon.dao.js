const model = require("../models");
const AddonModel = model.addon;
const MenuItemAddonModel = model.menu_items_addon;


exports.saveAddon = (data) => {
    return new Promise((resolve, reject) => {
        AddonModel.create(data).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });
};

exports.findAllAddon = () => {
    return new Promise((resolve, reject) => {
        AddonModel.findAll().then((res) => {
            resolve(res);
        }).catch((err) => {
            reject(err);
        });
    });
};

exports.findAllAddonByAddonId = (addonIds) => {
    return new Promise((resolve, reject) => {
        AddonModel.findAll({where:{addon_id:addonIds}}).then((res) => {
            resolve(res);
        }).catch((err) => {
            reject(err);
        });
    });
};


exports.findMenuItemAddon = (menuItemId) => {
    return new Promise((resolve, reject) => {
        MenuItemAddonModel.findAll({where:{menu_item_id:menuItemId},
            include:[{model: AddonModel,as: 'addon'}],}).then((res) => {
            console.log(res)
            resolve(res);
        }).catch((err) => {
            reject(err);
        });
    });
};

 