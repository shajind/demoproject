const model = require("../models");
const Chef = model.chefs;
const User = model.users;
const State = model.state;
const City = model.city;
const userModel = model.users;
const menuItem = model.menu_items;
const cityModel = model.city;
const MenuItemAvailable=model.menu_item_available;
const MenuItemDeliveryTimeSlotMaster = model.deliveryTimeSlotMaster;

exports.saveChef = (data) => {
    return new Promise((resolve, reject) => {
        Chef.create(data).then((response) => {
            resolve(response);
        }).catch(err => {
            reject(err);
        });
    });
};

exports.findChef = (userId) => {
    return new Promise((resolve, reject) => {
        Chef.findOne({where: {user_id: userId}}).then((response) => {
            if(!response){
                throw new Error("chef_id is not valid");
            }
            resolve(response);
        }).catch(err => {
            reject(err);
        });
    });
};

exports.findChefById = (chefId) => {
    return new Promise((resolve, reject) => {
        Chef.findOne({where: {chef_id: chefId}}).then((response) => {
            resolve(response);
        }).catch(err => {
            reject(err);
        });
    });
};

function findChefByChefId (chefId) {
    return new Promise((resolve, reject) => {
        Chef.findOne({where: {chef_id: chefId}, include: [{model: User,
            as: "usersData",attributes: ["user_id","user_name","user_email",
            "user_mobile","status"]},{model: State,as: "stateData"},
            {model: City,as: "cityData"}]}).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

function updateChefProfile(chefId, data) {
    return new Promise((resolve, reject) => {
        Chef.update(data,{ where: { chef_id: chefId } }).then((res) => {
            return findChefByChefId(chefId);
        }).then(data => {
            resolve(data);
        }).catch((err) => {
            reject(err);
        });
    });
}

exports.update = (chefId, data) => {
    return updateChefProfile(chefId, data);
};

exports.updateProfile = (chefId, data) => {
    return new Promise((resolve, reject) => {
        Chef.findOne({where: {chef_id: chefId}}).then((response) => {
            let chef = response.dataValues;
            if(data['chef_fullname']) {
                chef['chef_fullname'] = data['chef_fullname'];
            }
            if(data['chef_dob']) {
                chef['chef_dob'] = data['chef_dob'];
            }
            if(data['chef_gender']) {
                chef['chef_gender'] = data['chef_gender'];
            }
            if(data['chef_profile_pic']) {
                chef['chef_profile_pic'] = data['chef_profile_pic'];
            }
            if(data['chef_profile_cover_pic']) {
                chef['chef_profile_cover_pic'] = data['chef_profile_cover_pic'];
            }
            if(data['chef_address1']) {
                chef['chef_address1'] = data['chef_address1'];
            }
            if(data['chef_address2']) {
                chef['chef_address2'] = data['chef_address2'];
            }
            if(data['chef_zipcode']) {
                chef['chef_zipcode'] = data['chef_zipcode'];
            }
            if(data['chef_kitchen_timing']) {
                chef['chef_kitchen_timing'] = data['chef_kitchen_timing'];
            }
            if(data['chef_about_yourself']) {
                chef['chef_about_yourself'] = data['chef_about_yourself'];
            }
            if(data['chef_pure_veg_kitchen']) {
                chef['chef_pure_veg_kitchen'] = data['chef_pure_veg_kitchen'];
            }
            if(data['chef_lat']) {
                chef['chef_lat'] = data['chef_lat'];
            }
            if(data['chef_lang']) {
                chef['chef_lang'] = data['chef_lang'];
            }
            return updateChefProfile(chefId, chef);
        }).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    })
}

exports.findAllChef = (whereClauseItem,whereClause,array,availability,deliverySlot) => {
    return new Promise((resolve, reject) => {
        Chef.findAll({where:whereClause,["order"]:array,include: [{model: userModel,as: "usersData",
            attributes: ["user_id","user_name","user_email","user_mobile","status"]},
            {model: State,as: "stateData"},
            {model: menuItem,as: "menu_items",where:whereClauseItem,
            include:[{model: MenuItemAvailable,as: 'availability',where:availability},
            {model:MenuItemDeliveryTimeSlotMaster,where:deliverySlot},"varities","sub_category",],
            raw: true},
            {model: cityModel,as: "cityData"}]})
        .then(res=> {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    })
}