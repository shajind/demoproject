const model = require("../models");
const Tax = model.Tax;


exports.saveTax = (data) => {
    return new Promise((resolve, reject) => {
    Tax.create(data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  });
};

exports.findAllTax = () => {
  return new Promise((resolve, reject) => {
    Tax.findAll().then((res) => {
          resolve(res);
      }).catch((err) => {
          reject(err);
      });
  });
};

exports.fetchTax = (taxCode) => {
  return new Promise((resolve, reject) => {
    Tax.findOne({where:{is_active:true,tax_code: taxCode}}).then((res) => {
        resolve(res);
    }).catch((err) => {
        reject(err);
    });
  });
};
