const model = require("../models");
const Course = model.course;

exports.saveCourse = (data) => {
    return new Promise((resolve, reject) => {
        Course.create(data).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });
};


exports.findAllCourse = () => {
    return new Promise((resolve, reject) => {
        Course.findAll().then((res) => {
            resolve(res);
        }).catch((err) => {
            console.log(err);
            reject(err);
        });
    });
};






