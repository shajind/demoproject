const model = require("../models");
const CustomerCurrentLocation = model.CustomerCurrentLocation;

exports.saveCustomerCurrentLocation = (data) => {
  return new Promise((resolve, reject) => {
    CustomerCurrentLocation.create(data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  });
};

exports.updateCustomerCurrentLocation = (data,customerId) => {
    return new Promise((resolve, reject) => {
        CustomerCurrentLocation.update(data,{ where: { customer_id: customerId }})
        .then(res => {
            return fetchCustomerCurrentLocationByCustomerId(customerId);
        }).then(response => {
            resolve(response);
        }).catch(err => {
            reject(err);
        })
    });
};

exports.getCustomerCurrentLocationByCustomerId = (customerId) => {
    return fetchCustomerCurrentLocationByCustomerId(customerId);
};

function fetchCustomerCurrentLocationByCustomerId(customerId) {
    return new Promise((resolve, reject) => {
        CustomerCurrentLocation.findOne({ where: { customer_id: customerId } }).then((res) => {
            resolve(res);
        }).catch((err) => {
            console.log(err);
            reject(err);
        });
    });
};




