const model = require("../models");
const MenuItem = model.menu_items;
const Inclusion = model.MenuItemInclusion;
const Course = model.course;
const MenuItemDeliveryTimeSlotMaster = model.deliveryTimeSlotMaster;
const MenuItemCategory = model.MenuItemCategory;
const AddonModel = model.addon;
const MenuItemAvailable=model.menu_item_available;
const MenuItemDeliveryTimeSlot = model.MenuItemDeliveryTimeSlot;
const kitchenType=model.kitchen_type;
const MenuType = model.menu_types;
const MenuFoodTypeModel = model.MenuFoodType;
const chefModel = model.chefs;
const MenuItemAddon = model.menu_items_addon;
const MenuItemVarietyModel = model.MenuItemVariety;
const MenuItemCutoffTime = model.MenuItemCutoffTime;
const Categorys = model.categorys;



exports.saveMenuItem = (data) => {
    return new Promise((resolve, reject) => {
        MenuItem.create(data).then((response) => {
            resolve(response);
        }).catch((err) => {
            reject({ error: err });
        });
    });
};

exports.updateMenuItem = (data,whereClause) => {
    return new Promise((resolve, reject) => {
        MenuItem.update(data,{where:whereClause}).then((response) => {
            resolve(response);
        }).catch((err) => {
            reject(err);
        });
    });
};

exports.findAllMenuItem = (whereClause,array,availability,deliverySlot,cutOfftime) => {
    return new Promise((resolve, reject) => {
        MenuItem.findAll({
            where:whereClause,["order"]:array,
            include:[{model: kitchenType,as: 'kitchenTypeData'},
            {model: chefModel,as: 'chefData'},
            {model: Categorys,as:'root_category',required: false},
            {model: Categorys,as:'sub_category',required: false},
            {model: MenuFoodTypeModel,as: 'foodType'},
            {model: Course,as: 'course'},
            {model: MenuItemVarietyModel,as: 'varities'},
            {model: Inclusion,as: 'inclusions'},
            {model: MenuItemCutoffTime,as: 'menuItemCutoffTime',where:cutOfftime},
            {model: MenuItemAvailable,as: 'availability',
              where:availability
            },{model:MenuItemDeliveryTimeSlotMaster,where:deliverySlot},
            {model: MenuItemAddon,as: 'addon',include:[{model: AddonModel,as: 'addon'}]},      
            {model: MenuType,as: 'menuTypeData'}]}).then((response) => {
            resolve(response);
        }).catch((err) => {
            reject(err);
        });
    });
};

exports.linkMenuItemInclusion = (inclusions) => {    
    return new Promise((resolve, reject) => {
        if(inclusions && inclusions.length == 0) {
            resolve(inclusions);
            return;
        }
        Inclusion.bulkCreate(inclusions).then(response => {      
        	resolve(response);
        }).catch(err => {   
        	reject(err);
        });
    });
};


exports.linkMenuItemCategory = (categories) => {
    return new Promise((resolve, reject) => {
        if(categories && categories.length == 0) {
            resolve(categories);
            return;
        }
        MenuItemCategory.bulkCreate(categories).then((response) => {
            resolve(response);
        }).catch((err) => {
            reject(err);
        });
    });
};

exports.linkMenuItemAddon = (addon) => {
    return new Promise((resolve, reject) => {
        if(addon && addon.length == 0) {
            resolve(addon);
            return;
        }
        MenuItemAddon.bulkCreate(addon).then((response) => {
            resolve(response);
        }).catch((err) => {
            reject(err);
        });
    });
};

exports.findlinkMenuItemCategory = (filters) => {
    let whereClause = {};
    if (filters) {    
        let conditions = JSON.parse(filters);
        for (let i = 0; i < conditions.length; i++) {
        let field = conditions[i]["field"];
        let predicate = conditions[i]["predicate"];
        let value = conditions[i]["value"];
            if(field == 'category_ids') {
                whereClause["category_id"] = value;        
            }
        }
    }
    return new Promise((resolve, reject) => {  
        if(whereClause && whereClause == {}) {
            resolve([]);
            return;
        }      
        MenuItemCategory.findAll({where:whereClause}).then((response) => {
            resolve(response);
        }).catch((err) => {
            console.log(err)
            reject(err);
        });
    });
};

exports.linkMenuItemAvailability = (availabilities) => {
    return new Promise((resolve, reject) => {
        if(availabilities && availabilities.length == 0) {
            resolve(availabilities);
            return;
        }
        MenuItemAvailable.bulkCreate(availabilities).then((response) => {
            resolve(response);
        }).catch((err) => {
            reject({ error: err });
        });
    });
};

exports.fetchDeliveryTimeSlot= (menuItemIds) => {
    return new Promise((resolve, reject) => {
        MenuItemDeliveryTimeSlot.findAll({where:{menu_item_id:menuItemIds}}).then((response) => {
            resolve(response);
        }).catch((err) => {
            reject(err);
        });
    });
};

exports.linkMenuItemDeliveryTimeSlot = (timeSlots) => {
    return new Promise((resolve, reject) => {
        if(timeSlots && timeSlots.length == 0) {
            resolve(timeSlots);
            return;
        }
        MenuItemDeliveryTimeSlot.bulkCreate(timeSlots).then((response) => {
            resolve(response);
        }).catch((err) => {
            reject({ error: err });
        });
    });
};

exports.linkMenuItemCutoffTimes = (cutoffTimes) => {
    return new Promise((resolve, reject) => {
        if(cutoffTimes && cutoffTimes.length == 0) {
            resolve(cutoffTimes);
            return;
        }
        MenuItemCutoffTime.bulkCreate(cutoffTimes).then((response) => {
            resolve(response);
        }).catch((err) => {
            reject({ error: err });
        });
    });
};

exports.linkMenuItemVarieties = (varieties) => {
    return new Promise((resolve, reject) => {
        if(varieties && varieties.length == 0) {
            resolve(varieties);
            return;
        }
        MenuItemVarietyModel.bulkCreate(varieties).then((response) => {
            resolve(response);
        }).catch((err) => {
            reject({ error: err });
        });
    });
};

exports.findMenuItemById = (menuItemId) => {
    return new Promise((resolve, reject) => {
        MenuItem.findOne({ where: { menu_item_id: menuItemId,is_deleted: false },
            include:[{model: kitchenType,as: 'kitchenTypeData'},
            {model: chefModel,as: 'chefData'},
            {model: MenuFoodTypeModel,as: 'foodType'},
            {model: Course,as: 'course'},
            {model: Categorys,as:'root_category'},
            {model: Categorys,as:'sub_category'},
            {model: MenuItemVarietyModel,as: 'varities'},
            {model: Inclusion,as: 'inclusions'},
            {model: MenuItemCutoffTime,as: 'menuItemCutoffTime'},
            {model: MenuItemAvailable,as: 'availability'},
            {model:MenuItemDeliveryTimeSlotMaster},
            {model: MenuType,as: 'menuTypeData'}]}).then((data) => {
            if(!data) {
                throw new Error("Menu Item not found");
            }
            resolve(data);
        }).catch((err) => {
            reject(err);
        });
    });
};

exports.findMenuItemByChefId = (menuItemId,chefId) => {
    return new Promise((resolve, reject) => {
        MenuItem.findOne({ where: { menu_item_id: menuItemId,chef_id:chefId,is_deleted: false }}).then((data) => {
            if(!data) {
                throw new Error("Menu Item not found");
            }
            resolve(data);
        }).catch((err) => {
            reject(err);
        });
    });
};


exports.deleteMenuItemInclusion = (menuItemId) => {    
    return new Promise((resolve, reject) => {
        Inclusion.destroy({where:{menu_item_id:menuItemId}}).then(response => {      
        	resolve(response);
        }).catch(err => {   
        	reject(err);
        });
    });
};

exports.deleteMenuItemAddon = (menuItemId) => {
    return new Promise((resolve, reject) => {
        MenuItemAddon.destroy({where:{menu_item_id:menuItemId}}).then((response) => {
            resolve(response);
        }).catch((err) => {
            reject(err);
        });
    });
};

exports.deleteMenuItemAvailability = (menuItemId) => {
    return new Promise((resolve, reject) => {
        MenuItemAvailable.destroy({where:{menu_item_id:menuItemId}}).then((response) => {
            resolve(response);
        }).catch((err) => {
            reject({ error: err });
        });
    });
};

exports.deleteMenuItemDeliveryTimeSlot = (menuItemId) => {
    return new Promise((resolve, reject) => {
        MenuItemDeliveryTimeSlot.destroy({where:{menu_item_id:menuItemId}}).then((response) => {
            resolve(response);
        }).catch((err) => {
            reject({ error: err });
        });
    });
};

exports.deleteMenuItemCutoffTimes = (menuItemId) => {
    return new Promise((resolve, reject) => {
        MenuItemCutoffTime.destroy({where:{menu_item_id:menuItemId}}).then((response) => {
            resolve(response);
        }).catch((err) => {
            reject({ error: err });
        });
    });
};

exports.deleteMenuItemVarieties = (menuItemId) => {
    return new Promise((resolve, reject) => {
        MenuItemVarietyModel.destroy({where:{menu_item_id:menuItemId}}).then((response) => {
            resolve(response);
        }).catch((err) => {
            reject({ error: err });
        });
    });
};