const model = require("../models");
const Order = model.Order;
const OrderItem = model.OrderItem;
const OrderCustomer = model.OrderCustomer;
const OrderChefStatus = model.OrderChefStatus;
const OrderAddonModel = model.OrderAddon;

exports.createOrder = (data) => {
    return new Promise((resolve, reject) => {
        Order.create(data).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

exports.createOrderItems = (items) => {
    return new Promise((resolve, reject) => {
        OrderItem.bulkCreate(items).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

exports.findOrderById = (order_id) => {
    return new Promise((resolve, reject) => {
      Order.findOne({where: {orderId: order_id}, include: ['items','order_customer']})
          .then((order) => {
            resolve(order);
          })
          .catch((err) => {
            reject(err);
          });
      });
};

exports.findAllOrders = (whereClause,whereClause_I,fetchId) => {
    let wherecustomer={};
    let wherechef;
    if(whereClause_I=={}){
        wherechef={};
    }else{
        wherechef=whereClause_I;
    }    
    if(fetchId){
        if(fetchId['customerId']){
            wherecustomer['cust_id']=fetchId['customerId'];
        }
        if(fetchId['chefId']){
            wherechef['chefId']=fetchId['chefId'];
        }
    }
    console.log(whereClause,wherecustomer,wherechef);
    return new Promise((resolve, reject) => {
      let whereCondition = whereClause;
      Order.findAll({where: whereCondition,["order"]:[['orderId','DESC']], 
      include: [
          "order_addon","customer_reviews","chef_status",
          {model: OrderItem,as: 'items',where:wherechef,include: ["customer_item_reviews"]},
          {model: OrderCustomer,as: 'order_customer',where:wherecustomer}
        ]}).then((order) => {
            resolve(order);
        }).catch((err) => {
            reject(err);
        });
    });
};


exports.saveOrderCustomer = (orderId, customer) => {
    return new Promise((resolve, reject) => {
        OrderCustomer.findOne({where: {order_id:orderId}}).then(order => {
        if(!order){
          return OrderCustomer.create(customer)
        }else{
          return order;
        }
        }).then((data) => {
            resolve(data)
        }).catch((err) => {
            reject(err);
        });
    });
};

exports.acceptOrder = (order_id, chefId) => {
    return new Promise((resolve, reject) => {
        Order.findOne({where: {orderId:order_id, orderStatus: 'ordered'}}).then(order => {
            if(order) {
                let orderChefStatus = {};
                orderChefStatus['status'] = "accepted";
                orderChefStatus['order_id'] = order_id;
                orderChefStatus['chef_id'] = chefId;
                return Promise.all([order, OrderChefStatus.create(orderChefStatus)]);
            } else {
                throw new Error("This Order already accepted");
            }
        }).then(([order, orderStatus]) => {
            let orderValuse=order.dataValues;
            orderValuse['status'] = 'closed';
            orderValuse['orderStatus'] = 'accepted';
            return Order.update(orderValuse, { where: { orderId: orderValuse['orderId']}});
        }).then((data) => {
            resolve(data)
        }).catch((err) => {
            reject(err);
        });
    });
};

exports.rejectOrder = (order_id, chefId, reason) => {
    return new Promise((resolve, reject) => {
        Order.findOne({where: {orderId:order_id, orderStatus: 'ordered'}}).then(order => {
            if(order) {
                let orderChefStatus = {};
                orderChefStatus['status'] = "rejected";
                orderChefStatus['order_id'] = order_id;
                orderChefStatus['chef_id'] = chefId;
                orderChefStatus['rejected_reason'] = reason;
                return Promise.all([order, OrderChefStatus.create(orderChefStatus)]);
            } else {
                throw new Error("This Order already rejected");
            }
        }).then(([order, orderStatus]) => {
            let orderValuse=order.dataValues;
            orderValuse['status'] = 'closed';
            orderValuse['orderStatus'] = 'cancelled';
            return Order.update(orderValuse, { where: { orderId: orderValuse['orderId']}});
        }).then((data) => {
            resolve(data)
        })
        .catch(err => {
            reject(err);
        });
    });
};

exports.completedOrder = (order_id, chefId) => {
    return new Promise((resolve, reject) => {
        Order.findOne({where: {orderId:order_id, orderStatus: 'outdelivery'}}).then(order => {
            if(order) {
                let orderChefStatus = {};
                orderChefStatus['status'] = "completed";
                orderChefStatus['order_id'] = order_id;
                orderChefStatus['chef_id'] = chefId;
                return Promise.all([order, OrderChefStatus.create(orderChefStatus)]);
            } else {
                throw new Error("This Order already completed");
            }
        }).then(([order, orderStatus]) => {
            let orderValuse=order.dataValues;
            orderValuse['status'] = 'completed';
            orderValuse['orderStatus'] = 'completed';
            return Order.update(orderValuse, { where: { orderId: orderValuse['orderId']}});
        }).then((data) => {
            resolve(data)
        }).catch((err) => {
            reject(err);
        });
    });
};

exports.outdeliveryOrder = (order_id, chefId) => {
    return new Promise((resolve, reject) => {
        Order.findOne({where: {orderId:order_id, orderStatus:'accepted'}}).then(order => {
            console.log()
            if(order) {
                let orderChefStatus = {};
                orderChefStatus['status'] = "outdelivery";
                orderChefStatus['order_id'] = order_id;
                orderChefStatus['chef_id'] = chefId;
                return Promise.all([order, OrderChefStatus.create(orderChefStatus)]);
            } else {
                throw new Error("This Order already out_delivery");
            }
        }).then(([order, orderStatus]) => {
            let orderValuse=order.dataValues;
            orderValuse['status'] = 'outdelivery';
            orderValuse['orderStatus'] = 'outdelivery';
            return Order.update(orderValuse, { where: { orderId: orderValuse['orderId']}});
        }).then((data) => {
            resolve(data)
        }).catch((err) => {
            reject(err);
        });
    });
};

exports.getAllOrderChefStatus = (chefId,whereCondition) => {
    return new Promise((resolve, reject) => {
    OrderChefStatus.findAll({where: {chef_id:chefId},
        include:[{model: Order,as: 'order',where:whereCondition,
        include: ['items','order_customer',"customer_reviews"]}]}).then((order) => {
        resolve(order);        
    }).catch((err) => {
        reject(err);
        });
    });
};

exports.getAllOrderChef = (chefId) => {
    return new Promise((resolve, reject) => {
        Order.findAll({["order"]:[['orderId','DESC']], 
        include: [{model: OrderItem,as: 'items',where:{chefId:chefId}},"order_addon","customer_reviews"]}).then((order) => {
        resolve(order);        
    }).catch((err) => {
        reject(err);
        });
    });
};

exports.createOrderAddons = (data) => {
    return new Promise((resolve, reject) => {
        OrderAddonModel.bulkCreate(data).then((res) => {
            resolve(res);
        }).catch((err) => {
            reject(err);
        });
    });
};