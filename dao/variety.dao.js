const model = require("../models");
const VarietyModel = model.Variety;

exports.saveVariety = (data) => {
    return new Promise((resolve, reject) => {
        VarietyModel.create(data).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });
};

exports.findAllVariety = () => {
    return new Promise((resolve, reject) => {
        VarietyModel.findAll().then((res) => {
            resolve(res);
        })
        .catch((err) => {
            reject(err);
        });
    });
};

exports.findVarietyByChef = (chefId) => {
    return new Promise((resolve, reject) => {
        VarietyModel.findAll({where:{chef_id:chefId}}).then((res) => {
            resolve(res);
        })
        .catch((err) => {
            reject(err);
        });
    });
};
