const model = require("../models");
const KitchenType = model.kitchen_type;

exports.fetchKitchenTypeById = (kitchenTypeId) => {
  return getKitchenTypeById(kitchenTypeId);
};

exports.findAllKitchenTypes = () => {
    return new Promise((resolve, reject) => {
        KitchenType.findAll()
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            console.log(err);
            reject(err);
          });
      });
};

exports.findKitchenTypeById = (kitchenTypeId) => {
  return new Promise((resolve, reject) => {
    getKitchenTypeById(kitchenTypeId).then(data => {
      if(!data) {
        throw new Error("Kitchen Type not found");
      }
      resolve(data);
    }).catch(err => {
      reject(err);
    })
  })
}

function getKitchenTypeById(kitchenTypeId) {
  return new Promise((resolve, reject) => {
    KitchenType.findOne({ where: { kitchen_type_id: kitchenTypeId} })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        console.log(err);
        reject(err);
      });
  });
}

exports.updateKitchenType = (kitchen_type_id, data) => {
    return new Promise((resolve, reject) => {
        KitchenType.update(data, { where: { kitchen_type_id: kitchen_type_id } }).then(res => {
        return getKitchenTypeById(kitchen_type_id);
      }).then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      })
    });
};

exports.saveKitchenType = (data) => {
  return new Promise((resolve, reject) => {
    KitchenType.create(data).then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  });
};