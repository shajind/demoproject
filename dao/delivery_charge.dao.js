const model = require("../models");
const DeliveryChargeModel = model.DeliveryCharge;
const {Op} = require('sequelize');

exports.saveDeliveryCharge = (data) => {
    return new Promise((resolve, reject) => {
    DeliveryChargeModel.create(data).then(res => {
        resolve(res);
    }).catch(err => {
        reject(err);
      })
    });
};

exports.updateDeliveryCharge = (deliveryChargeID,deliveryCharge) => {
    return new Promise((resolve, reject) => {
        DeliveryChargeModel.update(deliveryCharge, { where: { delivery_charge_id:deliveryChargeID } }).then(res => {
            return getDeliveryChargeById(deliveryChargeID);
        }).then(response => {
            resolve(response);
        }).catch(err => {
            reject(err);
        })
    });
};

function getDeliveryChargeById(deliveryChargeID) {
    return new Promise((resolve, reject) => {
        DeliveryChargeModel.findOne({ where: { delivery_charge_id:deliveryChargeID} })
        .then((res) => {
            resolve(res);
        })
        .catch((err) => {
            reject(err);
        });
    });
}

exports.findAllDeliveryCharge = () => {
    return new Promise((resolve, reject) => {
        DeliveryChargeModel.findAll().then((res) => {
            resolve(res);
        }).catch((err) => {
            console.log(err);
            reject(err);
        });
    });
};


exports.fetchDeliveryCharge = (distanceKm) => {
    return new Promise((resolve, reject) => {
        DeliveryChargeModel.findOne().then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });
};