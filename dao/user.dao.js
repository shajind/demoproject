const model = require("../models");
const User = model.users;
const Role = model.role;
const UserRoleMst = model.UserRole;
const { Op } = require("sequelize");

exports.fetchUserByMobileAndUserTypeId = (mobile, userTypeId) => {
  return new Promise((resolve, reject) => {
    User.findOne({ where: { user_mobile: mobile, user_type_id: userTypeId } })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

exports.findUserById = (userId) => {
  return getUserById(userId);
};

function getUserById(userId) {
  return new Promise((resolve, reject) => {
    User.findOne({ where: { user_id: userId}, include: Role})
      .then((res) => {
        resolve(res);
      }).catch((err) => {
        reject(err);
      });
  });
}

exports.updateUser = (userId, data) => {
    return new Promise((resolve, reject) => {
      User.update(data, { where: { user_id: userId } }).then(res => {
        return getUserById(userId);
      }).then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      })
    });
};

exports.saveUser = (data, role) => {
  return new Promise((resolve, reject) => {
    data['user_mobile'] = data['mobile'];
    data['user_email'] = data['email'];
    User.create(data).then(res => {
      return Promise.all([res, res.addRole(role, {through: {selfGranted: false }})]);
    }).then(([user, role]) => {
      return getUserById(user['user_id']);
    }).then(res => {
      resolve(res);
    })
    .catch(err => {
      reject(err);
    })
  });
};

exports.findUserByMobileOrEmail = (mobile, email) => {
  return new Promise((resolve, reject) => {
    User.findOne({ where: {user_mobile: mobile}, include: [ Role ] }).then((res) => {
        resolve(res);
      }).catch((err) => {
        reject(err);
      });
  });
};


exports.createRoleForExistingUser = (userId, role) => {
  return new Promise((resolve, reject) => {
    User.findOne({ where: {user_id: userId}, include: [Role]}).then(res => {
      let roles = res.roles;
      for(let idx = 0;idx < roles.length;idx++) {
        if(roles[idx].role_code == role.role_code) {
          throw new Error("User Role already exist");
        }
      }
      return Promise.all([res, res.addRole(role, {through: {selfGranted: false }})]);
    }).then(([user, role]) => {
      return getUserById(user['user_id']);
    }).then(res => {
      resolve(res);
    })
    .catch(err => {
      reject(err);
    })
  });
};

exports.validateLogin = (username, password) => {
  return new Promise((resolve, reject) => {
    User.findOne({ where: {[Op.or]: [{ user_name: username }, { user_mobile: username }],
      user_otp: password}, include: [ Role ] }).then((res) => {
        resolve(res);
      }).catch((err) => {
        reject(err);
      });
  });
};


exports.validateAdminLogin = (username, passWord) => {
  return new Promise((resolve, reject) => {
    User.findOne({ where: {[Op.or]: [{ user_name: username }, { user_mobile: username }],
      password: passWord}, include: [ Role ] }).then((res) => {
        resolve(res);
      }).catch((err) => {
        reject(err);
      });
  });
};

exports.updateUserData = (userId, data) => {
  return new Promise((resolve, reject) => {
    getUserById(userId).then(res => {
      let userDb = res.dataValues;
      if(data['user_email']) {
        userDb['user_email'] = data['user_email'];
      }
      return Promise.all([User.update(userDb, { where: { user_id: userId } }), getUserById(userId)]);
    }).then(([response, res]) => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  });
};


exports.adminLoginRole = (userId, roleId) => {
  return new Promise((resolve, reject) => {
    UserRoleMst.findOne({ where: { user_id: userId, role_id: roleId } })
    .then((res) => {
      if(!res) {
        throw new Error("admin not found");
      }
      resolve(res);
    })
    .catch((err) => {
      reject(err);
    });
  });
};