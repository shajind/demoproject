const model = require("../models");
const CustomerReview = model.CustomerReview;
const Customer = model.customers;
const sequelize = require('sequelize');

exports.saveCustomerReview = (data) => {
    return new Promise((resolve, reject) => {
        CustomerReview.create(data).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });
};


exports.findAllCustomerReview = (whereClause) => {
    return new Promise((resolve, reject) => {
        CustomerReview.findAll({where:whereClause,include: [{model:Customer,
            as: "customer",attributes: ["cust_fullname"]}]}).then((res) => {
            resolve(res);
        }).catch((err) => {
            console.log(err);
            reject(err);
        });
    });
};

exports.getCustomerReview = (orderId) => {
    return new Promise((resolve, reject) => {
        CustomerReview.findOne({where:{order_id:orderId},include: ["menu_item_review"]}).then((res) => {
            resolve(res);
        }).catch((err) => {
            console.log(err);
            reject(err);
        });
    });
};

exports.getCustomerReviewByChefId = (whereClause) => {
    return new Promise((resolve, reject) => {
        CustomerReview.findAll({where:whereClause}).then((res) => {
            resolve(res);
        }).catch((err) => {
            console.log(err);
            reject(err);
        });
    });
};


exports.getCustomerReviewCount = (whereClause) => {
    return new Promise((resolve, reject) => {
        CustomerReview.findAll({where:whereClause,
            attributes: [
                [ sequelize.fn('count', sequelize.col('menu_item_id')), 'count']
            ],raw: true}).then((res) => {
                console.log(whereClause,res[0].count);
            resolve(res[0].count);
        }).catch((err) => {
            console.log(err);
            reject(err);
        });
    });
};