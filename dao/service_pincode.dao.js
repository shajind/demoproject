const model = require("../models");
const ServicePincodeModel = model.ServicePincode;
const City = model.city;


exports.saveServicePincode = (data) => {
    return new Promise((resolve, reject) => {
        ServicePincodeModel.bulkCreate(data).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    });
};


exports.findAllServicePincode = () => {
    return new Promise((resolve, reject) => {
        ServicePincodeModel.findAll().then((res) => {
            resolve(res);
        }).catch((err) => {
            reject(err);
        });
    });
};


exports.findServicePincodeByPincode = (pincodeNo) => {
    return new Promise((resolve, reject) => {
        ServicePincodeModel.findOne({where:{pincode:pincodeNo},include: [{model: City,as: "city"}]}).then((res) => {
            resolve(res.city);
        }).catch((err) => {
            reject(err);
        });
    });
};

