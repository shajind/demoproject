const express = require("express");
const bodyparser = require("body-parser");

const app = express();
const db = require("./models/index");

let session = require('express-session');
const { fetchAllCategories } = require("./services/category.service");
app.use(session(   {secret: 'daypay.yapyad.daypay.ssh',
key: "sessionID",
resave: false,
saveUninitialized: true,
cookie: {
    secure: true
}}))
// serve images
app.use('/image/',express.static('/home/ubuntu/yumma_backend//image'));

// serve images
//app.use(express.static("image"));

app.all("/*", function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
  res.header(
    "Access-Control-Allow-Headers",
    "Content-type,Accept,access-token,X-Key,group_id"
  );
  if (req.method == "OPTIONS") {
    res.status(200).end();
  } else {
    next();
  }
});

db.sequelize
  .sync({ alter: false })
  .then(() => {
    app.use(
      bodyparser.urlencoded({
        extended: true,
      })
    );
    app.use(
      bodyparser.json({
        limit: "10mb",
      })
    );

    let routes = require("./routes/routes.js")(app);

    let port = process.env.PORT || 2021;
    app.listen(port, () => console.log(`Server listening on port ${port}!`));
  })
  .catch((err) => {
    console.log("connection error ", err);
  });
