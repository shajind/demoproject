module.exports = (sequelize, Sequelize) => {
    const CustomerReview = sequelize.define("CustomerReview",{
        review_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        overall_rating: {
            type: Sequelize.DECIMAL(2,1),
            allowNull: false,
            defaultValue: 0
        },
        taste_rating: {
            type: Sequelize.DECIMAL(2,1),
            allowNull: false,
            defaultValue: 0
        },
        presentation_rating: {
            type: Sequelize.DECIMAL(2,1),
            allowNull: false,
            defaultValue: 0
        },
        delivery_rating: {
            type: Sequelize.DECIMAL(2,1),
            allowNull: false,
            defaultValue: 0
        },
        title: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        review_description: {
            type: Sequelize.STRING,
            allowNull: true,
        },        
        image_or_video_path: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        order_id: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        menu_item_id: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        parent_review_id: {
            type: Sequelize.INTEGER,
            allowNull: true,
            defaultValue: 0
        }
    }, {tableName: 'customer_review'});
    return CustomerReview;
};