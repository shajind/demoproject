module.exports = (sequelize, Sequelize) => {
    const Countries = sequelize.define("countries",{
        country_id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true         
        },
        country_code:{
            type:Sequelize.STRING,
            //unique: true                  
        },
        country_name: {
            type: Sequelize.STRING
        },
        continent_name: {
            type: Sequelize.STRING
        }
    }, {tableName: 'country_mst'});
    return Countries;
};