module.exports = (sequelize, Sequelize) => {
    const notification = sequelize.define("notifications",{
        notification_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        notification_type:{
            type: Sequelize.STRING
        },
        notification_message:{
            type: Sequelize.STRING
        },
        notifiDateTime: {
            type: Sequelize.DATE(3),
            defaultValue: Sequelize.literal('CURRENT_TIMESTAMP(3)'),
        },
        target_group: {
            type: Sequelize.STRING
        },
        notifi_status: {
            type: Sequelize.STRING,
            defaultValue:'Unaccept'
        },
        
    }, {tableName: 'notification'});
    return notification;
};