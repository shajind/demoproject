module.exports = (sequelize, Sequelize) => {
    const Banner = sequelize.define("banners",{
        banner_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        banner_type: {
            type: Sequelize.STRING
        },
        banner_title: {
            type: Sequelize.STRING
        },
        banner_image: {
            type: Sequelize.STRING
        },
        banner_destination_page: {
            type: Sequelize.STRING
        },
        banner_target: {
            type: Sequelize.STRING
        },
        status: {
            type: Sequelize.STRING
        }
    }, {tableName: 'banner'});
    return Banner;
};	
				