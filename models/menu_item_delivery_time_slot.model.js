module.exports = (sequelize, Sequelize) => {
    const MenuItemDeliveryTimeSlot = sequelize.define("MenuItemDeliveryTimeSlot",{
        link_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        }
    }, {
        tableName: 'menu_item_delivery_time_slot',
        timestamps: false,
        primaryKey: false
    });
    return MenuItemDeliveryTimeSlot;
};