module.exports = (sequelize, Sequelize) => {
    const DeliveryTimeSlotMst = sequelize.define("delivery_time_slot_msts",{
        slot_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        slot_from_time: {
            type: Sequelize.TIME
        },
        slot_to_time: {
            type: Sequelize.TIME
        },
        slot_label: {
            type: Sequelize.STRING
        },
        cutoff_type: {
            type: Sequelize.STRING
        }
    }, {tableName: 'delivery_time_slot_mst'});
    return DeliveryTimeSlotMst;
};