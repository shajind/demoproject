module.exports = (sequelize, Sequelize) => {
    const ChefEarning = sequelize.define("chef_earning",{
        earnedId: {
            field: 'earned_id',
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        totalAmountEarned: {
            field: 'total_amount_earned',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        totalWithdrawAmount: {
            field: 'total_withdraw_amount',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        balanceToWithdraw: {
            field: 'balance_to_withdraw',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        }        
    }, {tableName: 'chef_earning',timestamps: false});
  return ChefEarning;
};