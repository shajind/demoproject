module.exports = (sequelize, Sequelize) => {
    const DayAvailableMenuItems = sequelize.define("day_available_menu_items",{
        day_available_menu_item_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        available_day: {
            type: Sequelize.STRING,
            allowNull: true
        }       
    });
    return DayAvailableMenuItems;
};