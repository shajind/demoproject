module.exports = (sequelize, Sequelize) => {
    const Users = sequelize.define("users",{
        user_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        user_name: {
            type: Sequelize.STRING
        },
        user_mobile: {
            type: Sequelize.STRING
        },
        user_otp: {
            type: Sequelize.STRING
        },
        user_email: {
            type: Sequelize.STRING
        },
        password: {
            type: Sequelize.STRING
        },
        access_token: {
            type: Sequelize.STRING
        },
        login_type: {
            type: Sequelize.STRING
        },
        status: {
            type: Sequelize.STRING,
            defaultValue:'active'
        }
    }, {tableName: 'user_mst'});
    return Users;
};