module.exports = (sequelize, Sequelize) => {
    const MenuFoodType = sequelize.define("MenuFoodType",{
        food_type_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        food_type: {
            type: Sequelize.STRING,
            //unique: true,
            allowNull: false
        },
        food_type_desc: {
            type: Sequelize.STRING
        },
        food_type_code: {
            type: Sequelize.STRING,
            //unique: true
        }
    }, {tableName: 'menu_food_type_mst'});
    return MenuFoodType;
};