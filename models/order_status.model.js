module.exports = (sequelize, Sequelize) => {
    const OrderStatusMst = sequelize.define("OrderStatusMst",{
      status_id:{
        type:Sequelize.INTEGER,
        autoIncrement:true,
        primaryKey:true
      },
      status:{
        type:Sequelize.STRING,
        defaultValue:null
      },
      status_parent_id: {
        type: Sequelize.INTEGER,
        defaultValue: 0
    }
    }, {tableName: 'order_status_mst'});
  return OrderStatusMst;
};