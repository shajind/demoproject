module.exports = (sequelize, Sequelize) => {
    const CartMenuItemAvailable = sequelize.define("cart_available",{
        cart_available_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        cartId: {
            field: 'cart_id',
            type: Sequelize.INTEGER,
            allowNull: false
        },
        available_date: {
            type: Sequelize.DATEONLY,
        },
        cartItemId: {
            field: 'cart_item_id',
            type: Sequelize.INTEGER,
            allowNull: false
        }
    }, {tableName: 'cart_availability',timestamps: false});
    return CartMenuItemAvailable;
};