module.exports = (sequelize, Sequelize) => {
    const MenuItemCutoffTime = sequelize.define("menu_item_cutoff_time",{
        menu_item_cutoff_time_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true              
        },
        cutoff_day: {
            type: Sequelize.ENUM({
                values: ['Today', 'Previous_day']
            }),
            defaultValue:"Today"
        },
        cutoff_time:{
            type: Sequelize.TIME
        },
        cutoff_type:{
            type: Sequelize.STRING
        }
    }, {tableName: 'menu_item_cutoff_time',timestamps: false});
    return MenuItemCutoffTime;
};