module.exports = (sequelize, Sequelize) => {
    const CartAddon = sequelize.define("cart_addon",{
        cart_addon_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        addon_id: {
            type: Sequelize.INTEGER
        },
        addon_name: {
            type: Sequelize.STRING
        },
        addon_image: {
            type: Sequelize.STRING
        },
        price: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        qty: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        cartId:{
            field: 'cart_id',
            type:Sequelize.INTEGER
        },
        rowTotal: {
            field: 'row_total',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        cartItemId:{
            field: 'cart_item_id',
            type:Sequelize.INTEGER
        }
    }, {tableName: 'cart_addon'});
    return CartAddon;
};