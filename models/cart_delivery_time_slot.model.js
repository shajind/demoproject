module.exports = (sequelize, Sequelize) => {
    const CartDeliveryTimeSlot = sequelize.define("cart_delivery_time_slot",{
        cart_delivery_slot_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        cartId: {
            field: 'cart_id',
            type: Sequelize.INTEGER,
            allowNull: false
        },
        slot_from_time: {
            type: Sequelize.TIME
        },
        slot_to_time: {
            type: Sequelize.TIME
        },
        slot_label: {
            type: Sequelize.STRING
        },
        cartItemId: {
            field: 'cart_item_id',
            type: Sequelize.INTEGER,
            allowNull: false
        }
    }, {tableName: 'cart_delivery_time_slot'});
    return CartDeliveryTimeSlot;
};