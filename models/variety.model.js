module.exports = (sequelize, Sequelize) => {
    const Variety = sequelize.define("VarietyMst",{
        variety_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true              
        },
        // variety_code: {
        //     type: Sequelize.STRING,
        // },
        variety_name: {
            type: Sequelize.STRING
        },
        // description: {
        //     type: Sequelize.STRING
        // }
    }, { tableName: 'variety_mst'});
    return Variety;
};