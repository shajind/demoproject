module.exports = (sequelize, Sequelize) => {
    const ChefTransactionHistory = sequelize.define("ChefTransactionHistory",{
        txnId: {
            field: 'txn_id',
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        amountWithdraw: {
            field: 'amount_withdraw',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        status: {
            field: 'status',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        taxDeduction: {
            field: 'tax_deduction',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        }
    }, {tableName: 'chef_transaction_history'});
  return ChefTransactionHistory;
};