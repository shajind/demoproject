const dbConfig = require("../config/db.config.js").development;
const Sequelize = require("sequelize");

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle,
  },
});

// sequelize.authenticate()
//   .then(() => {
//     console.log("Connection has been established successfully.");
//   })
//   .catch(err => {
//     console.error("Unable to connect to the database:", err);
//   });

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.role = require("./role.model")(sequelize, Sequelize);
db.users = require("./users.model.js")(sequelize, Sequelize);
db.UserRole = require("./user_role.model.js")(sequelize, Sequelize);
db.customers = require("./customer.model.js")(sequelize, Sequelize);
db.chefs = require("./chef.model.js")(sequelize, Sequelize);
db.menu_items = require("./menu_item.model.js")(sequelize, Sequelize);
db.menu_item_available = require("./menu_item_available.model.js")(sequelize, Sequelize);
db.menu_items_addon = require("./menu_item_addon.model.js")(sequelize, Sequelize);
db.menu_types = require("./menu_type.model")(sequelize, Sequelize);
db.user_device_tokens = require("./user_device_token.model.js")(sequelize,Sequelize);
db.categorys = require("./category.model.js")(sequelize, Sequelize);
db.Order = require("./order.model.js")(sequelize, Sequelize);
db.OrderItem = require("./order_item.model.js")(sequelize, Sequelize);
db.OrderCustomer = require("./order_customer.model.js")(sequelize, Sequelize);
db.OrderChefStatus = require("./order_chef_status.model")(sequelize, Sequelize);
db.OrderStatus = require("./order_status.model")(sequelize, Sequelize);
db.MenuItemCutoffTime = require("./menu_item_cutoff_time.model")(sequelize, Sequelize);
db.ServicePincode = require("./service_pincode.mode")(sequelize, Sequelize);


db.ChefEarning = require("./chef_earning.model")(sequelize, Sequelize);
db.ChefEarningHistory = require("./chef_earning_history.model")(sequelize, Sequelize);


db.CartAddon = require("./cart_addon.model")(sequelize, Sequelize);
db.OrderAddon = require("./order_addon.model")(sequelize, Sequelize);


db.country = require("./countries.model.js")(sequelize, Sequelize);
db.MenuFoodType = require("./menu_food_type.model.js")(sequelize, Sequelize);
db.notification = require("./notification.model.js")(sequelize, Sequelize);
db.notificationlog = require("./notificationlog.model.js")(sequelize,Sequelize);
db.testimonial = require("./testimonial.model.js")(sequelize, Sequelize);
db.banner = require("./banner.model")(sequelize, Sequelize);
db.state = require("./state.model.js")(sequelize, Sequelize);
db.city = require("./city.model.js")(sequelize, Sequelize);
db.coupons = require("./coupons.js")(sequelize, Sequelize);

db.advanceBookingMaster = require("./advance_booking_master.model")(sequelize, Sequelize);
db.deliveryTimeSlotMaster = require("./delivery_time_slot_master.model")(sequelize, Sequelize);
db.peopleServiceMaster = require("./people_service_master.model")(sequelize, Sequelize);
db.MenuItemInclusion = require("./menu_item_inclusion.model")(sequelize, Sequelize);
//db.MenuItemCategory = require("./menu_item_category.model")(sequelize, Sequelize);
db.MenuItemDeliveryTimeSlot = require("./menu_item_delivery_time_slot.model")(sequelize, Sequelize);
db.Variety = require("./variety.model")(sequelize, Sequelize);
db.MenuItemVariety = require("./menu_item_variety.model")(sequelize, Sequelize);
db.course=require("./course.model")(sequelize, Sequelize);
db.addon = require("./addon.model")(sequelize, Sequelize);


db.customerAddress = require("./customer_address.model")(sequelize, Sequelize);
db.CustomerReview = require("./customer_review.model")(sequelize, Sequelize);
db.CustomerCurrentLocation = require("./customer_current_location.model")(sequelize, Sequelize);

db.Cart = require("./cart.model")(sequelize, Sequelize);
db.CartItem = require("./cart_item.model")(sequelize, Sequelize);
db.CartCustomer = require("./cart_customer.model")(sequelize, Sequelize);
db.CartAddress = require("./cart_address.model")(sequelize, Sequelize);
db.kitchen_type=require("./kitchen_type.model")(sequelize, Sequelize);
db.DeliveryCharge = require("./delivery_charge.model")(sequelize, Sequelize);

db.cartAvailability = require("./cart_availability.model")(sequelize, Sequelize);
db.CartDeliveryTimeSlot = require("./cart_delivery_time_slot.model")(sequelize, Sequelize);
db.Tax = require("./tax.model")(sequelize, Sequelize);
db.YummaCommission = require("./yumma_commission.model")(sequelize, Sequelize);
db.WeekDate = require("./week.model")(sequelize, Sequelize);


//for users
db.users.belongsToMany(db.role, { through: db.UserRole, foreignKey: 'user_id'});
db.role.belongsToMany(db.users, { through: db.UserRole, foreignKey: 'role_id'});
db.users.belongsTo(db.country, {as: "countryData", foreignKey: "country_id"});

//for customers
db.customers.belongsTo(db.users, { as: "usersData", foreignKey: "user_id" });
db.customers.belongsTo(db.country, { as: "countryData",foreignKey: "country_id"});
db.customers.belongsTo(db.city, { as: "cityData", foreignKey: "city_id"});
db.customers.belongsTo(db.state, { as: "stateData", foreignKey: "state_id" });

//for customer review
db.CustomerReview.belongsTo(db.chefs, { as: "chef", foreignKey: "chef_id" });
db.CustomerReview.belongsTo(db.customers, { as: "customer", foreignKey: "customer_id" });

//for customer current location
db.CustomerCurrentLocation.belongsTo(db.customers, { as: "customer", foreignKey: "customer_id" });


//for menu_items
db.menu_items.belongsTo(db.menu_types, { as: "menuTypeData", foreignKey: "menu_type_id"});
db.menu_items.belongsTo(db.MenuFoodType, { as: "foodType",foreignKey: "food_type_id"});
db.menu_items.belongsTo(db.course, { as: "course", foreignKey: "course_id" });

db.menu_items.belongsTo(db.chefs, { as: "chefData", foreignKey: "chef_id" });
db.menu_items.belongsTo(db.kitchen_type, { as: "kitchenTypeData", foreignKey: "kitchen_type_id" });
db.menu_items.hasMany(db.MenuItemInclusion, {as: 'inclusions', foreignKey: 'menu_item_id'});
//db.menu_items.belongsToMany(db.categorys, { through: db.MenuItemCategory, foreignKey: 'menu_item_id'});
//db.categorys.belongsToMany(db.menu_items, { through: db.MenuItemCategory, foreignKey: 'category_id'});
db.menu_items.belongsTo(db.categorys, { as: "root_category", foreignKey: "root_category_id" });
db.menu_items.belongsTo(db.categorys, { as: "sub_category", foreignKey: "sub_category_id" });
db.menu_items.hasMany(db.menu_item_available, { as: "availability",foreignKey: "menu_item_id"});
db.menu_items.hasMany(db.MenuItemVariety, { as: "varities", foreignKey: "menu_item_id"});
db.menu_items.hasMany(db.MenuItemCutoffTime, { as: "menuItemCutoffTime",foreignKey: "menu_item_id"});
db.menu_items.belongsTo(db.Variety, { as: "variety", foreignKey: "variety_id" });

db.menu_items.belongsToMany(db.deliveryTimeSlotMaster, { through: db.MenuItemDeliveryTimeSlot, foreignKey: 'menu_item_id', 
uniqueKey: 'menu_item_delivery_time_slot_unique', constraints: false});
db.deliveryTimeSlotMaster.belongsToMany(db.menu_items, { through: db.MenuItemDeliveryTimeSlot, foreignKey: 'delivery_time_slot_id', 
uniqueKey: 'menu_item_delivery_time_slot_unique', constraints: false});

db.menu_items.hasMany(db.menu_items_addon, { as: "addon",foreignKey: "menu_item_id"});

// //for course
// db.course.belongsTo(db.menu_items, { as: "menuItemData",foreignKey: "menu_item_id"});
//for 
//db.MenuItemDeliveryTimeSlot.belongsTo(db.deliveryTimeSlotMaster, { as: "delivery_time_slot",foreignKey: "delivery_time_slot_id"});

//for Variety
db.Variety.belongsTo(db.chefs, { as: "chef", foreignKey: "chef_id" });

//for link menu item availabilities
db.menu_item_available.belongsTo(db.menu_items, { as: "menuItemData",foreignKey: "menu_item_id"});

//for menu_items_add
db.menu_items_addon.belongsTo(db.addon, { as: "addon", foreignKey: "addon_id" });

 //for cart
db.Cart.hasMany(db.CartItem, {as: 'cart_items', foreignKey: 'cart_id'});
db.Cart.hasOne(db.CartCustomer, {as: 'cart_customer', foreignKey: 'cart_id'});
//db.CartItem.belongsTo(db.Cart);
db.CartAddress.belongsTo(db.Cart, { as: "cart_address", foreignKey: "cart_id"});

db.Cart.hasMany(db.CartAddon, {as: 'cart_addon', foreignKey: 'cart_id'});


db.CartItem.hasMany(db.cartAvailability, {as: 'cart_availability', foreignKey: 'cart_items_id'});
db.CartItem.hasMany(db.CartDeliveryTimeSlot, {as: 'cart_delivery_time_slot', foreignKey: 'cart_items_id'});
db.CartItem.hasMany(db.CartAddon, {as: 'cart_addon', foreignKey: 'cart_item_id'});


//for chef_earning
db.ChefEarning.belongsTo(db.chefs, { as: "chef", foreignKey: "chef_id"});

//for chef_earning_history
db.ChefEarningHistory.belongsTo(db.OrderItem, { as: "order_item", foreignKey: "order_item_id"});
db.ChefEarningHistory.belongsTo(db.ChefEarning, { as: "earning", foreignKey: "earned_id"});


//for orde_items
db.Order.hasMany(db.OrderItem, {as: 'items', foreignKey: 'order_id'});
db.Order.hasOne(db.OrderCustomer, {as: 'order_customer', foreignKey: 'order_id'});
db.OrderItem.belongsTo(db.Order);
db.Order.hasMany(db.OrderChefStatus, {as: 'chef_status', foreignKey: 'order_id'});
db.Order.hasMany(db.CustomerReview, {as: 'customer_reviews', foreignKey: 'order_id'});
db.OrderItem.hasOne(db.CustomerReview, {as: 'customer_item_reviews', foreignKey: 'order_item_id'});



db.Order.hasMany(db.OrderAddon, {as: 'order_addon', foreignKey: 'order_id'});


//for orde_chef
db.OrderChefStatus.belongsTo(db.Order, {as: 'order', foreignKey: 'order_id'});
db.OrderChefStatus.belongsTo(db.chefs, { as: "ChefStatus", foreignKey: "chef_id" });

//for chefs
db.user_device_tokens.belongsTo(db.users, { as: "usersData", foreignKey: "user_id"});

//for chefs
db.chefs.belongsTo(db.users, { as: "usersData", foreignKey: "user_id" });
db.chefs.belongsTo(db.country, { as: "countryData", foreignKey: "country_id"});
db.chefs.belongsTo(db.city, { as: "cityData", foreignKey: "city_id"});
db.chefs.belongsTo(db.state, { as: "stateData", foreignKey: "state_id" });
db.chefs.hasMany(db.menu_items, {as: 'menu_items', foreignKey: 'chef_id'});

//for state city
db.state.belongsTo(db.country, { as: "countryData", foreignKey: "country_id"});
db.city.belongsTo(db.state, { as: "stateData", foreignKey: "state_id" });

//for customer address
db.customerAddress.belongsTo(db.state, { as: "state", foreignKey: "state_id" });
db.customerAddress.belongsTo(db.city, { as: "city", foreignKey: "city_id" });
db.customerAddress.belongsTo(db.users, { as: "user", foreignKey: "user_id" });

//for menu_cutoff_time
db.MenuItemCutoffTime.belongsTo(db.course, { as: "course", foreignKey: "course_id" });

//for tax
db.Tax.belongsTo(db.country, {as: "countryData", foreignKey: "country_id"});

//for ServicePincode
db.ServicePincode.belongsTo(db.city, { as: "city", foreignKey: "city_id" });

//for notification
db.notification.belongsTo(db.users, { as: "usersData", foreignKey: "user_id" });


module.exports = db;
