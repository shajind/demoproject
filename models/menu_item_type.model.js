module.exports = (sequelize, Sequelize) => {
    const MenuItemType = sequelize.define("menu_item_types",{
        menu_item_type_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true              
        },
        menu_item_type: {
            type: Sequelize.STRING
        },
        menu_item_type_desc: {
            type: Sequelize.STRING
        }
    });
    return MenuItemType;
};