module.exports = (sequelize, Sequelize) => {
    const Course = sequelize.define("courses",{
        course_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        course_name:{
            type: Sequelize.STRING,
            //unique: true
        },
        course_code:{
            type: Sequelize.STRING,
            //unique: true
        }
    }, {
        tableName: 'course',
        timestamps: false
    });
    return Course;
};