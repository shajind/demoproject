module.exports = (sequelize, Sequelize) => {
    const CategoryMenuItems = sequelize.define("category_menu_items",{
        category_menu_item_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        }       
    });
    return CategoryMenuItems;
};