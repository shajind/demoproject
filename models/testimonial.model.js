module.exports = (sequelize, Sequelize) => {
    const Testimonials = sequelize.define("testimonials",{
        testimonials_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        testimonial_type: {
            type: Sequelize.STRING,
            //(Values: video,text,photo)
        },
        testimonial_desc: {
            type: Sequelize.STRING
        },
        testimonial_photo_name: {
            type: Sequelize.STRING
        },
        testimonial_photo_video_name: {
            type: Sequelize.STRING
        },
        posted_by: {
            type: Sequelize.STRING
        },
        posted_contact: {
            type: Sequelize.STRING
        },
        status: {
            type: Sequelize.STRING
        }
    }, {tableName: 'testimonial'});
    return Testimonials;
};
