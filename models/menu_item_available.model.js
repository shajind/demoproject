module.exports = (sequelize, Sequelize) => {
    const MenuItemAvailable = sequelize.define("menu_item_available",{
        menu_item_available_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },        
        available_day: {
            type: Sequelize.STRING,
        },
        available_date: {
            type: Sequelize.DATEONLY,
        },
        is_recursive: {
            type: Sequelize.BOOLEAN,
            defaultValue:true
        }
    }, {tableName: 'menu_item_availability',timestamps: false});
    return MenuItemAvailable;
};