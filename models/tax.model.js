module.exports = (sequelize, Sequelize) => {
    const Tax = sequelize.define("Tax",{
        tax_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        tax_code: {
            type: Sequelize.STRING,
            allowNull: false
        },
        tax_label: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        tax_amount: {
            type: Sequelize.DOUBLE(12, 4),
            allowNull: false,
            defaultValue: 0
        },
        tax_rating: {
            type: Sequelize.DOUBLE(12, 4),
            allowNull: false,
            defaultValue: 0
        },
        tax_rating_type: {
            type: Sequelize.ENUM({
              values: ['percentage', 'fixed']
            }),
            defaultValue:"percentage"
        },
        is_active: {
            type: Sequelize.BOOLEAN,
            defaultValue:false
        }
    },{ tableName: 'tax' }, { indexes: [ { unique:true, fields: ['tax_code']} ] });
    return Tax;
};