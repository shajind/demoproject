module.exports = (sequelize, Sequelize) => {
    const UserDeviceToken = sequelize.define("user_device_tokens",{
        user_device_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        device_token: {
            type: Sequelize.STRING
        },
        device_status: {
            type: Sequelize.STRING
        }
    }, {tableName: 'user_device_token'});
    return UserDeviceToken;
};