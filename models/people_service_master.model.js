module.exports = (sequelize, Sequelize) => {
    const PeopleServiceMst = sequelize.define("people_service_msts",{
        people_service_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        people_service_lable: {
            type: Sequelize.STRING
        }
    }, { tableName: 'people_serve_mst' });
    return PeopleServiceMst;
};