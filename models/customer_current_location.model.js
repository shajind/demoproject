module.exports = (sequelize, Sequelize) => {
    const CustomerCurrentLocations = sequelize.define("customer_current_locations",{
        cust_loc_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        location_name: {
            type: Sequelize.STRING
        },
        cus_lat: {
            type: Sequelize.DECIMAL(10,8)
        },
        cus_lang: {
            type: Sequelize.DECIMAL(11,8)
        }
    }, {tableName: 'customer_current_location'});
    return CustomerCurrentLocations;
};