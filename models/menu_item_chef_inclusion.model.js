module.exports = (sequelize, Sequelize) => {
    const MenuItemChefInclusion = sequelize.define("menu_item_chef_inclusions",{
        inclusion_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true              
        },
        item_name: {
            type: Sequelize.STRING
        },
        item_position: {
            type: Sequelize.STRING
        }
       
    });
    return MenuItemChefInclusion;
};