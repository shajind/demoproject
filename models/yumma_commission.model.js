module.exports = (sequelize, Sequelize) => {
    const YummaCommission = sequelize.define("yumma_commission_mst",{
        commissionId: {
        field: 'commission_id',
        type:Sequelize.INTEGER,
        autoIncrement:true,
        primaryKey:true
    },
    commissionName: {
        field: 'commission_name',
        type: Sequelize.STRING,
        allowNull: false
    },
    commissionAmount: {
        field: 'commission_amount',
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    commissionType: {
        field: 'commission_type',
        type: Sequelize.ENUM({
            values: ['percentage', 'fixed']
        }),
        defaultValue:"percentage"
    }
    }, {tableName: 'yumma_commission_mst'});
  return YummaCommission;
};
