module.exports = (sequelize, Sequelize) => {
    const CustomerMenuItemReview = sequelize.define("customer_menuitem_review",{
        menuitem_review_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        taste_rating: {
            type: Sequelize.DECIMAL(2,1),
            allowNull: false,
            defaultValue: 0
        },
        presentation_rating: {
            type: Sequelize.DECIMAL(2,1),
            allowNull: false,
            defaultValue: 0
        },
        menu_item_id: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        order_id: {
            type: Sequelize.INTEGER,
            allowNull: true
        }
    }, {tableName: 'customer_menuitem_review',timestamps: false});
    return CustomerMenuItemReview;
};