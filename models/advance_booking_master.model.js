module.exports = (sequelize, Sequelize) => {
    const AdvanceBookingMst = sequelize.define("advance_booking_msts",{
        advance_booking_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        advance_booking_hour: {
            type: Sequelize.STRING
        }
    }, {tableName: 'advance_booking_mst'});
    return AdvanceBookingMst;
};