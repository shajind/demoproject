module.exports = (sequelize, Sequelize) => {
    const MenuItemVariety = sequelize.define("MenuItemVariety",{
        link_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true              
        },
        unit_price: {
            type: Sequelize.INTEGER,
        },
        variety_name: {
            type: Sequelize.STRING
        }      
        // unit: {
        //     type: Sequelize.STRING
        // },
        // unit_qty: {
        //     type: Sequelize.INTEGER
        // },
        // unit_qty_label: {
        //     type: Sequelize.STRING
        // }
    }, { tableName: 'menu_item_variery', timestamps: false});
    return MenuItemVariety;
};