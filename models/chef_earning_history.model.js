module.exports = (sequelize, Sequelize) => {
    const ChefEarningHistory = sequelize.define("chef_earning_history",{
    historyId: {
        field: 'history_id',
        type:Sequelize.INTEGER,
        autoIncrement:true,
        primaryKey:true
    },
    itemEarnedAmount: {
        field: 'item_earned_amount',
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    yummaCommissionAmount: {
        field: 'yumma_commission_amount',
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    yummaCommissionType: {
        field: 'yumma_commission_type',
        type: Sequelize.ENUM({
            values: ['percentage', 'fixed']
        }),
        defaultValue:"percentage"
    },
    weekNo: {
        field: 'week_no',
        type: Sequelize.INTEGER,
        allowNull: false
    },    
    status: {
        field: 'status',
        type: Sequelize.ENUM({
            values: ['paid', 'unpaid']
        }),
        defaultValue:"unpaid"
    }
    }, {tableName: 'chef_earning_history'});
  return ChefEarningHistory;
};
