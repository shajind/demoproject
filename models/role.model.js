module.exports = (sequelize, Sequelize) => {
    const Role = sequelize.define("roles",{
        role_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        role: {
            type: Sequelize.STRING,
            allowNull: false
        },
        role_code: {
            type: Sequelize.STRING,
            allowNull: false
        }
    }, {
        tableName: 'role_mst',
        indexes: [
            {unique:true, fields:['role_code']}
          ]});
    return Role;
};