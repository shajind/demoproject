module.exports = (sequelize, Sequelize) => {
    const Categorys = sequelize.define("categorys",{
        category_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        category_name: {
            type: Sequelize.STRING
        },
        category_desc: {
            type: Sequelize.STRING
        },
        status: {
            type: Sequelize.STRING
        },
        parent_category_id: {
            type: Sequelize.INTEGER,
            defaultValue:"0"
        },
        category_image: {
            type: Sequelize.STRING
        },
    }, {tableName: 'category_mst'});
    return Categorys;
};