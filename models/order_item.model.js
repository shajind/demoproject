module.exports = (sequelize, Sequelize) => {
    const OrderItem = sequelize.define("order_item",{
        itemId:{
            field: 'item_id',
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        qty: {
            field: 'qty',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        price: {
            field: 'price',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        rowTotal: {
            field: 'row_total',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        menuItemId: {
            field: 'menu_item_id',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        itemName: {
            field: 'item_name',
            type: Sequelize.STRING,
            allowNull: false
        },
        itemDescription: {
            field: 'item_description',
            type: Sequelize.STRING,
            allowNull: true
        },
        minDish: {
            field: 'min_dish',
            type: Sequelize.INTEGER,
            allowNull: true
        },
        maxDish: {
            field: 'max_dish',
            type: Sequelize.INTEGER,
            allowNull: true
        },
        kitchenTypeId: {
            field: 'kitchen_type_id',
            type: Sequelize.STRING,
            allowNull: true
        },
        kitchenType: {
            field: 'kitchen_type',
            type: Sequelize.STRING,
            allowNull: true
        },
        kitchenTypeCode: {
            field: 'kitchen_type_code',
            type: Sequelize.STRING,
            allowNull: true
        },
        chefkitchenImage: {
            field: 'chef_kitchen_image',
            type: Sequelize.STRING,
            allowNull: true
        },
        spiciness: {
            field: 'spiciness',
            type: Sequelize.STRING,
            allowNull: true
        },
        root_category_name: {
            field: 'root_category_name',
            type: Sequelize.STRING,
            allowNull: true
        },
        foodTypeId: {
            field: 'food_type_id',
            type: Sequelize.STRING,
            allowNull: true
        },
        foodType: {
            field: 'food_type',
            type: Sequelize.STRING,
            allowNull: true
        },
        foodTypeCode: {
            field: 'food_type_code',
            type: Sequelize.STRING,
            allowNull: true
        },
        menuTypeId: {
            field: 'menu_type_id',
            type: Sequelize.STRING,
            allowNull: true
        },
        menuType: {
            field: 'menu_type',
            type: Sequelize.STRING,
            allowNull: true
        },
        menuTypeCode: {
            field: 'menu_type_code',
            type: Sequelize.STRING,
            allowNull: true
        },
        chefId: {
            field: 'chef_id',
            type: Sequelize.INTEGER,
            allowNull: false
        },
        chefFullName: {
            field: 'chef_full_name',
            type: Sequelize.STRING,
            allowNull: false
        },
        menuItemImg: {
            field: 'menu_item_img',
            type: Sequelize.STRING,
            allowNull: true
        }
    }, { tableName: 'order_item', timestamps: false });
    return OrderItem;
};