module.exports = (sequelize, Sequelize) => {
    const DeliveryCharge = sequelize.define("DeliveryCharge",{
        delivery_charge_id:{
            field: 'delivery_charge_id',
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        minimum_kms: {
            field: 'minimum_kms',
            type: Sequelize.INTEGER,
            allowNull: false
        },
        min_delivery_charge: {
            field: 'min_delivery_charge',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        delivery_charge_per_km: {
            field: 'delivery_charge_per_km',
            type: Sequelize.INTEGER,
            allowNull: false
        }        
    }, {tableName: 'delivery_charge_mst', timestamps: false});
    return DeliveryCharge;
};