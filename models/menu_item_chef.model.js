module.exports = (sequelize, Sequelize) => {
    const MenuItemChef = sequelize.define("menu_item_chef",{
        menu_item_chef_id:{
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,          
        },
        item_name: {
            type: Sequelize.STRING,
        },
        item_desc: {
            type: Sequelize.STRING,
        },
        item_usp: {
            type: Sequelize.STRING,
        },
        item_img: {
            type: Sequelize.STRING,
        },
        item_price: {
            type: Sequelize.INTEGER,
        },
        overall_rating: {
            type: Sequelize.DECIMAL(2,1),
        },
        min_dish: {
            type: Sequelize.INTEGER,
        },
        max_dish: {
            type: Sequelize.INTEGER,
        },
        is_published: {
            type: Sequelize.BOOLEAN,
        },
        is_main_dish: {
            type: Sequelize.BOOLEAN,
        },
        item_product_type: {
            type: Sequelize.STRING,
        },
        time_to_prepare: {
            type: Sequelize.STRING,
        },
        category_ids:{
            type: Sequelize.STRING,
        },
        has_accompainment:{
            type:Sequelize.BOOLEAN,            
        },
        offer_id: {
            type: Sequelize.INTEGER,
        },
        deliver_time_slots:{
            type: Sequelize.STRING,
        }
    });
    return MenuItemChef;
};