    module.exports = (sequelize, Sequelize) => {
    const KitchenType = sequelize.define("kitchen_types",{
        kitchen_type_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true              
        },
        kitchen_type: {
            type: Sequelize.STRING,
            allowNull: false,
            //unique: true
        },
        kitchen_type_code: {
            type: Sequelize.STRING,
            //unique: true
        }
    }, {tableName: 'chef_kitchen_type_mst'});
    return KitchenType;
};