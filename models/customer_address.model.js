module.exports = (sequelize, Sequelize) => {
  const CustomerAddress = sequelize.define("customer_address", {
    address_id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    address_line1: {
      type: Sequelize.STRING,
    },
    address_line2: {
      type: Sequelize.STRING,
    },
    area: {
      type: Sequelize.STRING,
    },
    pincode: {
      type: Sequelize.INTEGER,
    },
    latitude: {
      type: Sequelize.DECIMAL(10,8),
    },
    longitude: {
      type: Sequelize.DECIMAL(11,8),
    },
    city_name: {
      type: Sequelize.STRING
    },
    state_name: {
      type: Sequelize.STRING
    },
    address_type: {
      type: Sequelize.STRING
      //type: Sequelize.DataTypes.ENUM("home", "office", "others"),
      //defaultValue: "home",
    },
  }, {tableName: 'customer_address'});
  return CustomerAddress;
};
