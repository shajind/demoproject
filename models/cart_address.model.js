module.exports = (sequelize, Sequelize) => {
    const CartAddress = sequelize.define("CartAddress",{
        cartAddressId:{
            field: 'cart_address_id',
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        cartId: {
            field: 'cart_id',
            type: Sequelize.INTEGER,
            allowNull: false
        },
        customerId: {
            field: 'customer_id',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        customerName: {
            field: 'customer_name',
            type: Sequelize.STRING,
            allowNull: false
        },
        customerAddress: {
            field: 'customer_address',
            type: Sequelize.STRING,
            allowNull: true
        }
    }, {tableName: 'cart_address', timestamps: false});
    return CartAddress;
};