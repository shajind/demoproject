module.exports = (sequelize, Sequelize) => {
    const OrderAddon = sequelize.define("order_addon",{
        order_addon_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        addon_name: {
            type: Sequelize.STRING
        },
        addon_image: {
            type: Sequelize.STRING
        },
        price: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        qty: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        orderId:{
            field: 'order_id',
            type:Sequelize.INTEGER
        }
    }, {tableName: 'order_addon'});
    return OrderAddon;
};