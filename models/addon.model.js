module.exports = (sequelize, Sequelize) => {
    const AddonMst = sequelize.define("addon_mst",{
        addon_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        addon_name: {
            type: Sequelize.STRING
        },
        addon_image: {
            type: Sequelize.STRING
        },
        price: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        qty: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        }
    }, {tableName: 'addon_mst'});
    return AddonMst;
};