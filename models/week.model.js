module.exports = (sequelize, Sequelize) => {
    const WeekDate = sequelize.define("chef_earning_date_mst",{
        weekId: {
        field: 'week_id',
        type:Sequelize.INTEGER,
        autoIncrement:true,
        primaryKey:true
    },
    fromDate: {
        field: 'from_date',
        type: Sequelize.DATEONLY,
        allowNull: false
    },
    toDate: {
        field: 'to_date',
        type: Sequelize.DATEONLY,
        allowNull: false
    },
    weekNum: {
        field: 'week_num',
        type: Sequelize.INTEGER,
        allowNull: false
    }
    }, {tableName: 'chef_earning_date_mst',timestamps: false});
  return WeekDate;
};
