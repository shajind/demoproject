module.exports = (sequelize, Sequelize) => {
    const Cart = sequelize.define("Cart",{
        cartId:{
            field: 'cart_id',
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        createdAt: {
            field: 'created_at',
            type: Sequelize.DATEONLY,
            allowNull: false,
            defaultValue: Sequelize.NOW
        },
        count: {
            field: 'count',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        item_count: {
            field: 'item_count',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        subtotal: {
            field: 'subtotal',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        base_total: {
            field: 'base_total',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        grand_total: {
            field: 'grand_total',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        chef_total_price: {
            field: 'chef_total_price',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        subtotal_addon: {
            field: 'subtotal_addon',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        delivery_charge: {
            field: 'delivery_charge',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        tax_amount: {
            field: 'tax_amount',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        distance_charge: {
            field: 'distance_charge',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        distance_km: {
            field: 'distance_km',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        discount: {
            field: 'discount',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        couponId: {
            field: 'coupon_id',
            type: Sequelize.INTEGER,
            allowNull: true,
            defaultValue: null
        },
        reversedOrderId: {
            field: 'reversed_order_id',
            type: Sequelize.INTEGER,
            allowNull: true,
            defaultValue: null
        },
        customerId: {
            field: 'customer_id',
            type: Sequelize.INTEGER,
            allowNull: false
        },
        deliveryDate:{
            field: 'delivery_date',
            type: Sequelize.DATEONLY,
            allowNull: true
        },
        delivery_time_slot:{
            field: 'delivery_time_slot',
            type: Sequelize.STRING,
            allowNull: true
        },
        cartType:{
            field: 'cart_type',
            type: Sequelize.STRING,
            allowNull: true,
            defaultValue:'checkout'
        },
        bookingNote: {
            field: 'booking_note',
            type: Sequelize.STRING,
            allowNull: true
        },
        spiciness: {
            field: 'spiciness',
            type: Sequelize.STRING,
            allowNull: true
        },
        isActive: {
            field: 'is_active',
            type: Sequelize.BOOLEAN,
            defaultValue:true
        },
        savedAvailabilityDeliveryTimeSlot:{
            field: 'saved_availability_delivery_time_slot',
            type: Sequelize.BOOLEAN,
            defaultValue:false
        }
    }, {tableName: 'cart'});
    return Cart;
};