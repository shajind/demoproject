module.exports = (sequelize, Sequelize) => {
    const OrderCustomers = sequelize.define("order_customers",{
        order_cust_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        cust_id: {
            type: Sequelize.INTEGER
        },
        cust_fullname: {
            type: Sequelize.STRING
        },
        cust_gender: {
            type: Sequelize.STRING
        },
        cust_dob: {
            type: Sequelize.STRING
        },
        cust_fpreference: {
            type: Sequelize.STRING
        },
        cust_profile_pic: {
            type: Sequelize.STRING
        },
        cust_cover_pic: {
            type: Sequelize.STRING
        },
        cust_address1: {
            type: Sequelize.STRING
        },
        cust_address2: {
            type: Sequelize.STRING
        },
        cust_city: {
            type: Sequelize.STRING
        },
        cust_state: {
            type: Sequelize.STRING
        },
        cust_zipcode: {
            type: Sequelize.STRING
        },        
        status: {
            type: Sequelize.STRING            
        }
    }, {tableName: 'order_customer'});
    return OrderCustomers;
};