module.exports = (sequelize, Sequelize) => {
    const Orders = sequelize.define("orders",{
        orderId:{
            field:'order_id',
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        invoiceNo:{
            field: 'invoice_no',
            type:Sequelize.STRING,
            allowNull: false,
            //unique: true      
        },
        status: {
            field: 'status',
            type: Sequelize.STRING,
            defaultValue:'open'

        },
        createdAt: {
            field: 'created_at',
            type: Sequelize.DATEONLY,
            allowNull: false,
            defaultValue: Sequelize.NOW
        },
        count: {
            field: 'count',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        itemCount: {
            field: 'item_count',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        subtotal: {
            field: 'subtotal',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        baseTotal: {
            field: 'base_total',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        grandTotal: {
            field: 'grand_total',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        chef_total_price: {
            field: 'chef_total_price',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        discount: {
            field: 'discount',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        delivery_charge: {
            field: 'delivery_charge',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        tax_amount: {
            field: 'tax_amount',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        distance_charge: {
            field: 'distance_charge',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        deliveryDate:{
            field: 'delivery_date',
            type: Sequelize.DATEONLY,
            allowNull: true
        },
        delivery_time_slot:{
            field: 'delivery_time_slot',
            type: Sequelize.STRING,
            allowNull: true
        },
        distance_km: {
            field: 'distance_km',
            type: Sequelize.DECIMAL(12,4),
            allowNull: false,
            defaultValue: 0
        },
        couponId: {
            field: 'coupon_id',
            type: Sequelize.INTEGER,
            allowNull: true,
            defaultValue: null
        },
        customerId: {
            field: 'customer_id',
            type: Sequelize.INTEGER,
            allowNull: false
        },
        bookingNote: {
            field: 'booking_note',
            type: Sequelize.STRING,
            allowNull: false
        },
        spiciness: {
            field: 'spiciness',
            type: Sequelize.STRING,
            allowNull: true
        },        
        orderStatus: {
            field: 'order_status',
            type: Sequelize.ENUM({
                values: ['ordered', 'accepted', 'cancelled','outdelivery','completed']
            }),
            defaultValue: 'ordered'
        },
    }, {tableName: 'orders'});
    return Orders;
};