module.exports = (sequelize, Sequelize) => {
    const City = sequelize.define("cities",{
        city_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        city: {
            type: Sequelize.STRING
        }
    }, {tableName: 'city_mst'});
    return City;
};