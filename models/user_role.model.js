module.exports = (sequelize, Sequelize) => {
    const UserRole = sequelize.define("user_roles",{
        user_role_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        }
    }, {
        tableName: 'user_role',
        timestamps: false
    });
    return UserRole;
};