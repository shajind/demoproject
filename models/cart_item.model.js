module.exports = (sequelize, Sequelize) => {
    const CartItem = sequelize.define("CartItem",{
        cartItemId:{
            field: 'cart_item_id',
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        cartId: {
            field: 'cart_id',
            type: Sequelize.INTEGER,
            allowNull: false
        },
        qty: {
            field: 'qty',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        price: {
            field: 'price',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        chef_price: {
            field: 'chef_price',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        chef_total_price: {
            field: 'chef_total_price',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        rowTotal: {
            field: 'row_total',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        menuItemId: {
            field: 'menu_item_id',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        itemName: {
            field: 'item_name',
            type: Sequelize.STRING,
            allowNull: false
        },
        itemDescription: {
            field: 'item_description',
            type: Sequelize.STRING,
            allowNull: true
        },
        minDish: {
            field: 'min_dish',
            type: Sequelize.INTEGER,
            allowNull: true
        },
        maxDish: {
            field: 'max_dish',
            type: Sequelize.INTEGER,
            allowNull: true
        },
        kitchenTypeId: {
            field: 'kitchen_type_id',
            type: Sequelize.STRING,
            allowNull: true
        },
        kitchenType: {
            field: 'kitchen_type',
            type: Sequelize.STRING,
            allowNull: true
        },
        kitchenTypeCode: {
            field: 'kitchen_type_code',
            type: Sequelize.STRING,
            allowNull: true
        },
        foodTypeId: {
            field: 'food_type_id',
            type: Sequelize.STRING,
            allowNull: true
        },
        foodType: {
            field: 'food_type',
            type: Sequelize.STRING,
            allowNull: true
        },
        foodTypeCode: {
            field: 'food_type_code',
            type: Sequelize.STRING,
            allowNull: true
        },
        spiciness: {
            field: 'spiciness',
            type: Sequelize.STRING,
            allowNull: true
        },
        root_category_name: {
            field: 'root_category_name',
            type: Sequelize.STRING,
            allowNull: true
        },
        variety_link_id: {
            field: 'variety_link_id',
            type: Sequelize.STRING,
            allowNull: true
        },
        menuTypeId: {
            field: 'menu_type_id',
            type: Sequelize.STRING,
            allowNull: true
        },
        menuType: {
            field: 'menu_type',
            type: Sequelize.STRING,
            allowNull: true
        },
        addonSubTotal: {
            field: 'addon_subtotal',
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        menuTypeCode: {
            field: 'menu_type_code',
            type: Sequelize.STRING,
            allowNull: true
        },       
        chefId: {
            field: 'chef_id',
            type: Sequelize.INTEGER,
            allowNull: true
        },
        chefFullName: {
            field: 'chef_full_name',
            type: Sequelize.STRING,
            allowNull: true
        },
        chefkitchenImage: {
            field: 'chef_kitchen_image',
            type: Sequelize.STRING,
            allowNull: true
        },
        menuItemImg: {
            field: 'menu_item_img',
            type: Sequelize.STRING,
            allowNull: true
        },
        menuParentId: {
            field: 'menu_parent_id',
            type: Sequelize.STRING,
            allowNull: true
        }
    }, {tableName: 'cart_item', timestamps: false});
    return CartItem;
};