module.exports = (sequelize, Sequelize) => {
    const OrderChefStatus = sequelize.define("OrderChefStatus",{
      order_chef_id:{
        type:Sequelize.INTEGER,
        autoIncrement:true,
        primaryKey:true              
      },
      rejected_reason:{
        type:Sequelize.STRING,
        defaultValue:null              
      },
      status: {
        type: Sequelize.ENUM({
          values: ['accepted','outdelivery', 'rejected','completed']
        })
      },
    }, {tableName: 'order_chef_status'});
  return OrderChefStatus;
};