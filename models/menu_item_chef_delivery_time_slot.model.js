module.exports = (sequelize, Sequelize) => {
    const MenuItemChefDeliveryTimeSlot = sequelize.define("menu_item_chef_delivery_time_slot",{
        chef_delivery_id:{
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,          
        },
    });
    return MenuItemChefDeliveryTimeSlot;
};