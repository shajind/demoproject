module.exports = (sequelize, Sequelize) => {
    const Chefs = sequelize.define("chefs",{
        chef_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        chef_fullname: {
            type: Sequelize.STRING
        },
        chef_gender: {
            type: Sequelize.STRING
        },
        chef_dob: {
            type: Sequelize.STRING
        },
        chef_cuisine: {
            type: Sequelize.STRING
        },
        chef_profile_pic: {
            type: Sequelize.STRING
        },
        chef_profile_cover_pic: {
            type: Sequelize.STRING
        },
        kitchen_image: {
            type: Sequelize.STRING
        },
        chef_address1: {
            type: Sequelize.STRING
        },
        chef_address2: {
            type: Sequelize.STRING
        },
        // chef_city: {
        //     type: Sequelize.STRING
        // },
        // chef_state: {
        //     type: Sequelize.STRING
        // },
        chef_zipcode: {
            type: Sequelize.STRING
        },
        chef_type: {
            type: Sequelize.ENUM({
                values: ['home_chef', 'yumma_chef']
            }),
            defaultValue:"yumma_chef"
        },
        chef_about_yourself: {
            type: Sequelize.STRING,
            allowNull: true
        }, 
        chef_kitchen_timing: {
            type: Sequelize.STRING,
            allowNull: true
        },
        chef_pure_veg_kitchen: {
            type: Sequelize.STRING,
            allowNull: false,
            defaultValue: 'No'
        },
        chef_lat: {
            type: Sequelize.DECIMAL(10,8)
        },
        chef_lang: {
            type: Sequelize.DECIMAL(11,8)
        },  
        status: {
            type: Sequelize.STRING,
            defaultValue:'active'
        },
        overall_rating: {
            type: Sequelize.DECIMAL(2,1),
            allowNull: false,
            defaultValue: 0
        },
        total_rating_count: {
            type: Sequelize.INTEGER,
            defaultValue: 0
        }
    }, {tableName: 'chef_mst'});
    return Chefs;
};