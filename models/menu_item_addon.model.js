module.exports = (sequelize, Sequelize) => {
    const AddonMenuItem = sequelize.define("menu_items_addon", {
      link_addon_id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      }      
    }, {tableName: 'menu_item_addon', timestamps: false});
    return AddonMenuItem;
  };