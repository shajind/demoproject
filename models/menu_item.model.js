module.exports = (sequelize, Sequelize) => {
  const MenuItem = sequelize.define("menu_items", {
    menu_item_id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    menu_item_name: {
      type: Sequelize.STRING,
    },
    menu_item_desc: {
      type: Sequelize.STRING,
    },
    menu_item_usp: {
      type: Sequelize.STRING,
    },
    menu_item_img: {
      type: Sequelize.STRING,
    },
    menu_item_price: {
      type: Sequelize.INTEGER,
      defaultValue:0
    },
    menu_overall_rating: {
      type: Sequelize.DECIMAL(2,1),
      defaultValue:0
    },
    min_dish: {
      type: Sequelize.INTEGER,
    },
    max_dish: {
      type: Sequelize.INTEGER,
    },
    is_visible: {
      type: Sequelize.BOOLEAN,
      defaultValue:true
    },
    is_deleted: {
      type: Sequelize.BOOLEAN,
      defaultValue:false
    },
    is_main_dish: {
      type: Sequelize.BOOLEAN,
    },
    has_addon: {
      type: Sequelize.ENUM({
        values: ['yes', 'no']
      }),
      defaultValue:"no"
    },
    is_pure_veg_kitchen: {
      type: Sequelize.BOOLEAN,
      defaultValue:false
    },
    menu_item_type: {
      type: Sequelize.ENUM({
        values: ['simple', 'grouped','configurable','virtual','bundle']
      }),
      defaultValue:"simple"
    },
    time_to_prepare: {
      type: Sequelize.STRING,
    },    
    yumma_price: {
      type: Sequelize.INTEGER,
      defaultValue:0
    },
    yumma_price_type: {
      type: Sequelize.STRING,
      // type: Sequelize.ENUM({
      //   values: ['percentage', 'fixed']
      // }),
      // defaultValue:"percentage"
    },
    spiciness: {
      type: Sequelize.STRING,
    },
    position_size:{
      type: Sequelize.STRING,
    },
    has_variety: {
      type: Sequelize.BOOLEAN,
      defaultValue:false
    },
    yumma_recommend: {
      type : Sequelize.BOOLEAN,
      defaultValue : false
    },
    total_rating_count: {
        type: Sequelize.INTEGER,
        defaultValue: 0
    }
  }, {tableName: 'menu_item'});
  return MenuItem;
};
