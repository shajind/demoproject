module.exports = (sequelize, Sequelize) => {
    const MenuItemCategory = sequelize.define("MenuItemCategory",{
        menu_item_category_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true  
        }
    }, {
        tableName: 'menu_item_category',
        timestamps: false
    });
    return MenuItemCategory;
};