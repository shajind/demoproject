module.exports = (sequelize, Sequelize) => {
    const MenuType = sequelize.define("menu_types",{
        menu_type_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        menu_type: {
            type: Sequelize.STRING
        },
        menu_type_desc: {
            type: Sequelize.STRING
        },
        menu_type_code: {
            type: Sequelize.STRING,
            //unique: true
        }
    }, {tableName: 'menu_type_mst'});
    return MenuType;
};