module.exports = (sequelize, Sequelize) => {
    const Coupons = sequelize.define("coupons",{
        coupon_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        offer_name: {
            type: Sequelize.STRING
        },
        offer_code: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        },
        discount: {
            type: Sequelize.STRING
        },
        minimum_amount: {
            type: Sequelize.INTEGER
        },
        offer_validity_from: {
            type: Sequelize.DATEONLY  
        },
        offer_validity_to: {
            type: Sequelize.DATEONLY
        },
        offer_status: {
            type: Sequelize.STRING
        }
        
    }, {tableName: 'coupon'});
    return Coupons;
};