module.exports = (sequelize, Sequelize) => {
    const notification_log = sequelize.define("notification_log",{
        notify_log_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        notify_delivery_date: {
            type: Sequelize.DATE(3),
            defaultValue: Sequelize.literal('CURRENT_TIMESTAMP(3)'),
        },
        notifi_status: {
            type: Sequelize.STRING,
            defaultValue:'Unaccept'
        },
        
    }, {tableName: 'notification_history'});
    return notification_log;
};