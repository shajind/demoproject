module.exports = (sequelize, Sequelize) => {
    const MenuItemInclusion = sequelize.define("MenuItemInclusion",{
        inclusion_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true              
        },
        item_name: {
            type: Sequelize.STRING
        },
        item_portion: {
            type: Sequelize.STRING
        }
    }, { tableName: 'menu_item_inclusion', timestamps: false});
    return MenuItemInclusion;
};