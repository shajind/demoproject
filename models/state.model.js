module.exports = (sequelize, Sequelize) => {
    const State = sequelize.define("state",{
        state_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        state: {
            type: Sequelize.STRING
        }
    },{tableName: 'state_mst'});
    return State;
};