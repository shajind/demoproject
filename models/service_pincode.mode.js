module.exports = (sequelize, Sequelize) => {
    const Role = sequelize.define("servicable_pincode_mst",{
        service_pincode_id:{
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true      
        },
        pincode: {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    }, {tableName: 'servicable_pincode_mst',timestamps: false},{ indexes: [ { unique:true, fields: ['pincode']} ] });
    return Role;
};