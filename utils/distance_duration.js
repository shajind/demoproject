var request = require('request');
const GOOGLE_MAPS_APIKEY = 'AIzaSyCeyGbs9rndtx6TEG9cOF9tOkQA7vjy6CU';
const geolib = require('geolib');

exports.distance = (origin,destination)=>{
    return new Promise((resolve, reject) => {
    let distance;
        request('https://maps.googleapis.com/maps/api/directions/json?'+'origin='+origin+"&destination="+destination +"&sensor=false&key="+GOOGLE_MAPS_APIKEY,{ json: true }, (err, res, body) => {
            //console.log('body=',body);    
            distance=body.routes[0].legs[0].distance.text;
            //duration=body.routes[0].legs[0].duration.text;
            let distanceSplit=distance.split(',');
            let distanceKM="";
            for(let i=0;i<distanceSplit.length;i++){
                distanceKM=distanceKM+distanceSplit[i]
            }
            console.log('distance=',distanceKM);
            //console.log('duration=',duration);
            resolve(distanceKM)
        });    
    })
}


exports.distanceRadius = (customerLocation,chefLocation,distance)=>{
    let result=geolib.isPointWithinRadius(
        customerLocation,  // { latitude: 51.525, longitude: 7.4575 },
        chefLocation,  // { latitude: 51.5175, longitude: 7.4678 },
        distance*1000  // 5000 
    );
    return result
}


exports.distanceCalculation= (customerLocation,chefLocation)=>{
    let result=geolib.getDistance(customerLocation,chefLocation);
    return result/1000;
}
