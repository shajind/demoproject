var jwt = require('jwt-simple');
exports.genToken = (user_id, role_code)=>{
    var expires = expiresIn(30); // 30 days
    var token = jwt.encode({
        exp: expires,
        user_id: user_id,
        role_code: role_code
    }, require('../config/secret')());
    return token;
}

function expiresIn(numDays) {
    var dateObj = new Date();

    return dateObj.setDate(dateObj.getDate() + numDays);
}