const generator_URL ="https://devapi.yummafood.in/image/";


exports.menuItemImage_URL = ()=>{
    let image = generator_URL+'menuItemImg/';
    return image;
}

exports.customerImage_URL = ()=>{
    let image = generator_URL+'customerProfile/';
    return image;
}

exports.menuItemChefImg_URL = ()=>{
    let image = generator_URL+'menuItemChefImg/';
    return image;
}

exports.setChefImagePath = () => {
    let image = generator_URL+'chef/'
    return image;
}

exports.setChefDocumentPath = () => {
    let image = generator_URL+'chef/documents/'
    return image;
}

exports.setbannertPath = () => {
    let image = generator_URL+'bannerImage/'
    return image;
}

exports.setcategoryPath = () => {
    let image = generator_URL+'categoryImg/'
    return image;
}

exports.setTestimonialPath = () => {
    let image = generator_URL+'testimonial/'
    return image;
}

exports.setCustomerReviewFilePath = () => {
    let image = generator_URL+'customer_review_file/'
    return image;
}