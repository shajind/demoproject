const model = require('../models');
const testimonialModel = model.testimonial;
const imageURL = require("../utils/image_URL-generator");

exports.saveTestimonial = (payload) => {    
    return new Promise((resolve, reject) => { 
        testimonialModel.create(payload).then(response => {                  
            console.log(response);
        	resolve(response);
        },err => {                    
        	reject({error:err});
        });
    });
};

let parseData = (data) => {
    let res = [];
    data.map((row) => {
      let resultObj = {};
      resultObj["testimonials_id"] = row.testimonials_id;
      resultObj["testimonial_type"] = row.testimonial_type;
      resultObj["testimonial_desc"] = row.testimonial_desc;
      resultObj["testimonial_photo_name"] = row.testimonial_photo_name;
      resultObj["testimonial_photo_video_name"] = row.testimonial_photo_video_name;
      resultObj["posted_by"] = row.posted_by;
      resultObj["posted_contact"] = row.posted_contact;
      resultObj["status"] = row.status;
      resultObj["banner_Image_URL"] = imageURL.setTestimonialPath()+ row.testimonial_photo_name;
      res.push(resultObj);
    });
    return res;
};


exports.getAllTestimonial = (id) =>{    
    return new Promise((resolve,reject) => {               
        testimonialModel.findAll().then(testimonialData =>{
            let data = parseData(testimonialData);
            console.log(data);            
            resolve(data);            
        },err => {                    
        	reject({error:err});
        });
    });
};

exports.getTestimonialById = (id) =>{    
    return new Promise((resolve,reject) => {               
        testimonialModel.findOne({ where : {testimonials_id:id}}).then( testimonialData=>{
            let data = parseData(testimonialData);            
            resolve(data);            
        },err => {                    
        	reject({error:err});
        });
    });
};