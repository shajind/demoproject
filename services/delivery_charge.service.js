const deliveryChargeDAO = require("../dao/delivery_charge.dao");



exports.createDeliveryCharge = (data) => {
  return new Promise((resolve, reject) => {
    deliveryChargeDAO.saveDeliveryCharge(data)
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};


exports.updateDeliveryCharge = (deliveryChargeID,deliveryCharge) => {
    return new Promise((resolve, reject) => {
        deliveryChargeDAO.updateDeliveryCharge(deliveryChargeID,deliveryCharge)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  };

exports.findAllDeliveryCharge = () => {
  return new Promise((resolve, reject) => {
    deliveryChargeDAO.findAllDeliveryCharge().then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};
  