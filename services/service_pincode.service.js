const ServicePincodeDAO = require("../dao/service_pincode.dao");



exports.createServicePincode = (payload) => {
    return new Promise((resolve, reject) => {
        let data = payloadServicePincode(payload);
        ServicePincodeDAO.saveServicePincode(data).then((res) => {
            resolve(res);
        }).catch((err) => {
            reject(err);
        });
    });
};


function payloadServicePincode(payload) {
    let results=[];
    let cityId=payload['city_id'];
    let pincode=payload['pincode'];
    for (let i = 0; i < pincode.length; i++) {
      pincodeService={};
      pincodeService["pincode"]=pincode[i];
      pincodeService["city_id"]=cityId;
      results.push(pincodeService);
    }
    return results;
}

exports.findAllServicePincode = () => {
    return new Promise((resolve, reject) => {
        ServicePincodeDAO.findAllServicePincode().then((res) => {
            resolve(res);
        }).catch((err) => {
            reject(err);
        });
    });
};
  
exports.fetchServicePincodeByPincode = (pincode) => {
    return new Promise((resolve, reject) => {
        ServicePincodeDAO.findServicePincodeByPincode(pincode).then((res) => {
            resolve(res);
        }).catch((err) => {
            reject(err);
        });
    });
};