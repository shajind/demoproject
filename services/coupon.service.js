const couponDAO = require("../dao/coupon.dao");

exports.saveCoupon = (coupondata) => {    
    return new Promise((resolve, reject) => {        
        couponDAO.createCoupon(coupondata).then(response => { 
        	resolve(response);
        },err => {                    
        	reject({error:err});
        });
    });
};

exports.getCoupon = () =>{    
    return new Promise((resolve,reject) => {               
        couponDAO.getCoupon().then(data =>{
            resolve(data);            
        },err => {                    
        	reject({error:err});
        });
    });
};