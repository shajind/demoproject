const { response } = require("express");
const model = require("../models");
const customerAddressModel = model.customerAddress;

exports.saveAddresses = (addressData) => {
  return new Promise((resolve, reject) => {
    customerAddressModel
      .create(addressData)
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

exports.getAllAddress = (userId) => {
  return new Promise((resolve, reject) => {
    if (!userId) {
      reject("User id not found");
    }
    customerAddressModel
      .findAll({
        where: { user_id: userId },["order"]:[['address_id','DESC']]
      })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        console.log("address service catch ", err);
        reject(err);
      });
  });
};

exports.deleteAddress = (addressId) => {
  return new Promise((resolve, reject) => {
    customerAddressModel
      .destroy({
        where: { address_id: addressId },
      })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

exports.updateAddress = (addressId, address) => {
  return new Promise((resolve, reject) => {
    customerAddressModel
      .update(address, {
        where: { address_id: addressId },
      })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

exports.fetchCustomerAddress = (customerAddressId) => {
  return new Promise((resolve, reject) => {
    if (!customerAddressId) {
      throw new Error("customer_address id not found");
    }
    customerAddressModel.findOne({where: { address_id: customerAddressId }}).then((res) => {
      if (!res) {
        throw new Error("customer_address id not found");
      }
      resolve(res);
    }).catch((err) => {
      reject(err);
    });
  });
};


exports.saveCustomerAddressHome = (userId,customerAdrressData) => {
  return new Promise((resolve, reject) => {
    customerAddressModel.findOne({where: { user_id: userId,address_type:'home'}}).then((res) => {
      if(res){
        return customerAddressModel.update(customerAdrressData,{where: { user_id: userId,address_type:'home' }});
      }else{
        return customerAddressModel.create(customerAdrressData);
      }
    }).then(response=>{
      resolve(response);
    }).catch((err) => {
      reject(err);
    });
  });
};