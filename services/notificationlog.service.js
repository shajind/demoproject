const model = require('../models');
const notificationLogModel = model.notificationlog;


exports.saveNotificationLog = (payload) => {    
    return new Promise((resolve, reject) => {        
    console.log("payload",payload)
        notificationLogModel.create(payload).then(response => {                  
            console.log(response);
        	resolve(response);
        }).catch(err => {                    
        	reject({error:err});
        });
    });
};

exports.getAllNotificationLog = (id) =>{    
    return new Promise((resolve,reject) => {               
        notificationLogModel.findAll().then(data =>{
            console.log(data);            
            resolve(data);            
        }).catch(err => {                    
        	reject({error:err});
        });
    });
};

exports.getAllNotificationLogByUserId = (id) =>{    
    return new Promise((resolve,reject) => {               
        notificationLogModel.findOne({ where : {notification_id:id}}).then(data =>{
            console.log(data);            
            resolve(data);            
        },err => {                    
        	reject({error:err});
        });
    });
};