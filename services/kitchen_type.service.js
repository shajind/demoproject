const kitchenTypeDAO = require("../dao/kitchen_type.dao");
const core = require("../helpers/core.helper");



exports.createKitchenType = (data) => {
  return new Promise((resolve, reject) => {
      let kitchenTypeCode = core.createCode(data['kitchen_type']);
      data['kitchen_type_code'] = kitchenTypeCode;
      kitchenTypeDAO.saveKitchenType(data)
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

exports.findAllKitchenTypes = () => {
  return new Promise((resolve, reject) => {
      kitchenTypeDAO.findAllKitchenTypes().then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};
  