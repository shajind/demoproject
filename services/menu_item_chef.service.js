const model = require('../models');
const MenuItemChef = model.menu_item_chef;
const MenuItemChefInclusion = model.menu_item_chef_inclusion;
const menuChefDeliverTimeSlots=model.menu_item_chef_delivery_time_slot;
const categoryMenuItemModel=model.category_menu_item;
const MenuType = model.menu_types;
const kitchenType=model.kitchen_type;
const MenuItemType = model.menu_item_types;
const chefModel = model.chefs;
const AdvanceBookingModel = model.advanceBookingMaster;
const PeopleServiceModel = model.peopleServiceMaster;
const imageURL = require("../utils/image_URL-generator");

let saveMenuItemChefValues=(data)=>{
    let menuItemChefId=data["menu_item_chef_id"];  
    if(data.category_ids){
        let category_ids=data["category_ids"].split(",");
        let categoryData=[];
        for (let i = 0; i < category_ids.length; i++) {
            resultObj={};
            resultObj["category_id"]=category_ids[i];
            resultObj["menu_item_chef_id"]=menuItemChefId;
            categoryData.push(resultObj);
        }
        categoryMenuItemModel.bulkCreate(categoryData);        
    }
    if(data.deliver_time_slots){
        let deliver_time_slots=data["deliver_time_slots"].split(",");
        let deliverTimeSlotData=[];
        for (let i = 0; i < deliver_time_slots.length; i++) {
        resultObj={};
        resultObj["slot_id"]=deliver_time_slots[i];
        resultObj["menu_item_chef_id"]=menuItemChefId;
        deliverTimeSlotData.push(resultObj);
        }
        menuChefDeliverTimeSlots.bulkCreate(deliverTimeSlotData); 
    }
    return 
}

exports.saveMenuItemChef = (menu_item_chef_data) => {    
    return new Promise((resolve, reject) => {        
        MenuItemChef.create(menu_item_chef_data).then(response => {
            let date=saveMenuItemChefValues(response);
        	resolve(response);
        },err => {                    
        	reject({error:err});
        });
    });
};



let parseData = (data) => {
    let res = [];
    data.map((row) => {
      let resultObj = {};
      resultObj["menu_item_chef_id"] = row.menu_item_chef_id;
      resultObj["item_name"] = row.item_name;
      resultObj["item_desc"] = row.item_desc;
      resultObj["item_usp"] = row.item_usp;
      resultObj["item_img"] = row.item_img;
      resultObj["item_price"] = row.item_price;
      resultObj["overall_rating"] = row.overall_rating;
      resultObj["min_dish"] = row.min_dish;
      resultObj["max_dish"] = row.max_dish;
      resultObj["is_published"] = row.is_published;
      resultObj["is_main_dish"] = row.is_main_dish;
      resultObj["item_product_type"] = row.item_product_type;
      resultObj["time_to_prepare"] = row.time_to_prepare;
      resultObj["category_ids"] = row.category_ids;
      resultObj["has_accompainment"] = row.has_accompainment;
      resultObj["offer_id"] = row.offer_id;
      resultObj["deliver_time_slots"] = row.deliver_time_slots;
      resultObj["advance_booking_id"] = row.advance_booking_id;
      if (row.advanceBookingData) {
        resultObj["advanceBookingData"] =row.advanceBookingData;
      }
      resultObj["people_serve_id"] = row.people_serve_id;
      if (row.peopleServeData) {
        resultObj["peopleServeData"] =row.peopleServeData;
      }
      if (row.menuTypeData) {
        resultObj["menuTypeData"] =row.menuTypeData;
      }
      resultObj["menu_item_type_id"] = row.menu_item_type_id;
      if (row.menuItemTypeData) {
        resultObj["menuItemTypeData"] =row.menuItemTypeData;
      }    
      resultObj["chef_id"] = row.chef_id;
      if (row.chefData) {
        resultObj["chefData"] = row.chefData;
      }
      resultObj["kitchen_type_id"] = row.kitchen_type_id;
      if (row.kitchenTypeData) {
        resultObj["kitchenTypeData"] = row.kitchenTypeData;
      }

      if(row.item_img) {
          resultObj["item_img_URL"] = imageURL.menuItemChefImg_URL() + row.item_img;
      }
      res.push(resultObj);
    });
    return res;
};
exports.getMenuItemChef = () =>{    
    return new Promise((resolve,reject) => {               
        MenuItemChef.findAll({["order"]:[['menu_item_chef_id','DESC']],
        include:[{model: kitchenType,as: 'kitchenTypeData'},
        {model: chefModel,as: 'chefData'},
        {model: MenuItemType,as: 'menuItemTypeData'},
        {model: MenuType,as: 'menuTypeData'},
        {model: AdvanceBookingModel,as: 'advanceBookingData'},
        {model: PeopleServiceModel,as: 'peopleServeData'}]}).then(data =>{
            let response=parseData(data)
            //console.log(data);            
            resolve(response);            
        },err => {                    
        	reject({error:err});
        });
    });
};
let chefInclusionData=(inclusionValue,menuItemChefData)=>{
    let data=inclusionValue;
    let res = [];
    data.map((row) => {
        let resultObj = {};
        resultObj["item_name"] = row.item_name;
        resultObj["item_position"] = row.item_position;
        resultObj["menu_item_chef_id"] = menuItemChefData;
        res.push(resultObj);
    });
    return res;
};
exports.saveMenuItemChefInclusion = (inclusionValue,menuItemChefData) => {    
    return new Promise((resolve, reject) => {
        let inclusionData=chefInclusionData(inclusionValue,menuItemChefData);
        MenuItemChefInclusion.bulkCreate(inclusionData).then(response => {                  
            console.log(response);
        	resolve(response);
        },err => {                    
        	reject({error:err});
        });
    });
};
exports.saveDeliverTimeSlot = (inclusionValue,menuItemChefData) => {    
    return new Promise((resolve, reject) => {
        let inclusionData=chefInclusionData(inclusionValue,menuItemChefData);
        MenuItemChefInclusion.bulkCreate(inclusionData).then(response => {                  
            console.log(response);
        	resolve(response);
        },err => {                    
        	reject({error:err});
        });
    });
};