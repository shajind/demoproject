const addonDAO = require("../dao/addon.dao");
const imageURL = require("../utils/image_URL-generator");


exports.createAddon = (data) => {
    return new Promise((resolve, reject) => {
      addonDAO.saveAddon(data).then((res) => {
          resolve(res);
      }).catch((err) => {
          reject(err);
      });
    });
};


exports.findAllAddon = () => {
    return new Promise((resolve, reject) => {
        addonDAO.findAllAddon().then((res) => {
          let result=addonParse(res);
          resolve(result);
        })
        .catch((err) => {
          reject(err);
        });
    });
};

function addonParse(data){
  let res=[];
  data.map((row) => {
    let result=row;
    result.dataValues['addon_image_URL']=imageURL.menuItemImage_URL()+row.addon_image;
    res.push(result);
  });
  return res;
}

function parseAddonData(data) {
  let res = [];
  data.map((row) => {
    let result={};
    result["addon_id"] = row.addon.addon_id;
    result["addon_name"] = row.addon.addon_name;
    result["addon_image"] = row.addon.addon_image;
    result["price"] = row.addon.price;
    result["qty"] = row.addon.qty;
    result["addon_image_URL"] = imageURL.menuItemImage_URL()+row.addon.addon_image;
    res.push(result);
  });
  return res;
};

exports.fetchAddonMenuItem = (menuItemId) => {
  return new Promise((resolve, reject) => {
      addonDAO.findMenuItemAddon(menuItemId).then((res) => {
        let result=parseAddonData(res)
        resolve(result);
      })
      .catch((err) => {
        reject(err);
      });
  });
};
