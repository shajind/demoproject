const orderStatusDAO = require('../dao/order_status.dao');

exports.saveOrderStatus = (payload) =>{    
    return new Promise((resolve,reject) => {               
        orderStatusDAO.createOrderStatus(payload).then(data =>{          
            resolve(data);            
        }).catch(err => {                    
        	reject(err);
        });
    });
};

exports.fetchAllOrderStatus = () => {    
    return new Promise((resolve,reject) => {               
        orderStatusDAO.getAllOrderStatus().then(data =>{         
            resolve(data);            
        }).catch(err => {                    
        	reject({error:err});
        });
    });
};