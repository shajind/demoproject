const model = require('../models');
const DeliveryTimeSlotMaster = model.deliveryTimeSlotMaster;

exports.deliveryTimeSlotMstSave = (deliveryTimeSlotData) => {    
    return new Promise((resolve, reject) => {        
        DeliveryTimeSlotMaster.create(deliveryTimeSlotData).then(response => {                  
            console.log(response);
        	resolve(response);
        }).catch(err => {                    
        	reject({error:err});
        });
    });
};

exports.getAllDeliveryTimeSlot = (filters) =>{
    let whereClause = {};
    if (filters) {    
        let conditions = JSON.parse(filters);
        for (let i = 0; i < conditions.length; i++) {
            let field = conditions[i]["field"];
            let predicate = conditions[i]["predicate"];
            let value = conditions[i]["value"];
            whereClause[field] = value;
        }
    }    
    return new Promise((resolve,reject) => {               
        DeliveryTimeSlotMaster.findAll({where:whereClause}).then(data =>{
            console.log(data);            
            resolve(data);            
        }).catch(err => {                    
        	reject({error:err});
        });
    });
};

exports.fetchDeliveryTimeSlot = (slotId) =>{
    return new Promise((resolve,reject) => {               
        DeliveryTimeSlotMaster.findOne({where:{slot_id:slotId}}).then(data =>{
            if(!data){
                throw new Error("This Delivery slot is not Defined");
            }
            resolve(data);            
        }).catch(err => {                    
        	reject(err);
        });
    });
};