const model = require('../models');
const notificationModel = model.notification;


exports.saveNotification = (payload) => {    
    return new Promise((resolve, reject) => {        
        notificationModel.create(payload).then(response => {                  
            console.log(response);
        	resolve(response);
        }).catch(err => {                    
        	reject({error:err});
        });
    });
};

exports.fetchAllNotifications = (filters) =>{
    let whereClause = {};
    if(filters){
        let conditions = JSON.parse(filters);
        for (let i = 0; i < conditions.length; i++) {
        let field = conditions[i]["field"];
        let predicate = conditions[i]["predicate"];
        let value = conditions[i]["value"];
        whereClause[field] = value;
        }
    }    
    return new Promise((resolve,reject) => {         
        notificationModel.findAll({where:whereClause}).then(data =>{
            console.log(data);            
            resolve(data);            
        }).catch(err => {                    
        	reject({error:err});
        });
    });
};

exports.fetchNotificationById = (notificationId) =>{    
    return new Promise((resolve,reject) => {               
        notificationModel.findOne({where:{notification_id: notificationId}}).then(data =>{
            console.log(data);            
            resolve(data);            
        }).catch(err => {
        	reject({error:err});
        });
    });
};