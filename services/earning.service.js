const earningDAO = require('../dao/earning.dao');

exports.findTotalEarnings = (chefId) => {
    return new Promise((resolve, reject) => {
        earningDAO.fetchEarningChef(chefId).then(data=>{
            return Promise.all([
                earningDAO.sumTotalEarningInWeek(data["earnedId"]),
                earningDAO.sumTotalEarningInMontly(data["earnedId"]),
                earningDAO.sumTotalEarningInYear(data["earnedId"]),
                earningDAO.sumTotalEarning(data["earnedId"])

            ]);
        }).then(([week,month,year,total]) => {
            let result=earningTotals(week,month,year,total)
            resolve(result);
        }).catch((err) => {
            console.log('error i is ' + err);
            reject(err);
        })
    });
};

function earningTotals(week,month,year,total) {
    let currentDay = new Date();
    let currentMonth = currentDay.getMonth()+1;
    let currentYear = currentDay.getFullYear();
    let oneJan =  new Date(currentYear, 0, 1);
    let numberOfDays =  Math.floor((currentDay - oneJan) / (24 * 60 * 60 * 1000)); 
    let currentWeek = Math.ceil(( currentDay.getDay() + 1 + numberOfDays) / 7);
    console.log(currentWeek,currentMonth,currentYear)
    let resultObj = {};
   if(week){ 
        week.map((row) => {
            if ( row['weekNo'] == currentWeek ) {
                resultObj["week"] = row;
            }else{
                resultObj["week"] = null;
            }
        })        
    }
    if(month){        
        month.map((row) => {
            if (row['monthly'] == currentMonth && row['year'] == currentYear) {
                resultObj["month"] = row;
            }else{
                resultObj["month"] = null;
            }
        })        
    }
    if(year){        
        year.map((row) => {
            if (row['year'] == currentYear) {
                resultObj["year"] = row;
            }else{
                resultObj["year"] = null;
            }
        })        
    }
    if(total){ 
        resultObj["total_amount"] = total["total_amount"];
    }
    return resultObj;
}

exports.findEarningDetails = (chefId, filter) => {
    return new Promise((resolve, reject) => {
        earningDAO.fetchEarningChef(chefId).then(data=>{
            if (filter=='week'){
                return Promise.all([earningDAO.sumTotalEarningInWeek(data["earnedId"]),earningDAO.sumTotalEarning(data["earnedId"])]);
            }
            if (filter=='month'){
                return Promise.all([earningDAO.sumTotalEarningInMontly(data["earnedId"]),earningDAO.sumTotalEarning(data["earnedId"])]);
            }
            if (filter=='year'){
                return Promise.all([earningDAO.sumTotalEarningInYear(data["earnedId"]),earningDAO.sumTotalEarning(data["earnedId"])]);
            }else{
                return Promise.all([earningDAO.sumTotalEarningInDate(data["earnedId"]),earningDAO.sumTotalEarning(data["earnedId"])]);
            }
        }).then(([item,data]) => {
            console.log(data);
            let result={"items":item,"total_amount":data['total_amount']};
            resolve(result);
        }).catch((err) => {
            console.log('error i is ' + err);
            reject(err);
        })
    });
};


exports.saveWeekDate = (weekYear) => {
    let results=[];
    let week=53;
    for (let i = 1; i <= week; i++) {
        let weekNumber = i+" "+weekYear ;
        let weekYearArr = weekNumber.split(" ").map(n => parseInt(n))
        let weekOut = getISOWeek(...weekYearArr) ;        
        let payload={};
        payload['fromDate']=weekOut[0];
        payload['toDate']=weekOut[6];
        payload['weekNum']=i;
        var date = new Date(payload['fromDate']);
        let year = date.getFullYear();
        if(year>weekYear){
            console.log(year);
            break
        }
        results.push(payload);
    }
    console.log(results);
    return new Promise((resolve, reject) => {
        earningDAO.createWeekDate(results).then(data=>{
            resolve(data);
        }).catch((err) => {
            reject(err);
        })
    });
};


function getISOWeek(w, y) {
    var simple = new Date(y, 0, 1 + (w - 1) * 7);
    var dow = simple.getDay();
    var ISOweekStart = simple;
    if (dow <= 4)
        ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
    else
        ISOweekStart.setDate(simple.getDate() + 6 - simple.getDay());
    const temp = {
      d: ISOweekStart.getDate(),
      m: ISOweekStart.getMonth(),
      y: ISOweekStart.getFullYear(),
    }
    //console.log(ISOweekStart)
    const numDaysInMonth = new Date(temp.y, temp.m + 1, 0).getDate()
    
    return Array.from({length: 7}, _ => {
      if (temp.d > numDaysInMonth){
        temp.m +=1;
        temp.d = 1;
        // not needed, Date(2020, 12, 1) == Date(2021, 0, 1)
        /*if (temp.m >= 12){
          temp.m = 0
          temp.y +=1
        }*/
      }      
      return new Date(temp.y, temp.m, temp.d++).toUTCString()
    });
}

exports.weekEarning = (chefId) => {
    return new Promise((resolve, reject) => {
        let date = new Date();
        let weekNum = date.getWeek();
        earningDAO.fetchweek(weekNum).then((res) => {
            return Promise.all([res, earningDAO.fetchEarningChef(chefId)]);
        }).then(([res,data]) => {
            let startDate = res['fromDate'];
            let endDate = res['toDate'];
            return earningDAO.fetchChefEarningWeekHistory(startDate,endDate,data["earnedId"])
        }).then(data =>{
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
};

Date.prototype.getWeek = function() {
    var onejan = new Date(this.getFullYear(),0,1);
    let weekNum=Math.ceil((((this - onejan) / 86400000) + onejan.getDay()+2)/7);
    return weekNum-2;
}
//let date = new Date('20 Sep 2021');

