const roleDAO = require('../dao/role.dao');

exports.fetchRoleByCode = (code) =>{    
    return new Promise((resolve,reject) => {               
        roleDAO.findRoleByCode(code).then(data =>{
            console.log("user_type by code ================", data['role_id']);          
            resolve(data);            
        },err => {                    
        	reject({error:err});
        });
    });
};

exports.fetchRoleById = (roleId) => {    
    return new Promise((resolve,reject) => {               
        roleDAO.findRoleById(roleId).then(data =>{         
            resolve(data);            
        },err => {                    
        	reject({error:err});
        });
    });
};

exports.saveRole = (data) => {    
    return new Promise((resolve,reject) => {               
        roleDAO.createRole(data).then(data =>{         
            resolve(data);            
        },err => { 
            console.log(err);              
        	reject({error:err});
        });
    });
};

exports.fetchAllRoles = () => {    
    return new Promise((resolve,reject) => {               
        roleDAO.findAll().then(data =>{         
            resolve(data);            
        },err => {                    
        	reject({error:err});
        });
    });
};