const model = require('../models');
const AdvanceBookingMaster = model.advanceBookingMaster;

exports.advanceBookingMstSave = (advanceBookingData) => {    
    return new Promise((resolve, reject) => {        
        AdvanceBookingMaster.create(advanceBookingData).then(response => {                  
            console.log(response);
        	resolve(response);
        },err => {                    
        	reject({error:err});
        });
    });
};

exports.getAllAdvanceBooking = () =>{    
    return new Promise((resolve,reject) => {               
        AdvanceBookingMaster.findAll().then(data =>{
            console.log(data);            
            resolve(data);            
        },err => {                    
        	reject({error:err});
        });
    });
};