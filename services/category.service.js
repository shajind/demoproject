const model = require("../models");
const categoryModel = model.categorys;
const imageURL = require("../utils/image_URL-generator");

exports.saveCategory = (categoryData) => {
  return new Promise((resolve, reject) => {
    categoryModel.create(categoryData).then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};


function parseData(data) {
  let res = [];
  data.map((row) => {
    let resultObj = {};
    resultObj["category_id"] = row.category_id;
    resultObj["category_name"] = row.category_name;
    resultObj["category_desc"] = row.category_desc;
    resultObj["category_image"] = row.category_image;
    resultObj["parent_category_id"] = row.parent_category_id;    
    resultObj["status"] = row.status;;  
    if (row.category_image) {
      resultObj["category_image_URL"] = imageURL.setcategoryPath() + row.category_image;
    }
    res.push(resultObj);
  });
  return res;
};
exports.fetchAllCategories = (filters,parentId) => {
  let whereClause = {};
  if(filters) {
    let where = {};
    let conditions = JSON.parse(filters);
    for(let i = 0;i < conditions.length; i++) {
      let field = conditions[i]['field'];
      let predicate = conditions[i]['predicate'];
      let value = conditions[i]['value'];
      if(field == 'category_code') {
        where['parent_category_id'] = value == 'cusine' ? 2 : value;
      } else {
        where[field] = value;
      }
    }
    whereClause['where'] = where;
  }
  if(parentId) {
    let where = {};
    if(!parentId){
      where['parent_category_id']=0;
    }else{
      where['parent_category_id']=parentId;
    }    
    whereClause['where'] = where;
  }
  console.log(whereClause);
  return new Promise((resolve, reject) => {
    categoryModel.findAll(whereClause).then((res) => {
      let data=parseData(res);
        resolve(data);
      }).catch((err) => {
        reject(err);
      });
  });
};


exports.fetchAllCategoriesByParentId = (parentId) => {  
  return new Promise((resolve, reject) => {
    categoryModel.findAll({where:{parent_category_id:parentId}}).then((res) => {
      let data=parseData(res);
        resolve(data);
      }).catch((err) => {
        reject(err);
      });
  });
};
