const customerReviewDAO = require("../dao/customer_review.dao");
const imageURL = require("../utils/image_URL-generator");
const chefDAO = require('../dao/chef.dao');
const menuItemDAO=require("../dao/menu_item.dao");


exports.createCustomerReview = (data) => {
  return new Promise((resolve, reject) => {
    customerReviewDAO.saveCustomerReview(data)
    .then((res) => {
      return Promise.all([res,reviewcalculation(res['chef_id'],res['menu_item_id'])])
      }).then(([res,review])=>{
        resolve(res);
    })
    .catch((err) => {
        reject(err);
    });
  });
};



function reviewcalculation(chefId,menuItemId) {
  return new Promise((resolve, reject) => {
    Promise.all([
        customerReviewDAO.getCustomerReviewByChefId({chef_id:chefId}),
        customerReviewDAO.getCustomerReviewByChefId({menu_item_id:menuItemId})
      ]).then(([review, menuReview])=> {
        console.log([review, menuReview])
      let totalRating=0;
      for(let i=0;i<review.length;i++){
        totalRating = parseFloat(totalRating)+parseFloat(review[i]['overall_rating']);
      }
      let ratingCount=review.length;
      let result=parseFloat(totalRating)/ratingCount;
      let menuTotalRating=0;
      for(let i=0;i<menuReview.length;i++){
        let rating=0;
        rating=parseFloat(menuReview[i]['presentation_rating'])+parseFloat(menuReview[i]['taste_rating']);
        let ratingResult= rating/2;
        menuTotalRating = parseFloat(menuTotalRating)+ratingResult;
      }
      let menuRatingCount=menuReview.length;
      let menuResult=parseFloat(menuTotalRating)/menuRatingCount;
      let chefUpdatePayload={"overall_rating":result,"total_rating_count":ratingCount};
      let menuItemUpdatePayload={"total_rating_count":menuRatingCount,"menu_overall_rating":menuResult}
      return Promise.all([chefDAO.update(chefId,chefUpdatePayload),menuItemDAO.updateMenuItem(menuItemUpdatePayload,{'menu_item_id':menuItemId})]);
    }).then(([chefResponse,menuResponse])=>{
      resolve(chefResponse);
    }).catch((err) => {
      reject(err);
    });
  });
}


exports.findAllCustomerReview = (filters) => {
  let whereClause = {};
  if(filters){
    let conditions = JSON.parse(filters);
    for (let i = 0; i < conditions.length; i++) {
      let field = conditions[i]["field"];
      let predicate = conditions[i]["predicate"];
      let value = conditions[i]["value"];
      whereClause[field] = value;
    }
  }
  return new Promise((resolve, reject) => {
    customerReviewDAO.findAllCustomerReview(whereClause).then((res) => {
      console.log(res)

        let result = parseData(res);
        resolve(result);
    })
    .catch((err) => {
        reject(err);
    });
  });
};

exports.fetchCustomerReview = (orderId) => {
    return new Promise((resolve, reject) => {
      customerReviewDAO.getCustomerReview(orderId).then((res) => {
          let result = customerReviewData(res);
          resolve(result);
      })
      .catch((err) => {
          reject(err);
      });
    });
};


function parseData(data) {
    let res = [];
    data.map((row) => {
      let result=customerReviewData(row);
      res.push(result);
    });
    return res;
};

function customerReviewData (data){    
    if(data['image_or_video_path']){
        data.dataValues["image_or_video_path_URL"] = imageURL.setCustomerReviewFilePath()+data.image_or_video_path;
    }
    return data;
};
