const model = require('../models');
const PeopleServiceMaster = model.peopleServiceMaster;

exports.peopleServiceMstSave = (PeopleServiceMstData) => {    
    return new Promise((resolve, reject) => {        
        PeopleServiceMaster.create(PeopleServiceMstData).then(response => {                  
            console.log(response);
        	resolve(response);
        },err => {                    
        	reject({error:err});
        });
    });
};

exports.getAllPeopleService = () =>{    
    return new Promise((resolve,reject) => {               
        PeopleServiceMaster.findAll().then(data =>{
            console.log(data);            
            resolve(data);            
        },err => {                    
        	reject({error:err});
        });
    });
};