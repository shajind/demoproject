const model = require('../models');
const Order = model.Order;
const OrderItem = model.OrderItem;
const cartDAO = require("../dao/cart.dao");
const orderDAO = require("../dao/order.dao");
const imageURL = require("../utils/image_URL-generator");
const addonDAO = require("../dao/addon.dao");
const { response } = require('express');
const payuMoney = require('../payment/payumoney.service');
const earningDAO = require("../dao/earning.dao");


exports.doCheckout = (cartId,payload) => {    
    return new Promise((resolve, reject) => {
        cartDAO.findCustomerActiveCart(cartId).then(cart => {
            return Promise.all([cartDAO.updateCart(cart['cartId'], payload),saveCartAddon(cart['cartId'], payload['addon_ids'])]);
        }).then(([response,cartAddon])=>{
            let order = buildOrderModel(response);
            return Promise.all([response, orderDAO.createOrder(order)]);
        }).then(([cart, order,]) => {
            let items = buildOrderItemModel(order['orderId'], cart.dataValues['cart_items']);            
            return Promise.all([cart, order, orderDAO.createOrderItems(items),saveOrderCustomer(order['orderId'],cart['cart_customer']),saveOrderAddon(order['orderId'],cart['cartId'])]);
        }).then(([cart, order, items,customer]) => {
            return Promise.all([cartDAO.deactivateCart(cart['cartId']), orderDAO.findOrderById(order['orderId'])]);
        }).then(([cart, order]) => {
            return Promise.all([saveChefEarning(order),order]);
        }).then(([chefEarning,order]) => {
            let result=parseOrderItemData(order);
            resolve(result);
        }).catch(err => {
            reject(err);
        });
    });
}

function saveCartAddon(cartId,addon) {
    if(!addon){
        return 
    }
    return new Promise((resolve, reject) => {        
        addonDAO.findAllAddonByAddonId(addon)
        .then(addons => {
            let results=[];
            for (let i = 0; i < addons.length; i++) {
                let addon=addons[i]['dataValues'];
                addon['cartId']=cartId;
                results.push(addon);
            }
            return cartDAO.createCartAddons(results);
        }).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}



function saveChefEarning(order) {
    return new Promise((resolve, reject) => {
        let orderItems=order["items"];
        let chefId=orderItems[0].chefId;
        earningDAO.findEarningChef(chefId).then(data => {
            console.log(data);
            let results=[];
            let date = new Date();
            let weekNum = date.getWeek();
            for (let i = 0; i < orderItems.length; i++) {
                let row=orderItems[i]['dataValues'];
                let resultObj={};
                resultObj["earned_id"] = data["earnedId"];
                resultObj["order_item_id"] = row["itemId"];
                resultObj["itemEarnedAmount"] = row["price"];
                resultObj["weekNo"] = weekNum+1;
                results.push(resultObj);
            }
            console.log(results);
            return earningDAO.bulkCreateEarningChef(results);
        }).then( data => {
            resolve(data);
        }).catch((err) => {
            reject(err);
        });
    });  
}

Date.prototype.getWeek = function() {
    var onejan = new Date(this.getFullYear(),0,1);
    let weekNum=Math.ceil((((this - onejan) / 86400000) + onejan.getDay()+2)/7);
    return weekNum-1;
}

function generateInvoiceNo() {
    var todayDate = new Date();
    var milliseconds = todayDate.getMilliseconds();
    return "INV-" + milliseconds;
}

function buildOrderModel(cart) {
    let order = {};
    order['count'] = cart['count'];
    order["deliveryDate"] =cart['deliveryDate']; 
    order["delivery_time_slot"] =cart['delivery_time_slot'];
    order['invoiceNo'] = generateInvoiceNo();
    order['itemCount'] = cart['item_count'];
    order['subtotal'] = cart['subtotal'];
    order['chef_total_price'] = cart['chef_total_price'];
    order['baseTotal'] = cart['base_total'];
    order['grandTotal'] = cart['grand_total'];
    order['customerId'] = cart['customerId'];
    order['bookingNote'] = cart['bookingNote'];
    order["spiciness"] = cart["spiciness"];
    order['discount'] = cart['discount'];
    order['couponId'] = cart['couponId'];
    order['distance_km'] = cart['distance_km'];
    order['delivery_charge'] = cart['delivery_charge'];
    order['tax_amount'] = cart['tax_amount'];
    order['distance_charge'] = cart['distance_charge'];
    return order;
}

function buildOrderItemModel(orderId, cart_items) {
    let items = [];
    for(let i =0;i < cart_items.length;i++) {
        let item = {};
        let cart = cart_items[i];
        item['menuItemId'] = cart['menuItemId'];
        item['chefId'] = cart['chefId'];
        item['qty'] = cart['qty'];
        item['order_id'] = orderId;
        item['price'] = cart['price'];
        item['rowTotal'] = cart['rowTotal'];
        item['itemName'] = cart['itemName'];
        item['itemDescription'] = cart['itemDescription'];
        item['chefFullName'] = cart['chefFullName'];
        item["minDish"] = cart["minDish"];
        item["maxDish"] = cart["maxDish"];        
        item["kitchenTypeId"] = cart["kitchenTypeId"];
        item["kitchenType"] = cart["kitchenType"];
        item["chefkitchenImage"] = cart["chefkitchenImage"];
        item["kitchenTypeCode"] = cart["kitchenTypeCode"];
        item["menuItemImg"] = cart["menuItemImg"];
        item["foodTypeId"] = cart["foodTypeId"];
        item["foodType"] = cart["foodType"];
        item["foodTypeCode"] = cart["foodTypeCode"];
        item["menuTypeId"] = cart["menuTypeId"];
        item["menuType"] = cart["menuType"];
        item["menuTypeCode"] = cart["menuTypeCode"];
        item["spiciness"] = cart["spiciness"];
        item["root_category_name"] = cart["root_category_name"];        
        items.push(item);
    }
    return items;
}

function saveOrderAddon(orderId, cartId) {
    return new Promise((resolve, reject) => {
        cartDAO.findCartById(cartId)
        .then(cart => {
            let cartAddon=cart['cart_addon']
            if(cartAddon.length==0){
                return cart
            }
            let results=[];
            for (let i = 0; i < cartAddon.length; i++) {
                let orderAddon=cartAddon[i]['dataValues'];
                orderAddon['orderId']=orderId;
                results.push(orderAddon);
            }
            return orderDAO.createOrderAddons(results);
        }).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
}

function saveOrderCustomer(orderId,customer) {
    return new Promise((resolve, reject) => {
        l_customer=customer['dataValues'];
        l_customer['order_id']=orderId;
        orderDAO.saveOrderCustomer(orderId,l_customer).then(data => {
            resolve(data);
        }).catch(err => {
            reject(err);
        })
    })
}

function parseOrderItemData(data){
    if(data.items){
      let order_item = data.items;
      for(i=0;order_item.length>i;i++){
        let row=order_item[i];
        row.dataValues['menuItemImg_URL']=imageURL.menuItemImage_URL()+row['menuItemImg'];
        row.dataValues['chef_kitchen_image_URL']=imageURL.setChefDocumentPath()+row['chefkitchenImage'];

      }
    }
    if(data.order_customer){
      let row=data.order_customer;
      row.dataValues['cust_profile_pic_URL']=imageURL.customerImage_URL()+row['cust_profile_pic'];
      row.dataValues['cust_cover_pic_URL']=imageURL.customerImage_URL()+row['cust_cover_pic'];
    }
    if(data.order_addon){
        let orderAddon=data.order_addon;
        for(i=0;orderAddon.length>i;i++){
            let row=orderAddon[i];
            row.dataValues['addon_image_URL']=imageURL.menuItemImage_URL()+row['addon_image'];          
        }
    }
    return data;
}
  
function parseData(data) {
    let res = [];
    data.map((row) => {
      let result=parseOrderItemData(row);
      res.push(result);
    });
    return res;
};

exports.fetchAllOrders = (filters,fetchId) => {
    let whereClause = {};
    let whereClause_I = {};
    if(filters){
        let conditions = JSON.parse(filters);
        for (let i = 0; i < conditions.length; i++) {
            let field = conditions[i]["field"];
            let predicate = conditions[i]["predicate"];
            let value = conditions[i]["value"];
            if(field == 'orderStatus' && value == 'new_order') {
                whereClause["orderStatus"] = ["accepted","ordered"];
            }else if(field == 'menu_type') {
                whereClause_I[field] = value;
            } else {
                whereClause[field] = value;
            }
        }
    }
    return new Promise((resolve, reject) => {        
        orderDAO.findAllOrders(whereClause,whereClause_I,fetchId).then(response => {
            let result=parseData(response);
            resolve(result);
        }).catch(err => {
            reject(err);
        });
    });
}

exports.fetchOrderById = (order_id) => {
    return new Promise((resolve, reject) => {        
        orderDAO.findOrderById(order_id).then(response => {
            resolve(response);
        }).catch(err => {
            reject(err);
        });
    });
}

exports.acceptOrderByChef = (order_id, chef_id) => {
    return new Promise((resolve, reject) => {
        orderDAO.findOrderById(order_id).then(data => {
            let orderItems=data["items"];
            let orderChefId=orderItems[0].chefId;
            if(orderChefId == chef_id){
                return orderDAO.acceptOrder(order_id, chef_id);
            }else{
                throw new Error("This chefId do not match");
            }
        }).then(response => {
            resolve(response);
        }).catch(err => {
            reject(err);
        });
    });
}

exports.rejectOrderByChef = (order_id, chef_id, payload) => {
    return new Promise((resolve, reject) => {
        orderDAO.findOrderById(order_id).then(data => {
            let orderItems=data["items"];
            let orderChefId=orderItems[0].chefId;
            if(orderChefId == chef_id){
                let reason = payload['rejected_reason'];     
                return orderDAO.rejectOrder(order_id, chef_id, reason)
            }else{
                throw new Error("This chefId do not match");
            }
        }).then(response => {
            resolve(response);
        }).catch(err => {
            reject(err);
        });
    });
}

exports.completedOrderByChef = (order_id, chef_id) => {
    return new Promise((resolve, reject) => {
        orderDAO.findOrderById(order_id).then(data => {
            let orderItems=data["items"];
            let orderChefId=orderItems[0].chefId;
            if(orderChefId == chef_id){
                return orderDAO.completedOrder(order_id, chef_id);
            }else{
                throw new Error("This chefId do not match");
            }
        }).then(response => {
            resolve(response);
        }).catch(err => {
            reject(err);
        });
    });
}

exports.outdeliveryOrderByChef = (order_id, chef_id) => {
    return new Promise((resolve, reject) => {
        orderDAO.findOrderById(order_id).then(data => {
            let orderItems=data["items"];
            let orderChefId=orderItems[0].chefId;
            if(orderChefId == chef_id){
                return orderDAO.outdeliveryOrder(order_id, chef_id);
            }else{
                throw new Error("This chefId do not match");
            }
        }).then(response => {
            resolve(response);
        }).catch(err => {
            reject(err);
        });
    });
}

function orderChefParse(data){
    let res=[];
    data.map((row) => {
        let orderChefStatus={};
        let result=row.order;
        orderChefStatus["order_chef_id"]=row.order_chef_id;
        orderChefStatus["rejected_reason"]=row.rejected_reason;
        orderChefStatus["status"]=row.status;
        orderChefStatus["order_id"]=row.order_id;
        orderChefStatus["chef_id"]=row.chef_id;
        result.dataValues['order_Chef_Status']=orderChefStatus;
        res.push(result);
    });
    return res;
}

exports.fetchAllOrderChefStatus = (chefId,filters) => {
    let whereClause = {};
    if(filters){
        let conditions = JSON.parse(filters);
        for (let i = 0; i < conditions.length; i++) {
            let field = conditions[i]["field"];
            let predicate = conditions[i]["predicate"];
            let value = conditions[i]["value"];
            whereClause[field] = value;
        }
    }
    return new Promise((resolve, reject) => {     
        orderDAO.getAllOrderChefStatus(chefId,whereClause).then(response => {
            let res=orderChefParse(response);
            let result=parseData(res);
            resolve(result);
        }).catch(err => {
            reject(err);
        });
    });
}


exports.fetchAllOrderChef = (chefId) => {
    return new Promise((resolve, reject) => {     
        orderDAO.getAllOrderChef(chefId).then(response => {
            resolve(response);
        }).catch(err => {
            reject(err);
        });
    });
}