const courseDAO = require("../dao/course.dao");
const core = require("../helpers/core.helper");



exports.createCourse = (data) => {
  return new Promise((resolve, reject) => {
    let courseCode = core.createCode(data['course_name']);
    data['course_code'] = courseCode;
    courseDAO.saveCourse(data)
    .then((res) => {
        resolve(res);
    })
    .catch((err) => {
        reject(err);
    });
  });
};

exports.findAllCourse = () => {
  return new Promise((resolve, reject) => {
    courseDAO.findAllCourse().then((res) => {
        resolve(res);
    })
    .catch((err) => {
        reject(err);
    });
  });
};
  