const model = require("../models");
const customerModel = model.customers;
const userModel = model.users;
const stateModel = model.state;
const cityModel = model.city;
const customerService = require("../services/customer-address.service");
const customerDAO = require("../dao/customer.dao");
const userService = require("../services/users.service");
const tokenGenerator = require('../utils/token-generator');
const userDAO = require("../dao/user.dao");
const userHelper = require("../helpers/user.helper");
const roleConstant = require("../config/role.json");
const imageURL = require("../utils/image_URL-generator");

exports.saveCustomerProfile = (customerdata) => {
  return new Promise((resolve, reject) => {
    customerModel.create(customerdata).then(
      (response) => {
        console.log(response);
        resolve(response);
      },
      (err) => {
        reject({ error: err });
      }
    );
  });
};

exports.updateCustomerProfile = (customerdata, userId) => {
  return new Promise((resolve, reject) => {
    customerModel.update(customerdata, { where: { user_id: userId } }).then(
      (response) => {
        if (response == 1) {
          let userName = { user_name: customerdata.cust_fullname };
          userModel.update(userName, { where: { user_id: userId } });
        }
        resolve(response);
      },
      (err) => {
        reject({ error: err });
      }
    );
  });
};

exports.updateCustomerProfileImage = (customerdata, userId) => {
  return new Promise((resolve, reject) => {
    customerModel.update(customerdata, { where: { user_id: userId } }).then(
      (response) => {
        resolve(response);
      },
      (err) => {
        reject({ error: err });
      }
    );
  });
};

let customerAllData = (data) => {
  let res = [];
  data.map((row) => {
    let customer = formatCustomerResponse(row);
    res.push(customer);
  });
  return res;
};

exports.getAllCustomers = () => {
  return new Promise((resolve, reject) => {
    customerModel.findAll({include: [{model: stateModel,as: "stateData"},
    {model: cityModel,as: "cityData"}]}).then(
      (data) => {
        console.log(data);
        let response = customerAllData(data);
        resolve(response);
      },
      (err) => {
        reject({ error: err });
      }
    );
  });
};

function formatCustomerResponse(data) {
  let resultObj = {};
  resultObj["cust_id"] = data.cust_id;
  resultObj["cust_fullname"] = data.cust_fullname;
  resultObj["cust_gender"] = data.cust_gender;
  resultObj["cust_dob"] = data.cust_dob;
  resultObj["cust_fpreference"] = data.cust_fpreference;
  resultObj["cust_profile_pic"] = data.cust_profile_pic;
  resultObj["cust_cover_pic"] = data.cust_cover_pic;
  resultObj["cust_address1"] = data.cust_address1;
  resultObj["cust_address2"] = data.cust_address2;
  resultObj["city_id"] = data.city_id;
  if(data.cityData) {
    resultObj["city"] = data.cityData["city"];
  }
  resultObj["state_id"] = data.state_id;
  if(data.stateData) {
    resultObj["state"] = data.stateData["state"];
  }
  resultObj["cust_zipcode"] = data.cust_zipcode;
  resultObj["status"] = data.status;
  resultObj["createdAt"] = data.createdAt;
  resultObj["updatedAt"] = data.updatedAt;
  resultObj["user_id"] = data.user_id;
  resultObj["country_code"] = data.country_code;
  if (data.cust_profile_pic) {
    resultObj["cust_profile_pic_URL"] = imageURL.customerImage_URL()+ data.cust_profile_pic;
  }
  if (data.cust_cover_pic) {
    resultObj["cust_cover_pic_URL"] = imageURL.customerImage_URL()+ data.cust_cover_pic;
  }

  return resultObj;
};

exports.fetchCustomerByUserId = (userId) => {
  return new Promise((resolve, reject) => {
    customerDAO.findCustomerByUserId(userId).then(data => {
      if(data) {
        let response = formatCustomerResponse((data));
        resolve(response);
        return;
      }
      resolve(data);
    }).catch(err => {
      reject(err);
    })
  });
};

exports.customerUpdate = (payload, customerId) => {
  return new Promise((resolve, reject) => {
    customerModel
      .update(payload, { where: { cust_id: customerId } })
      .then((res) => {
        console.log(res);
        resolve(res);
      })
      .catch((err) => {
        console.log(err);
        reject(err);
      });
  });
};

exports.getCustomersByCustomersId = (customerId) => {
  return new Promise((resolve, reject) => {
    customerModel.findOne({ where: { cust_id: customerId } ,
      include: [{model: stateModel,as: "stateData"},
      {model: cityModel,as: "cityData"}]}).then(
      (data) => {
        let response = formatCustomerResponse(data);
        resolve(response);
      },
      (err) => {
        reject({ error: err });
      }
    );
  });
};

exports.getCustomersProfileByUserId = (userId) => {
  return new Promise((resolve, reject) => {
    customerModel.findOne({ where: { user_id: userId } }).then(
      (data) => {
        let response = formatCustomerResponse(data);
        resolve(response);
      },
      (err) => {
        reject({ error: err });
      }
    );
  });
};


exports.validateLogin = (reqBody) => {
  return new Promise((resolve, reject) => {
    let userName = reqBody.user_name;
    let passWord = reqBody.password;   
    userService.login(userName, passWord).then(user => {
      return Promise.all([user, customerDAO.findCustomerByUserId(user['user_id'])]);
    }).then(([user, customer]) => {
      if(!customer){        
        //console.log('customer login data =======', customer);
        return Promise.all([user,customerDAO.saveCustomer({"user_id":user['user_id']})]);
      }else{
        return ([user, customer])
      }
    }).then(([user, customer]) => {
      //console.log('customer login data =======', customer);
      let custData = null;
      if(customer) {
        custData = formatCustomerResponse(customer);
      }
      let role = userHelper.formatRoles(user, roleConstant.ROLE_CUSTOMER);
      user['access_token'] = tokenGenerator.genToken(user.user_id, role['role_code']);
      user.setDataValue('customer', custData);
      resolve(user);
    }).catch(err => {
      reject(err);
    });
  });
};

exports.updateCustomerProfileData = (userId, reqBody) => {
  return new Promise((resolve, reject) => {
    let userData = reqBody['userData'];
    let customerData = reqBody['customerdata'];
    let customerAdrressData = reqBody['customerAddress'];
    Promise.all([userService.fetchUserById(userId), customerDAO.findCustomerByUserId(userId)])
    .then(([userDbData, customerDbData]) => {      
      let customerPromise;
      if(customerDbData) {
        customerDbData["cust_fullname"] = customerData.cust_fullname;
        customerDbData["cust_gender"] = customerData.cust_gender;
        customerDbData["cust_dob"] = customerData.cust_dob;
        customerDbData["cust_fpreference"] = customerData.cust_fpreference;
        customerDbData["cust_address1"] = customerData.cust_address1;
        customerDbData["cust_address2"] = customerData.cust_address2;
        customerDbData["city_id"] = customerData.city_id;
        customerDbData["state_id"] = customerData.state_id;
        customerDbData["cust_zipcode"] = customerData.cust_zipcode;
        customerDbData["country_code"] = customerData.country_code;
        customerDbData["cust_profile_pic"] = customerData.cust_profile_pic;
        customerDbData["cust_cover_pic"] = customerData.cust_cover_pic;
        customerAdrressData['user_id'] = userId;
        customerAddress = customerService.saveCustomerAddressHome(userId,customerAdrressData);
        customerPromise = customerDAO.updateCustomer(customerDbData['cust_id'], customerDbData.dataValues);
      } else {
        customerData['user_id'] = userId;
        customerPromise = customerDAO.saveCustomer(customerData);
      }
      userDbData['user_email'] = userData['user_email'];
      userDbData['user_name'] = userData['user_name'];
      userDbData['country_code'] = userData['country_code'];
      return Promise.all([userDAO.updateUser(userDbData['user_id'], userDbData.dataValues), customerPromise,customerAddress]);
    }).then(([userUpdatedDbData, customerUpdatedDbData]) => {
        userUpdatedDbData.setDataValue('customer', formatCustomerResponse(customerUpdatedDbData));
        console.log('updateCustomerProfileData', userUpdatedDbData);
        resolve(userUpdatedDbData);
    }).catch(err => {
      console.log(err);
      reject(err);
    });
  });
};

exports.fetchCustomerById = (customerId) => {
  return new Promise((resolve, reject) => {
    customerDAO.fetchCustomer(customerId).then(customer => {
       resolve(customer);
    }).catch(err => {
      reject(err);
    })
  });
};