const taxDAO = require("../dao/tax.dao");
const core = require("../helpers/core.helper");



exports.createTax = (data) => {
  return new Promise((resolve, reject) => {
    let taxCode = core.createCode(data['tax_label']);
    data['tax_code'] = taxCode;
    taxDAO.saveTax(data).then((res) => {
        resolve(res);
    }).catch((err) => {
        reject(err);
    });
  });
};

exports.findAllTax = () => {
  return new Promise((resolve, reject) => {
    taxDAO.findAllTax().then((res) => {
        resolve(res);
    }).catch((err) => {
        reject(err);
    });
  });
};
  