const model = require("../models");
const userModel = model.users;
const customerModel = model.customers;
const userTypeModel=model.user;
const Role = model.role;
const deviceTokenModel = model.user_device_tokens;
const otpGenerator = require("../utils/otp-generater");
const userDAO = require("../dao/user.dao");
const roleService = require('../services/role.service');
const tokenGenerator = require('../utils/token-generator');
const userHelper = require("../helpers/user.helper");
const customerDAO = require("../dao/customer.dao");

exports.saveUser = (userdata) => {
  return new Promise((resolve, reject) => {
    userModel
      .update(userdata, {
        where: {
          user_mobile: userdata.user_mobile,
          user_type_id: userdata.user_type_id,
        },
      })
      .then(
        (response) => {
          console.log(response);
          if (response == 0) {
            userModel.create(userdata).then((response) => {
              let payload = {};
              payload["user_id"] = response.user_id;
              customerModel.create(payload);
              console.log(response);
              resolve(response);
            });
          } else resolve(response);
        },
        (err) => {
          reject({ error: err });
        }
      );
  });
};

exports.login = (userName, passWord) => {
  return new Promise((resolve, reject) => {
    userDAO.validateLogin(userName, passWord).then(result => {
        if(!result) {
          throw new Error("User not found");
        }
        resolve(result);
      })
      .catch((err) => {
        reject(err);
      });
  });
};


exports.adminlogin = (userName, passWord,roleId) => {
  return new Promise((resolve, reject) => {
    userDAO.validateAdminLogin(userName, passWord).then(result => {
      if(!result) {
        throw new Error("admin not found");
      }
      return Promise.all([result,userDAO.adminLoginRole(result['user_id'],roleId)])
    }).then(([result,role])=>{
      resolve(result);
    }).catch((err) => {
      reject(err);
    });
  });
};

exports.validateUser = (userid) => {
  return new Promise(function (resolve, reject) {
    userDAO.findUserById(userid).then((userdata) => {
        resolve(userdata);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

exports.device_token = (userdata) => {
  return new Promise((resolve, reject) => {
    deviceTokenModel.create(userdata).then(
      (response) => {
        resolve(response);
      },
      (err) => {
        reject({ error: err });
      }
    );
  });
};

function getRole(roles, role_code) {
  let role = undefined;
  for(let idx = 0;idx < roles.length;idx++) {
    if(roles[idx].role_code == role_code) {
      role = roles[idx];
      break;
    }
  }
  return role;
}

exports.generateUserOTP = (data, current_role_code) => {
  return new Promise((resolve, reject) => {
      userDAO.findUserByMobileOrEmail(data['mobile'], data['email']).then(response => {
        let otp = '00000';//otpGenerator.OTPpassword();
        if(response) {
          response.dataValues["user_otp"] = otp;
          let isRolePresent = false;
          for(let idx = 0; idx < response.roles.length; idx++) {
            if(response.roles[idx]['role_code'] == current_role_code) {
              isRolePresent = true;
              break;
            }
          }
          if(isRolePresent) {
            userDAO.updateUser(response["user_id"], response.dataValues).then(data => {
              let role = userHelper.formatRoles(data, current_role_code);
              data['access_token'] = tokenGenerator.genToken(data['user_id'], role['role_code']);
              resolve(data);
            }).catch(err => {
              reject(err);
            })
          } else {
            roleService.fetchRoleByCode(current_role_code).then(role => {
              userDAO.createRoleForExistingUser(response.user_id, role).then(data => {
                userHelper.addCurrentRoleField(data, role);
                data['access_token'] = tokenGenerator.genToken(data['user_id'], role['role_code']);
                resolve(data);
              }).catch(err => {
                reject(err);
              })
            }).catch(err => {
              reject(err);
            });
          }
        } else {
            roleService.fetchRoleByCode(current_role_code).then(role => {
              data['user_otp'] = otp;
              userDAO.saveUser(data, role).then(user => {
                return Promise.all([user,customerDAO.saveCustomer({"user_id":user['user_id']})]);
              }).then(([user,customer]) => {
                  userHelper.addCurrentRoleField(user, role);
                  user['access_token'] = tokenGenerator.genToken(user['user_id'], role['role_code']);
                  resolve(user);
              }).catch(err => {
                reject(err);
              });
            }).catch(err => {
              reject(err);
            });
          }
      }).catch(err => {
        reject(err);
      }
    );
  });
};

exports.fetchUserById = (userId) => {
  return new Promise((resolve, reject) => {
    userDAO.findUserById(userId).then(
      (response) => {
        resolve(response);
      },
      (err) => {
        reject({ error: err });
      }
    );
  });
};

exports.fetchUser = (mobile, email) => {
  return new Promise((resolve, reject) => {
    userDAO.findUserByMobileOrEmail(mobile, email).then(response => {
        resolve(response);
      },(err) => {
        reject({ error: err });
      }
    );
  });
};
