const varietyDAO = require("../dao/variety.dao");



exports.createVariety = (data) => {
    return new Promise((resolve, reject) => {
        varietyDAO.saveVariety(data).then((res) => {
            resolve(res);
        }).catch((err) => {
            reject(err);
        });
    });
};

exports.findAllVariety = () => {
    return new Promise((resolve, reject) => {
        varietyDAO.findAllVariety().then((res) => {
            resolve(res);
        }).catch((err) => {
            reject(err);
        });
    });
};

exports.fetchVarietyByChef = (chefId) => {
    return new Promise((resolve, reject) => {
        varietyDAO.findVarietyByChef(chefId).then((res) => {
            resolve(res);
        }).catch((err) => {
            reject(err);
        });
    });
};
 