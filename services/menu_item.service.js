const model = require("../models");
const moment = require('moment');
const menuItem = model.menu_items;
const MenuType = model.menu_types;
const MenuFoodTypeModel = model.MenuFoodType;
const chefModel = model.chefs;
const categoryModel = model.categorys;
const Inclusion = model.MenuItemInclusion;
const kitchenType=model.kitchen_type;
const chefDAO = require('../dao/chef.dao');
const menuItemDAO=require("../dao/menu_item.dao");
const kitchenTypeDAO = require("../dao/kitchen_type.dao");
const imageURL = require("../utils/image_URL-generator");
//const categoryMenuItemModel=model.category_menu_item;
const menuTypeService = require("../services/menu_type.service");
const {Op} = require('sequelize');
const { response } = require("express");
const AddonModel = model.addon;
const MenuItemVarietyModel = model.MenuItemVariety;
const MenuItemAvailable=model.menu_item_available;
const MenuItemDeliveryTimeSlotMaster = model.deliveryTimeSlotMaster;
const Course = model.course;
const MenuItemAddon = model.menu_items_addon;
const MenuItemCutoffTime = model.MenuItemCutoffTime;
const distanceCalculation = require("../utils/distance_duration");
const customerReviewDAO = require("../dao/customer_review.dao");
const CustomerCurrentLocationDAO = require("../dao/customer_current_location.dao");


function createInclusions(menu_item_id, inclusions_p) {
  let results = [];
  if(!inclusions_p) {
    return results;
  }
  //let inclusions=inclusions_p;
  let inclusions = JSON.parse(inclusions_p);
  for(let i = 0;i < inclusions.length;i++) {
    let row = inclusions[i];
    let inclusion = {};
    inclusion["item_name"] = row.item_name;
    inclusion["item_portion"] = row.item_portion;
    inclusion["menu_item_id"] = menu_item_id;
    results.push(inclusion);
  }
  return results;
}

// function createCategories(menu_item_id, category) {
//   let results=[];
//   if(!category) {
//     return results;
//   }
//   let category_ids=category.split(",");
//   for (let i = 0; i < category_ids.length; i++) {
//     category={};
//     category["category_id"]=category_ids[i];
//     category["menu_item_id"]=menu_item_id;
//     results.push(category);
//   }
//   return results;
// }

function createAvailabilities(menu_item_id, p_availabilities) {
  let results = [];
  if(!p_availabilities) {
    return results;
  }
  //let availabilities=p_availabilities;
  let availabilities = JSON.parse(p_availabilities);
  for(let i = 0;i < availabilities.length;i++) {
    let row = availabilities[i];
    let availability = {};
    if(row.available_day) {
      availability["available_day"] = row.available_day;
    }
    availability["available_date"] = row.available_date;
    availability["menu_item_id"] = menu_item_id;
    results.push(availability);
  }
  return results;
}


function createCutoffTimes(menu_item_id, cutoffTimes_p) {
  let results = [];
  if(!cutoffTimes_p) {
    return results;
  }
  //let cutoffTimes=cutoffTimes_p;
  let cutoffTimes = JSON.parse(cutoffTimes_p);
  for(let i = 0;i < cutoffTimes.length;i++) {
    let row = cutoffTimes[i];
    let cutoffTime = {};
    let timeFormat= moment(row.cutoff_time, ["h:mm A"]).format("HH:mm");
    cutoffTime["cutoff_day"] = row.cutoff_day;
    cutoffTime["cutoff_time"] = timeFormat;
    cutoffTime["cutoff_type"] = row.cutoff_type;
    cutoffTime["course_id"] = row.course_id;
    cutoffTime["menu_item_id"] = menu_item_id;
    results.push(cutoffTime);
  }
  return results;
}

function createVarieties(menu_item_id, varieties_p) {
  let results = [];
  if(!varieties_p) {
    return results;
  }
  //let varieties=varieties_p;  
  let varieties = JSON.parse(varieties_p);
  for(let i = 0;i < varieties.length;i++) {
    let row = varieties[i];
    console.log(row)
    let variety = {};
    variety["variety_name"] = row.variety_name;
    // variety["unit"] = row.unit;
    // variety["unit_qty"] = row.weight;
    // variety["unit_qty_label"] = row.weight+" "+row.unit;
    variety["unit_price"] = row.price;
    variety["menu_item_id"] = menu_item_id;
    results.push(variety);
  }
  console.log(results);
  return results;
}

function createDeliveryTimeSlots(menu_item_id, slots) {
  let results=[];
  if(!slots) {
    return results;
  }
  let timeSlots = slots.split(",");
  for(let i = 0; i < timeSlots.length; i++) {
    let timeSlot = {};
    timeSlot["delivery_time_slot_id"] = timeSlots[i];
    timeSlot["menu_item_id"] = menu_item_id;
    results.push(timeSlot);
  }
  return results;
}

function createAddon(menu_item_id, addon) {
  let results=[];
  if(!addon) {
    return results;
  }
  let addon_ids=addon.split(",");
  for (let i = 0; i < addon_ids.length; i++) {
    addon={};
    addon["addon_id"]=addon_ids[i];
    addon["menu_item_id"]=menu_item_id;
    results.push(addon);
  }
  return results;
}

// function menuItemBulkCreate(menuItemData) {
//   let menuItemArray = [];
//   if(menuItemData['has_variety'] == 'true'){
//     if(!menuItemData['varieties']){
//       throw new Error("Menu item varieties not defined");
//     }
//     let menuItemVarieties=menuItemData['varieties'];
//     let varieties = JSON.parse(menuItemVarieties);
//     if(varieties.length==0){
//       throw new Error("Menu item variety not defined");
//     }        
//     for(let i = 0; i < varieties.length; i++) {
//       menuItemData["variety"] = varieties[i].units;
//       menuItemData["variety_id"] = varieties[i].variety_id;
//       menuItemArray.push(menuItemData);
//     } 
//   }else{
//     menuItemArray.push(menuItemData);
//   }
//   return menuItemArray;
// }

function findMenuType(chefId) {
  return new Promise((resolve, reject) => {
    chefDAO.findChefById(chefId).then(data => {
      let menuTypeCode
      if(data['chef_type']=="home_chef"){
        menuTypeCode='chef_menu'
      }else if(data['chef_type']=="yumma_chef"){
        menuTypeCode='yumma_menu'
      }      
      return menuTypeCode
    })
    .then(menuTypeCode => {
      return menuTypeService.getMenuTypeByCode(menuTypeCode)
    })
    .then(data => {
      resolve(data);
    }).catch((err) => {
      reject(err);
    });
  });  
}

exports.createMenuItem = (menuItemData,chefId) => {
  return new Promise((resolve, reject) => {    
    findMenuType(chefId).then(menuType => {
      menuItemData['menu_type_id'] = menuType['menu_type_id'];
      return Promise.all([menuItemData, menuItemDAO.saveMenuItem(menuItemData)]);
    }).then(([menuItemData, response]) => {
      let menuItemId=response['menu_item_id']
      let inclusions = createInclusions(menuItemId, menuItemData['inclusions']);
      let varieties = createVarieties(menuItemId, menuItemData['varieties']);
      let cutoffTimes = createCutoffTimes(menuItemId, menuItemData['cutoff_times']);
      let addon = createAddon(menuItemId, menuItemData['addon_ids']);
      let availabilities = createAvailabilities(menuItemId, menuItemData['availability']);
      let deliveryTimeSlots = createDeliveryTimeSlots(menuItemId, menuItemData['delivery_time_slot_ids']);     
      return Promise.all([response,
        menuItemDAO.linkMenuItemInclusion(inclusions),
        menuItemDAO.linkMenuItemAvailability(availabilities),
        menuItemDAO.linkMenuItemVarieties(varieties),
        menuItemDAO.linkMenuItemCutoffTimes(cutoffTimes),
        menuItemDAO.linkMenuItemDeliveryTimeSlot(deliveryTimeSlots),
        menuItemDAO.linkMenuItemAddon(addon)
      ]);      
    }).then(([menu_item]) => {
      resolve(menu_item);
    }).catch((err) => {
      reject(err);
    });
  });
};

exports.updateMenu_Item = (menuItemData,menuItemId) => {
  let whereClause={menu_item_id:menuItemId}
  return new Promise((resolve, reject) => {
    menuItemDAO.updateMenuItem(menuItemData,whereClause).then(data => {
      return Promise.all([data, 
        menuItemDAO.deleteMenuItemInclusion(menuItemId),
        menuItemDAO.deleteMenuItemAvailability(menuItemId),
        menuItemDAO.deleteMenuItemVarieties(menuItemId),
        menuItemDAO.deleteMenuItemCutoffTimes(menuItemId),
        menuItemDAO.deleteMenuItemDeliveryTimeSlot(menuItemId),
        menuItemDAO.deleteMenuItemAddon(menuItemId)
      ]);
    }).then(([data, deleteInclusions, deleteAvailability,deleteVarieties,deleteCutoffTimes, deleteDeliveryTimeSlots,deleteAddon]) => {
      let inclusions = createInclusions(menuItemId, menuItemData['inclusions']);
      let varieties = createVarieties(menuItemId, menuItemData['varieties']);
      let cutoffTimes = createCutoffTimes(menuItemId, menuItemData['cutoff_times']);
      let addon = createAddon(menuItemId, menuItemData['addon_ids']);
      let availabilities = createAvailabilities(menuItemId, menuItemData['availability']);
      let deliveryTimeSlots = createDeliveryTimeSlots(menuItemId, menuItemData['delivery_time_slot_ids']);     
      return Promise.all([data, 
        menuItemDAO.linkMenuItemInclusion(inclusions),
        menuItemDAO.linkMenuItemAvailability(availabilities),
        menuItemDAO.linkMenuItemVarieties(varieties),
        menuItemDAO.linkMenuItemCutoffTimes(cutoffTimes),
        menuItemDAO.linkMenuItemDeliveryTimeSlot(deliveryTimeSlots),
        menuItemDAO.linkMenuItemAddon(addon)
      ]);
    }).then(([data, inclusions, availability,varieties,cutoffTimes, deliveryTimeSlots,addon]) => {
      resolve(data);
    }).catch((err) => {
      reject(err);
    });
  });
};


exports.updateMenuItemVisibility = (menuItemData,menuItemId,chefId) => {
  return new Promise((resolve, reject) => {
    menuItemDAO.findMenuItemByChefId(menuItemId,chefId).then(data => {
      let whereClause={menu_item_id:data['menu_item_id']}
      return menuItemDAO.updateMenuItem(menuItemData,whereClause)
    }).then(response => {      
      resolve(response);
    }).catch((err) => {
      reject(err);
    });
  });
};


exports.updateMenuItemYummaprice = (menuItemData,menuItemId) => {
  let whereClause={menu_item_id:menuItemId};
  return new Promise((resolve, reject) => {
    menuItemDAO.updateMenuItem(menuItemData,whereClause).then(data => {
      resolve(data);
    }).catch((err) => {
      reject(err);
    });
  });
};

exports.updateMenuItemChefVisibility = (menuItemData,chefId) => {
  let whereClause={chef_id:chefId};
  return new Promise((resolve, reject) => {
    menuItemDAO.updateMenuItem(menuItemData,whereClause).then(data => {
      resolve(data);
    }).catch((err) => {
      reject(err);
    });
  });
};

exports.fetchMenuItems = (filters,sortBy,customerId) => {
  let currentDay = new Date();
  let currentTime=currentDay.getHours()+":"+currentDay.getMinutes();
  let cutOfftime={[Op.or]:[{'cutoff_time':{[Op.gte]:currentTime},"cutoff_day":"Today"},{"cutoff_day": "Previous_day"}]};
  let whereClause = {is_visible:true,is_deleted: false};
  let availability={'available_date':{[Op.gte]:currentDay}};
  let deliverySlot;
  let finalPriceSort;
  let distance=100;
  let array=[['menu_item_id','DESC']];
  if (filters) {    
    let conditions = JSON.parse(filters);
    for (let i = 0; i < conditions.length; i++) {
      let field = conditions[i]["field"];
      let predicate = conditions[i]["predicate"];
      let value = conditions[i]["value"];
      if(field == 'rating') {
        whereClause["menu_overall_rating"] = {[Op.gte]:value};
      }else if(field == 'available_date'&&value=='today') {
        availability["available_date"] = currentDay;
      }else if(field == 'available_date'&&value == 'tomorrow') {
        let tomorrow=new Date(currentDay.setDate(currentDay.getDate() + 1));
        availability["available_date"] = tomorrow;
      }else if(field == 'delivery_date') {
        availability={};
        availability["available_date"] = value;
      }else if(field == 'delivery_slot') {
        deliverySlot={};
        deliverySlot["slot_label"] = value;
      }else if(field == 'distance') {
        distance = value;
      }else if(field == 'search' && predicate == 'like') {
        whereClause[Op.or] = [
          {menu_item_name:{[Op.like]:"%"+value+"%"}},
          {'$root_category.category_name$' :{[Op.like]:"%"+value+"%"}},
          {'$sub_category.category_name$' :{[Op.like]:"%"+value+"%"}}
        ]
      }else {
        whereClause[field] = value;
      }
    }
  }
  if(sortBy){
    let conditions = JSON.parse(sortBy);
    for (let i = 0; i < conditions.length; i++) {
      let field = conditions[i]["field"];
      let predicate = conditions[i]["predicate"];
      let value = conditions[i]["value"];
      if(field == 'price' && value == 'high_to_low') {
        finalPriceSort='DESC';
      }else if(field == 'price' && value == 'low_to_high') {
        finalPriceSort='ASC';
      }else if(field == 'rating' && value == 'high_to_low') {
        array=[['menu_overall_rating','DESC']];
      }else if(field == 'rating' && value == 'low_to_high') {
        array=[['menu_overall_rating','ASC']];
      }

    }
  }  
  console.log(whereClause,array,availability,cutOfftime);
  return new Promise((resolve, reject) => {
    menuItemDAO.findAllMenuItem(whereClause,array,availability,deliverySlot,cutOfftime).then((menuItemData) => {
      if(customerId){
        return Promise.all([menuItemData,CustomerCurrentLocationDAO.getCustomerCurrentLocationByCustomerId(customerId)])
      }else{
        return ([menuItemData]);
      }
    }).then(([menuItemData,custLocation])=>{
      let data = parseData(menuItemData,custLocation,distance);
      if(finalPriceSort=='DESC')
        data.sort(function(a, b) {
        return parseFloat(b.final_price) - parseFloat(a.final_price);
      })
      if(finalPriceSort=='ASC')
        data.sort(function(a, b) {
        return parseFloat(a.final_price) - parseFloat(b.final_price);
      })       
      resolve(data);
    }).catch((err) => {       
      reject(err);
    });
  });
};

exports.fetchMenuItemByChef = (filters,sortBy,chefId) => {
  let whereClause = {chef_id:chefId,is_deleted: false};
  let availability={};
  let deliverySlot={};
  let finalPriceSort;
  let array;
  if (filters) {    
    let conditions = JSON.parse(filters);
    for (let i = 0; i < conditions.length; i++) {
      let field = conditions[i]["field"];
      let predicate = conditions[i]["predicate"];
      let value = conditions[i]["value"];
      if(field == 'rating') {
        whereClause["menu_overall_rating"] = {[Op.gte]:value};
      }else if(field == 'available_date'&&value=='today') {
        availability
        availability["available_date"] = currentDay;
      }else if(field == 'available_date'&&value == 'tomorrow') {
        availability;
        let tomorrow=new Date(currentDay.setDate(currentDay.getDate() + 1));
        availability["available_date"] = tomorrow;
      }else if(field == 'delivery_date') {
        availability={};
        availability["available_date"] = value;
      }else if(field == 'delivery_slot') {
        deliverySlot["slot_label"] = value;
      }else {
        whereClause[field] = value;
      }
    }
  }
  if(sortBy){
    let conditions = JSON.parse(sortBy);
    for (let i = 0; i < conditions.length; i++) {
      let field = conditions[i]["field"];
      let predicate = conditions[i]["predicate"];
      let value = conditions[i]["value"];
      if(field == 'price' && value=='high_to_low') {
        finalPriceSort='DESC';
      }else if(field == 'price' && value=='low_to_high') {
        finalPriceSort='ASC';
      }else if(field == 'rating' && value == 'high_to_low') {
        array=[['menu_overall_rating','DESC']];
      }else if(field == 'rating' && value == 'low_to_high') {
        array=[['menu_overall_rating','ASC']];
      }
    }
  }  
  console.log(whereClause,array,availability,"d",deliverySlot);
  return new Promise((resolve, reject) => {
    menuItemDAO.findAllMenuItem(whereClause,array,availability,deliverySlot).then((menuItemData) => {
        let menuItemVisibility=false;
        let data = parseData(menuItemData);
        if(finalPriceSort=='DESC')
          data.sort(function(a, b) {
          return parseFloat(b.final_price) - parseFloat(a.final_price);
        })
        if(finalPriceSort=='ASC')
          data.sort(function(a, b) {
          return parseFloat(a.final_price) - parseFloat(b.final_price);
        }) 
        // for(let i = 0 ; data.length >= i; i++){
        //   let visibility=data[i].is_visible;          
        //   if(visibility){
        //     menuItemVisibility=true;
        //   }
        // }
        let result={"visibility":menuItemVisibility,"menu_item_count":data.length,"items":data}
        resolve(result);
      }).catch((err) => {       
        reject(err);
      }
    );
  });
};


function parseData(data,customerLocation,distance) {
  let res = [];
  data.map((row) => {
    let result=parseMenuItemData(row);
    let finalResult;
    if(customerLocation){
      let custLocation={};
      custLocation["latitude"]=customerLocation["cus_lat"];
      custLocation["longitude"]=customerLocation["cus_lang"];
      if(result['chef_location']){
        finalResult=distanceCalculation.distanceRadius(custLocation,result['chef_location'],distance) ? result : "distanceIsHigi" ;
        finalResult['distance_km']= distanceCalculation.distanceCalculation(custLocation,result['chef_location']);
      }
      if(finalResult != "distanceIsHigi"&&finalResult){
        res.push(finalResult);
      }
    }else{
      res.push(result);
    }
  });
  return res;
};

exports.getMenu_ItemBymenuTypeId = (menuTypeId) => {
  return new Promise((resolve, reject) => {
    menuItem.findAll({where: { menu_type_id: menuTypeId},["order"]:[['menu_item_id','DESC']],
      include:[{model: kitchenType,where:whereKitchen,as: 'kitchenTypeData'},
      {model: chefModel,as: 'chefData'},
      {model: MenuItemVarietyModel,as: 'varities'},
      {model: MenuFoodTypeModel,as: 'foodType'},
      {model: MenuType,as: 'menuTypeData'}]}).then((categoryData) => {
        let data = parseData(categoryData);
        resolve(data);
      },      
      (err) => {
        reject({ error: err });
      }
    );
  });
};

function parseMenuItemData(data){
  let resultObj = {};
  resultObj["menu_item_id"] = data.menu_item_id;
  resultObj["menu_item_name"] = data.menu_item_name;
  resultObj["menu_item_desc"] = data.menu_item_desc;
  resultObj["menu_item_usp"] = data.menu_item_usp;
  resultObj["menu_item_img"] = data.menu_item_img;
  resultObj["has_variety"] = data.has_variety;
  // if (data['yumma_price_type']=="percentage") {
  //   let yumma_price=data["yumma_price"];
  //   let menuItemPrice=data["menu_item_price"];
  //   let yummaPrice=((yumma_price/ 100) * menuItemPrice);
  //   resultObj["menu_item_price"] = menuItemPrice+yummaPrice;
  // }
  // if (data['yumma_price_type']=="fixed") {
  //   let yummaPrice=data["yumma_price"];
  //   let menuItemPrice=data["menu_item_price"];
  //   resultObj["menu_item_price"] = menuItemPrice+parseFloat(yummaPrice);
  // }
  let menuItemPrice;
  if(data["has_variety"]){
    if(data["varities"].length>=1){
      let price=data.varities[0].dataValues["unit_price"];
      resultObj["menu_item_price"] = price;
      menuItemPrice=price;
    }else{
      resultObj["menu_item_price"] = data.menu_item_price;
      menuItemPrice=data["menu_item_price"];
    }
  }else{
    resultObj["menu_item_price"] = data.menu_item_price;
    menuItemPrice=data["menu_item_price"];
  }
  let yummaPrice=data["yumma_price"];
  //let menuItemPrice=data["menu_item_price"];
  resultObj["final_price"] = menuItemPrice+parseFloat(yummaPrice);
  resultObj["yumma_price"] = data["yumma_price"];
  resultObj["menu_overall_rating"] = data.menu_overall_rating;
  resultObj["total_rating_count"] = data.total_rating_count;
  resultObj["min_dish"] = data.min_dish;
  resultObj["max_dish"] = data.max_dish;
  resultObj["is_visible"] = data.is_visible;
  resultObj["is_main_dish"] = data.is_main_dish;
  resultObj["time_to_prepare"] = data.time_to_prepare;
  resultObj["menu_item_product_type"] = data.menu_item_product_type;
  resultObj["position_size"] = data.position_size;
  resultObj["menu_type_id"] = data.menu_type_id;
  if (data.menuTypeData) {
    resultObj["menu_type"] =data.menuTypeData.menu_type;
    resultObj["menu_type_code"] =data.menuTypeData.menu_type_code;
  }
  resultObj["course_id"] = data.course_id;
  if (data.course) {
    resultObj["course_name"] =data.course.course_name;
  }
  resultObj["food_type_id"] = data.food_type_id;
  if (data.foodType) {
    resultObj["foodType"] =data.foodType.food_type;
    resultObj['food_type_code'] = data.foodType.food_type_code;
  }  
  resultObj["chef_id"] = data.chef_id;
  if (data.chefData) {
    resultObj["chef_fullname"] = data.chefData.chef_fullname;
    resultObj["chef_location"] = {"latitude":data.chefData.chef_lat,"longitude":data.chefData.chef_lang};
    resultObj["kitchen_image_URL"] = imageURL.setChefDocumentPath()+data.chefData.kitchen_image;
  }
  resultObj["kitchen_type_id"] = data.kitchen_type_id;
  if (data.kitchenTypeData) {
    resultObj["kitchen_type"] = data.kitchenTypeData.kitchen_type;
    resultObj['kitchen_type_code'] = data.kitchenTypeData.kitchen_type_code;
  }
  resultObj["root_category_id"] = data.root_category_id;
  if (data.sub_category) {
    resultObj["sub_category_name"] =data.sub_category['category_name'];
  }
  if (data.root_category) {
    resultObj["root_category_name"] =data.root_category['category_name'];
  }
  resultObj["sub_category_id"] = data.sub_category_id;
  if (data.menu_item_img) {
    resultObj["menu_Item_Img_URL"] = imageURL.menuItemImage_URL() + data.menu_item_img;
  }
  if (data.availability) {
    resultObj["availability"] =data.availability;
  }
  if (data.varities) {
    resultObj["varities"] =data.varities;
  }
  if (data.menuItemCutoffTime) {
    resultObj["menuItemCutoffTime"] =data.menuItemCutoffTime;
  }
  if (data.inclusions) {
    resultObj["inclusions"] =data.inclusions;
  }
  if (data.addon) {
    let addonData=parseAddonData(data.addon)
    resultObj["addon"] =addonData;
  }
  if (data.delivery_time_slot_msts) {
    resultObj["delivery_time_slot"] =data.delivery_time_slot_msts;
  }
  return resultObj;
}
function parseAddonData(data) {
  let res = [];
  data.map((row) => {
    let result={};
    result["addon_id"] = row.addon.addon_id;
    result["addon_name"] = row.addon.addon_name;
    result["addon_image"] = row.addon.addon_image;
    result["price"] = row.addon.price;
    result["qty"] = row.addon.qty;
    result["addon_image_URL"] = imageURL.menuItemImage_URL()+row.addon.addon_image;
    res.push(result);
  });
  return res;
};

exports.fetchById = (menuItemId) => {
  return new Promise((resolve, reject) => {
    menuItemDAO.findMenuItemById(menuItemId).then((data) => {
      let result=parseMenuItemData(data);
      resolve(result);
    }).catch((err) => {
      reject(err);
    });
  });
};

function menuItemRatingCount (menuItemId) {
  return new Promise((resolve, reject) => {
    customerReviewDAO.getCustomerReviewCount({menu_item_id:menuItemId}).then((res) => {
      console.log(res);
      resolve(res);
    }).catch((err) => {
      console.log(err);
      reject(err);
    });
  });
};

function menuItemOverallRating(menuItemId) {
  return new Promise((resolve, reject) => {
    customerReviewDAO.getCustomerReviewByChefId({menu_item_id:menuItemId}).then(menuReview => {
      let menuTotalRating=0;
      for(let i=0;i<menuReview.length;i++){
        menuTotalRating = parseFloat(menuTotalRating)+parseFloat(menuReview[i]['overall_rating']);
      }
      let menuRatingCount=menuReview.length;
      let menuResult=parseFloat(menuTotalRating)/menuRatingCount;
      console.log(menuResult)
      resolve(menuResult);
    }).catch(err => {
      reject(err);
    });
  });
};

let accompainmentData = (data) => {
  let res = [];
  data.map((row) => {
    res.push(row.menu_item_id);
  });
  return res;
};


exports.getMenuItemAccompanimentBychefId = (chefId) => {
  return new Promise((resolve, reject) => {
    menuItem.findAll({where: { chef_id: chefId,is_main_dish: true,is_deleted: false}}).then((data) => {
    let response=accompainmentData((data));
    resolve(response);
    })      
    .catch(err => {
      reject(err);
    });
  });
};

exports.fetchDeliverySlot = (menuItemId) => {
  return new Promise((resolve, reject) => {
    let menuItemIds = menuItemId.split(",");
    menuItemDAO.fetchDeliveryTimeSlot(menuItemIds).then((data) => {
      resolve(data);
    }).catch((err) => {
      reject(err);
    });
  });
};