const yummaCommissionDAO = require("../dao/yumma_commission.dao");



exports.createYummaCommission = (data) => {
  return new Promise((resolve, reject) => {
    yummaCommissionDAO.saveYummaCommission(data).then((res) => {
        resolve(res);
    }).catch((err) => {
        reject(err);
    });
  });
};

exports.findAllYummaCommission = () => {
  return new Promise((resolve, reject) => {
    yummaCommissionDAO.findAllYummaCommission().then((res) => {
        resolve(res);
    }).catch((err) => {
        reject(err);
    });
  });
};
  