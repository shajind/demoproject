const model = require('../models');
const MenuFoodTypeModel = model.MenuFoodType;
const core = require("../helpers/core.helper");

exports.createMenuFoodType = (payload) => {    
    return new Promise((resolve, reject) => {
        let MenuFoodTypeCode = core.createCode(payload['food_type']);
        payload['food_type_code'] = MenuFoodTypeCode;
        console.log(payload);
        MenuFoodTypeModel.create(payload).then(response => {
        	resolve(response);
        }).catch(err => {                    
        	reject({error:err});
        });
    });
};

exports.findAllMenuFoodType = () =>{    
    return new Promise((resolve,reject) => {               
        MenuFoodTypeModel.findAll().then(data =>{
            resolve(data);            
        }).catch(err => {                    
        	reject({error:err});
        });
    });
};