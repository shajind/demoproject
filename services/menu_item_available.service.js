const model = require("../models");
const menuModel = model.menu_item_available;
const menuItem = model.menu_items;
const dayAvailableModel=model.day_available_menu_item;
const imageURL = require("../utils/image_URL-generator");

let daysAvailable=(data)=>{  
  let day_available=data["days_available"].split(",");
  let menu_item_id=data["menu_item_id"];
  let dayAvailableData=[];
  for (let i = 0; i < day_available.length; i++) {
    resultObj={};
    resultObj["available_day"]=day_available[i];
    resultObj["menu_item_id"]=menu_item_id;
    dayAvailableData.push(resultObj);
  }
  console.log("data...................",dayAvailableData);
  dayAvailableModel.bulkCreate(dayAvailableData)
  return
}
exports.saveMenuItemAvailable = (menuData) => {
  return new Promise((resolve, reject) => {
    menuModel.create(menuData).then((response) => {
        console.log(response);
        if(response.days_available){
          let dayAvailableData=daysAvailable((response))
        }        
        resolve(response);
      },
      (err) => {
        reject({ error: err });
      }
    );
  });
};

let menuItemAvailableAllData = (data) => {
  let res = [];
  data.map((row) => {
    let menuItemAvailableData = menuItemData(row);
    res.push(menuItemAvailableData);
  });
  return res;
};

exports.getAllMenuItemAvailable = () => {
  return new Promise((resolve, reject) => {
    menuModel.findAll({include:[{model: menuItem,as: 'menuItemData'}]}).then(
      (data) => {
        let response=menuItemAvailableAllData((data))
        console.log(response);
        resolve(response);
      },
      (err) => {
        reject({ error: err });
      }
    );
  });
};

let menuItemData=(data)=>{
  if(!data) {
    return null;
  }
  let resultObj={};
  resultObj["menu_item_available_id"] = data.menu_item_available_id;
  resultObj["days_available"] = data.days_available;
  resultObj["available_from_time"] = data.available_from_time;
  resultObj["available_to_time"] = data.available_to_time;
  resultObj["available_status"] = data.available_status;
  resultObj["time_to_prepare"] = data.menuItemData.time_to_prepare;
  resultObj["menu_item_id"] = data.menuItemData.menu_item_id;
  resultObj["menu_item_name"] = data.menuItemData.menu_item_name;
  resultObj["menu_item_desc"] = data.menuItemData.menu_item_desc;
  resultObj["menu_item_usp"] = data.menuItemData.menu_item_usp;
  resultObj["menu_item_img"] = data.menuItemData.menu_item_img;
  resultObj["menu_item_price"] = data.menuItemData.menu_item_price;
  resultObj["menu_overall_rating"] = data.menuItemData.menu_overall_rating;
  resultObj["min_dish"] = data.menuItemData.min_dish;
  resultObj["max_dish"] = data.menuItemData.max_dish;
  resultObj["is_published"] = data.menuItemData.is_published;
  resultObj["is_main_dish"] = data.menuItemData.is_main_dish;
  resultObj["menu_item_product_type"] = data.menuItemData.menu_item_product_type;
  resultObj["menu_type_id"] = data.menuItemData.menu_type_id;
  resultObj["menu_item_type_id"] = data.menuItemData.menu_item_type_id;
  resultObj["category_ids"] = data.menuItemData.category_ids;
  resultObj["kitchen_type_id"] = data.menuItemData.kitchen_type_id;
  resultObj["chef_id"] = data.menuItemData.chef_id;
  if (data.menuItemData.menu_item_img) {
    resultObj["menu_Item_Img_URL"] = imageURL.menuItemImage_URL()+ data.menuItemData.menu_item_img;
  }
  return resultObj
}

exports.getMenuItemAvailableByMenuItemId = (MenuItemId) => {
  return new Promise((resolve, reject) => {
    menuModel.findOne({ where: { menu_item_id: MenuItemId },
      include:[{model: menuItem,as: 'menuItemData'}] }).then((data) => {
        let response=menuItemData((data))
        resolve(response);
      },
      (err) => {
        reject({ error: err });
      }
    );
  });
};

exports.getMenuItemAvailableAccompanimentByMenuItemId = (MenuItemId) => {
  return new Promise((resolve, reject) => {
    menuModel.findAll({ where: { menu_item_id: MenuItemId,available_status:"available"},
      include:[{model: menuItem,as: 'menuItemData'}]}).then((data) => {
      let response=menuItemAvailableAllData((data))
        resolve(response);
      })
      .catch(err => {
        reject(err);
      })
  });
};
