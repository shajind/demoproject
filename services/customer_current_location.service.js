const CustomerCurrentLocationDAO = require("../dao/customer_current_location.dao");



exports.createCustomerCurrentLocation = (data,customerId) => {
    return new Promise((resolve, reject) => {
        CustomerCurrentLocationDAO.getCustomerCurrentLocationByCustomerId(customerId)
        .then((res) => {
            if(res){
                return CustomerCurrentLocationDAO.updateCustomerCurrentLocation(data,customerId);
            }else{
                return CustomerCurrentLocationDAO.saveCustomerCurrentLocation(data);
            }
        }).then((res) => {
            resolve(res);
        }).catch((err) => {
            reject(err);
        });
    });
};

exports.fetchCustomerCurrentLocationByCustomerId = (customerId) => {
    return new Promise((resolve, reject) => {
        CustomerCurrentLocationDAO.getCustomerCurrentLocationByCustomerId(customerId)
        .then((res) => {
            resolve(res);
        }).catch((err) => {
            reject(err);
        });
    });
};
  