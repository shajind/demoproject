const model = require('../models');
const bannerModel = model.banner;
const imageURL = require("../utils/image_URL-generator");

exports.saveBanner = (payload) => {    
    return new Promise((resolve, reject) => { 
    bannerModel.create(payload).then(response => {                  
            console.log(response);
        	resolve(response);
        },err => {                    
        	reject({error:err});
        });
    });
};

exports.updateBanner = (payload,banner_id) => {    
    return new Promise((resolve, reject) => { 
    bannerModel.update(payload,{where:{banner_id:banner_id}}).then(response => {                  
            console.log(response);
        	resolve(response);
        },err => {                    
        	reject({error:err});
        });
    });
};

let parseData = (data) => {
    let res = [];
    data.map((row) => {
      let resultObj = {};
      resultObj["banner_id"] = row.banner_id;
      resultObj["banner_type"] = row.banner_type;
      resultObj["banner_title"] = row.banner_title;
      resultObj["banner_image"] = row.banner_image;
      resultObj["banner_destination_page"] = row.banner_destination_page;
      resultObj["banner_target"] = row.banner_target;
      resultObj["status"] = row.status;
      resultObj["createdAt"] = row.createdAt;
      resultObj["banner_Image_URL"] = imageURL.setbannertPath()+ row.banner_image;
      res.push(resultObj);
    });
    return res;
};

exports.getAllBanner = () =>{    
    return new Promise((resolve,reject) => {               
        bannerModel.findAll().then(bannerData =>{
            let data = parseData(bannerData);
            resolve(data);            
        },err => {                    
        	reject({error:err});
        });
    });
};

exports.getAllBannerByType = (bannerType) =>{    
    return new Promise((resolve,reject) => {               
        bannerModel.findAll({ where : {banner_type:bannerType}}).then(bannerData =>{
            let data = parseData(bannerData);
            resolve(data);            
        },err => {                    
        	reject({error:err});
        });
    });
};