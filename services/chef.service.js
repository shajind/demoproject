const model = require("../models");
const chefModel = model.chefs;
const userModel = model.users;
const stateModel = model.state;
const menuItem = model.menu_items;
const cityModel = model.city;
const MenuItemAvailable=model.menu_item_available;
const MenuItemDeliveryTimeSlotMaster = model.deliveryTimeSlotMaster;
const UserRole=model.UserRole;
const sequelize = require('sequelize');
const { Op } = require("sequelize");
const otpGenerator = require("../utils/otp-generater");
const menuModel = model.menus;
const chefDAO = require('../dao/chef.dao');
const userService = require('../services/users.service');
const userDAO = require("../dao/user.dao");
const roleService = require('../services/role.service');
const tokenGenerator = require('../utils/token-generator');
const roleConstant = require("../config/role.json");
const userHelper = require("../helpers/user.helper");
const imageURL = require("../utils/image_URL-generator");
const CustomerCurrentLocationDAO = require("../dao/customer_current_location.dao");
const distanceCalculation = require("../utils/distance_duration");

exports.updateChefProfile = (userChefdata) => {
  return new Promise((resolve, reject) => {
    chefModel
      .update(userChefdata, { where: { user_id: userChefdata.user_id } })
      .then(
        (response) => {
          if (response == 0) {
            chefModel.create(userChefdata).then((response) => {
              console.log(response);
              resolve(response);
            });
          } else resolve(response);
        },
        (err) => {
          reject({ error: err });
        }
      );
  });
};

exports.createChef = (data, chef_role_code) => {
  return new Promise((resolve, reject) => {
    userService.fetchUser(data.user_mobile, data.email).then(response => {
        if(response) {
          chefDAO.findChef(response['user_id']).then(chef => {
            if(chef) {
              //throw new Error("Chef Already Exist");
              resolve("Chef Already Exist");
            } else {
              roleService.fetchRoleByCode(chef_role_code).then(role => {
                userDAO.createRoleForExistingUser(response.user_id, role).then(user => {
                    data['user_id'] = user['user_id'];
                    chefDAO.saveChef(data).then(result => {
                      resolve(result);
                    }).catch(err => {
                      reject(err);
                    })
                }).catch(err => {
                  reject(err);
                })
              }).catch(err => {
                  reject(err);
              });
            }
          }).catch(err => {
            reject(err);
          })
        } else {
          roleService.fetchRoleByCode(chef_role_code).then(role => {
              let userData = {};
              userData["user_name"] = data["chef_fullname"];
              userData['mobile'] = data['user_mobile'];
              userData['email'] = data['user_email'];
              userDAO.saveUser(userData, role).then(user => {
                data['user_id'] = user['user_id'];
                chefDAO.saveChef(data).then(result => {
                  resolve(result);
                })
            });
          }).catch(err => {
            reject(err);
          });
        }
      }
    ).catch(err => {
        reject(err);
    });
  });
};

function parseData(data,customerLocation,distance) {
  let res = [];
  data.map((row) => {
    let result=chefData(row);
    let finalResult;
    if(customerLocation){
      let custLocation={};
      custLocation["latitude"]=customerLocation["cus_lat"];
      custLocation["longitude"]=customerLocation["cus_lang"];      
      if(row["chef_lat"]&&row["chef_lang"]){
        let chefLocation={};
        chefLocation["latitude"]=row["chef_lat"];
        chefLocation["longitude"]=row["chef_lang"];
        finalResult=distanceCalculation.distanceRadius(custLocation,chefLocation,distance) ? result : "distanceIsHigi" ;
        finalResult['distance_km']= distanceCalculation.distanceCalculation(custLocation,chefLocation);
      }
      if(finalResult != "distanceIsHigi"&&finalResult){
        res.push(finalResult);
      }
    }else{
      res.push(result);
    }
  });
  return res;
};

let chefData = (data) => {
  let resultObj = {};
    resultObj["chef_id"] = data.chef_id;
    resultObj["chef_fullname"] = data.chef_fullname;
    resultObj["chef_gender"] = data.chef_gender;
    resultObj["chef_dob"] = data.chef_dob;
    resultObj["chef_cuisine"] = data.chef_cuisine;
    resultObj["chef_profile_pic"] = data.chef_profile_pic;
    resultObj["chef_address1"] = data.chef_address1;
    resultObj["chef_address2"] = data.chef_address2;
    resultObj["chef_city"] = data.chef_city;
    resultObj["chef_state"] = data.chef_state;
    resultObj["chef_zipcode"] = data.chef_zipcode;
    resultObj["chef_type"] = data.chef_type;
    resultObj["chef_about_yourself"] = data.chef_about_yourself;
    resultObj["status"] = data.status;
    resultObj["createdAt"] = data.createdAt;
    resultObj["updatedAt"] = data.updatedAt;
    resultObj["user_id"] = data.user_id;
    resultObj["overall_rating"] = data.overall_rating;
    resultObj["total_rating_count"] = data.total_rating_count;
    resultObj["kitchen_image"] = data.kitchen_image;
    resultObj["country_code"] = data.country_code;
    resultObj["chef_about_yourself"] = data.chef_about_yourself;
    resultObj["chef_kitchen_timing"] = data.chef_kitchen_timing;
    resultObj["chef_pure_veg_kitchen"] = data.chef_pure_veg_kitchen;
    resultObj["chef_lat"] = data.chef_lat;
    resultObj["chef_lang"] = data.chef_lang;
    if(data.cityData) {
      resultObj["city"] = data.cityData["city"];
    }
    if(data.stateData) {
      resultObj["state"] = data.stateData["state"];
    }
    let menuItem=data.menu_items;
    if(data.menu_items) {
      resultObj["chef_menu_items_count"] = menuItem.length;
    }    
    let menuItemPriceList=[];
    let subCategoryName=[];
    for(let i=0;i<menuItem.length;i++){ 
      let menuItemPrice;
      let menuItemLength=menuItem[i].dataValues;
      if(menuItemLength["has_variety"]&&menuItemLength["varities"]){
        if(menuItemLength["varities"].length>=1){
          //menuItemPrice=menuItemLength["varities"].varities[0].dataValues["unit_price"];
          menuItemPrice=89;
        }else{
          menuItemPrice=menuItemLength['menu_item_price'];
        }
      }else{
        menuItemPrice=menuItemLength['menu_item_price'];
      }
      let yummaPrice=menuItemLength["yumma_price"];
      let finalPrice = menuItemPrice+parseFloat(yummaPrice);
      let menuItemPrice_l=finalPrice;
      let subCategory;
      if (menuItemLength['sub_category']) {
        subCategory = menuItemLength['sub_category'].dataValues['category_name'];
      }
      //let subCategory=menuItemLength['sub_category_name'];
      menuItemPriceList.push(menuItemPrice_l);
      subCategoryName.push(subCategory);
    }
    let menuItemSortPrice=menuItemPriceList.sort(function(a, b){return a-b});
    let price_s=0;
    if(menuItemSortPrice[0] == 0){
      price_s = menuItemSortPrice[1];
    }else{
      price_s = menuItemSortPrice[0];
    }
    resultObj["starting_price"] = price_s;
    resultObj["cuisines"] =[...new Set(subCategoryName)];

    //resultObj["city"] = data.cityData.city;  
    //resultObj["state"] = data.stateData.state;
    if(data.usersData) {      
      resultObj["user_name"] = data.usersData.user_name;
      resultObj["user_email"] = data.usersData.user_email;
      resultObj["user_mobile"] = data.usersData.user_mobile;
      resultObj["user_status"] = data.usersData.status;
    }
    resultObj["chef_profile_pic_URL"] = imageURL.setChefImagePath()+data.chef_profile_pic;
    resultObj["chef_cover_pic_URL"] = imageURL.setChefImagePath()+data.chef_profile_cover_pic;
    resultObj["kitchen_image_URL"] = imageURL.setChefDocumentPath()+data.kitchen_image;
    return resultObj;
};

exports.getChefProfile = (id) => {
  return new Promise((resolve, reject) => {
    chefModel.findOne({where: { user_id: id },include: [{model: userModel,
      as: "usersData",attributes: ["user_id","user_name","user_email",
      "user_mobile","status"]},{model: stateModel,as: "stateData"},
      {model: menuItem,as: "menu_items",
      include:[{model: MenuItemAvailable,as: 'availability'}],
      raw: true,      
      },
      {model: cityModel,as: "cityData"}]})
      .then(response => {
        let result=chefData(response);
          resolve(result);
        },
        (err) => {
          reject({ error: err });
          console.log("not working");
        }
      );
  });
};

exports.fetchChefByUserId = (userId) => {
  return new Promise((resolve, reject) => {
    chefDAO.findChef(userId).then(data => {
      resolve(data);
    }).catch(err => {
      reject(err);
    })
  });
};

exports.getChefProfileBychefId = (id) => {
  return new Promise((resolve, reject) => {
    chefModel
      .findOne({
        where: { chef_id: id },
      })
      .then(
        (response) => {
          resolve(response);
          console.log(" working");
        },
        (err) => {
          reject({ error: err });
          console.log("not working");
        }
      );
  });
};

exports.getAllChefs = (filters,sortBy,customerId) => {
  let currentDay = new Date();
  let availability={'available_date':{[Op.gte]:currentDay}};
  let deliverySlot={};
  let whereClause = {};
  let whereClauseItem = {};
  let distance=100;
  let finalPriceSort;
  let array=[['chef_id','DESC']];
  if (filters) {    
    let conditions = JSON.parse(filters);
    for (let i = 0; i < conditions.length; i++) {
      let field = conditions[i]["field"];
      let predicate = conditions[i]["predicate"];
      let value = conditions[i]["value"];
      if(field == 'root_category_id') {
        whereClauseItem["root_category_id"] = value;
      }else if(field == 'sub_category_id') {
        whereClauseItem["sub_category_id"] = value;
      }else if(field == 'food_type_id') {
        whereClauseItem["food_type_id"] = value;
      }else if(field == 'delivery_date') {
        availability["available_date"] = value;
      }else if(field == 'delivery_slot') {
        deliverySlot["slot_label"] = value;
      }else if(field == 'distance') {
        distance = value;
      }else {
        whereClause[field] = value;
      }
    }
  }
  if(sortBy){
    let conditions = JSON.parse(sortBy);
    for (let i = 0; i < conditions.length; i++) {
      let field = conditions[i]["field"];
      let predicate = conditions[i]["predicate"];
      let value = conditions[i]["value"];
      if(field == 'rating' && value == 'high_to_low') {
        array=[['overall_rating','DESC']];
      }else if(field == 'rating' && value == 'low_to_high') {
        array=[['overall_rating','ASC']];
      }else if(field == 'distance' && value == 'high_to_low') {
        finalPriceSort='DESC';
      }else if(field == 'distance' && value == 'low_to_high') {
        finalPriceSort='ASC';
      }
    }
  } 
  console.log(whereClauseItem,whereClause,array,availability,deliverySlot)
  return new Promise((resolve, reject) => {
    chefDAO.findAllChef(whereClauseItem,whereClause,array,availability,deliverySlot)
    .then(chefData=> {
      if(customerId){
        return Promise.all([chefData,CustomerCurrentLocationDAO.getCustomerCurrentLocationByCustomerId(customerId)])
      }else{
        return ([chefData]);
      }
    }).then(([chefData,custLocation])=>{
      let result=parseData(chefData,custLocation,distance);
      if(finalPriceSort=='DESC'&&customerId){
        result.sort(function(a, b) {
          return parseFloat(b.distance_km) - parseFloat(a.distance_km);
        })
      }
      if(finalPriceSort=='ASC'&&customerId){
        result.sort(function(a, b) {
          return parseFloat(a.distance_km) - parseFloat(b.distance_km);
        })
      }
      resolve(result);
    }).catch(err => {
      reject(err);
    });
  });
};

exports.saveChefLoginOTP = (mobile) => {
  return new Promise((resolve, reject) => {
    userDAO.findUserByMobileOrEmail(mobile).then(user => {
      if(!user) {
          throw new Error("Please do a registration");
      }
      return Promise.all([user, chefDAO.findChef(user['user_id'])]);
    }).then(([user, chef]) => {
        let otp = "00000";//otpGenerator.OTPpassword();
        user.dataValues['user_otp'] = otp;
        return Promise.all([chef, userDAO.updateUser(user['user_id'], user.dataValues)]);
    }).then(([chef, user]) => {
        user.dataValues['chef'] = chef;
        resolve(user);
    })
    .catch(err => {
        reject(err);
    });
  });
};

exports.generateAndSaveLoginOTP = (u_id) => {
  return new Promise((resolve, reject) => {
    //let otp = otpGenerator.OTPpassword();
    let otp = "00000";
    let userOtp = { user_otp: otp };
    console.log("u_id", u_id, userOtp);
    userModel
      .update(userOtp, { where: { user_id: u_id } })
      .then((res) => {
        console.log(res);
        resolve(res);
      })
      .catch((err) => {
        console.log(err);
        reject(err);
      });
  });
};

exports.chefUpdate = (payload,chefId) => {
  return new Promise((resolve, reject) => {
    chefModel.update(payload,{ where: { chef_id: chefId } })
      .then((res) => {
        console.log(res);
        resolve(res);
      })
      .catch((err) => {
        console.log(err);
        reject(err);
      });
  });
};

exports.chefStatusUpdate = (payload,chefId) => {
  return new Promise((resolve, reject) => {
    console.log(chefId,payload)
    chefDAO.update(chefId,payload).then((res) => {
      return userDAO.updateUser(res['user_id'],payload)
    }).then(data =>{
        resolve(data);
      })
      .catch((err) => {
        console.log(err);
        reject(err);
      });
  });
};

function formatChefResponse(data) {
  let resultObj = {};
  resultObj["chef_id"] = data.chef_id;
  resultObj["chef_fullname"] = data.chef_fullname;
  resultObj["chef_gender"] = data.chef_gender;
  resultObj["chef_dob"] = data.chef_dob;
  resultObj["chef_cuisine"] = data.chef_cuisine;
  resultObj["chef_address"] = data.chef_address;
  resultObj["chef_city"] = data.chef_city;
  resultObj["chef_state"] = data.chef_state;
  resultObj["chef_zipcode"] = data.chef_zipcode;
  resultObj["chef_type"] = data.chef_type;  
  resultObj["chef_lat"] = data.chef_lat;
  resultObj["chef_lang"] = data.chef_lang;
  resultObj["status"] = data.status;
  if(data.cityData) {
    resultObj["city"] = data.cityData["city"];
  }
  if(data.stateData) {
    resultObj["state"] = data.stateData["state"];
  }
  resultObj["chef_profile_pic_URL"] = imageURL.setChefImagePath()+data.chef_profile_pic;
  resultObj["chef_cover_pic_URL"] = imageURL.setChefImagePath()+data.chef_profile_cover_pic;
  resultObj["kitchen_image_URL"] = imageURL.setChefDocumentPath()+data.kitchen_image;
  return resultObj;
}

exports.chefLogin = (userName, password) => {
  return new Promise((resolve, reject) => {
    userService.login(userName, password).then(data => {
      return Promise.all([data, chefDAO.findChef(data["user_id"])]);
    }).then(([user, chef]) => {
      user.dataValues['chef'] = formatChefResponse(chef);
      let role = userHelper.formatRoles(user, roleConstant.ROLE_CHEF);
      user['access_token'] = tokenGenerator.genToken(user['user_id'], role['role_code']);
      resolve(user);
    }).catch(err => {
      reject(err);
    })
  })
}

exports.fetchChefById = (chefId) => {
  if(!chefId){
    return {"chef_fullname":"yumma","chef_id":"0"}
  }
  return new Promise((resolve, reject) => {
    chefDAO.findChefById(chefId).then(chef => {
      resolve(chef);
    }).catch(err => {
      reject(err);
    })
  })
}

exports.updateChefProfile1 = (userId, chefId, payload) => {
  return new Promise((resolve, reject) => {
    Promise.all([userDAO.updateUserData(userId, payload), 
      chefDAO.updateProfile(chefId, payload)]).then(([user, res2]) => {
        userHelper.formatRoles(user, roleConstant.ROLE_CHEF);
        user.dataValues['chef'] = formatChefResponse(res2);;
        resolve(user);
    }).catch(err => {
      reject(err);
    })
  })
}

exports.updateChefDocument = (chefId, payload) => {
  return new Promise((resolve, reject) => {
    if(!payload['docName']) {
      throw new Error("Document Name not found");
    }
    chefDAO.findChefById(chefId).then(chef => {
      let data = chef.dataValues;
      if(payload['docName'] == 'kitchen_pic') {
        data['kitchen_image'] = payload['file_name'];
      }
      return chefDAO.update(chefId, data);
    }).then((res) => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}