const model = require("../models");
const MenuType = model.menu_types;

exports.saveMenuType = (menuType) => {
  return new Promise((resolve, reject) => {
    MenuType.create(menuType)
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

exports.getAllMenuType = () => {
  return new Promise((resolve, reject) => {
    MenuType.findAll().then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

exports.getMenuTypeByCode = (code) =>{    
  return new Promise((resolve,reject) => {               
    MenuType.findOne({where:{menu_type_code:code}}).then(data =>{
          console.log("MenuType by code ================", data['menu_type_id']);          
          resolve(data);            
      })
      .catch((err) => {                    
        reject({error:(err)});
      });
  });
};
