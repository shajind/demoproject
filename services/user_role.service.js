const model = require('../models');
const userRole = model.role;
const userTypeDAO = require('../dao/user_type.dao');

exports.saveUser_Type = (user_typedata) => {    
    return new Promise((resolve, reject) => {
        userTypeDAO.createUserType(user_typedata).then(response => {                  
            console.log(response);
        	resolve(response);
        }).catch((err) => {        
        	reject(err.error.parent.sqlMessage);
        });
    });
};

exports.getUser_Type = () =>{    
    return new Promise((resolve,reject) => {               
        userRole.findAll().then(data =>{
            console.log(data);            
            resolve(data);            
        },err => {                    
        	reject({error:err});
        });
    });
};

exports.getUserTypeByCode = (code) =>{    
    return new Promise((resolve,reject) => {               
        userRole.findOne({where:{role_code:code}}).then(data =>{
            console.log("user_type by code ================", data['role_id']);          
            resolve(data);            
        },err => {                    
        	reject({error:err});
        });
    });
};

exports.getUserType = (userTypeId) => {    
    return new Promise((resolve,reject) => {               
        userType.findOne({where:{user_type_id:userTypeId}}).then(data =>{         
            resolve(data);            
        },err => {                    
        	reject({error:err});
        });
    });
};