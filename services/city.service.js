const model = require('../models');
const City = model.city;

exports.saveCity = (citydata) => {    
    return new Promise((resolve, reject) => {        
        City.create(citydata).then(response => {                  
            console.log(response);
        	resolve(response);
        },err => {                    
        	reject({error:err});
        });
    });
};

exports.getCityBystateId = (stateId) =>{
    let whereClause;
    if(stateId){
        whereClause={state_id:stateId};
    }else{
        whereClause={};
    }    
    return new Promise((resolve,reject) => {               
        City.findAll({where:whereClause}).then(data =>{
            resolve(data);            
        }).catch(err => {                    
        	reject({error:err});
        });
    });
};