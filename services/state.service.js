const model = require('../models');
const State = model.state;

exports.saveState = (statedata) => {    
    return new Promise((resolve, reject) => {        
        State.create(statedata).then(response => {                  
            console.log(response);
        	resolve(response);
        },err => {                    
        	reject({error:err});
        });
    });
};


exports.getState = () =>{    
    return new Promise((resolve,reject) => {               
        State.findAll().then(data =>{
            console.log(data);            
            resolve(data);            
        },err => {                    
        	reject({error:err});
        });
    });
};