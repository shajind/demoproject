const model = require("../models");
const Cart = model.Cart;
const CartItem = model.CartItem;
const CartAddress = model.CartAddress;
const menuItemService = require("./menu_item.service");
const chefService = require("./chef.service");
const addonDAO = require("../dao/addon.dao");
const distanceCalculation = require("../utils/distance_duration");
const couponDAO = require("../dao/coupon.dao");
const taxDAO = require("../dao/tax.dao");
const cartDAO = require("../dao/cart.dao");
const customerService = require("../services/customer.service");
const imageURL = require("../utils/image_URL-generator");
const customerAddressService = require("../services/customer-address.service");
const deliveryChargeDAO = require("../dao/delivery_charge.dao");
const { response } = require("express");
const chefDAO = require('../dao/chef.dao');

exports.fetchCartList = (customerId,cartId) => {
  return new Promise((resolve, reject) => {
    cartDAO.findActiveCartList(customerId,cartId).then((cartData) => {
      return Promise.all([cartData,taxDAO.fetchTax('gst')])
    }).then(([cartData,tax])=>{
        let result=parseData(cartData);
        for(let i=0;i<result.length;i++){
          let taxData=result[i];
          taxData.dataValues['tax']=tax.dataValues;
        }
        resolve(result);
      }).catch((err) => {
        reject(err);
      });
  });
};

function parseData(data) {
  let res = [];
  data.map((row) => {
    let result=parsecartItemData(row);
    res.push(result);
  });
  return res;
};

function saveCartAvailabilityAndDeliverySlot(menuItem, cart, cartItem) {
  return new Promise((resolve, reject) => {
    if(cart['savedAvailabilityDeliveryTimeSlot']) {
      Promise.all([menuItem["availability"], cartDAO.findCartAvailability(cart['cartId'])])
      .then(([menuAvailaility, cartAvailability]) => {
        for (let i = 0; i < menuAvailaility.length; i++) {
          let availability = menuAvailaility[i].dataValues;
          let menuAvailabileDate = availability["available_date"];
          let fallUnderDate = false;          
          for(let j =0; j < cartAvailability.length;j++) {
            let cartAvailabe = cartAvailability[j].dataValues;
            let cartAvailableDate = cartAvailabe["available_date"];
            if(menuAvailabileDate == cartAvailableDate) {
              fallUnderDate = true;
              break;
            }
          }
          if(!fallUnderDate) {
            throw new Error("Menu Item Not available on this date");
          }
        }
        resolve("success");
      }).catch(err => {
        reject(err);
      })
    } else {
      let availabilites=menuItem["availability"];
      let deliveryTimeSlots=menuItem["delivery_time_slot"];
      let availabilityResults=[];
      let deliveryTimeSlotsResult=[];
      if(availabilites){
        for (let i = 0; i < availabilites.length; i++) {
          let availability = availabilites[i].dataValues;
          let cartAvailability = {};
          cartAvailability["available_date"] = availability["available_date"];
          cartAvailability["cartId"] = cartItem["cartId"];
          cartAvailability["cartItemId"] = cartItem["cartItemId"];
          availabilityResults.push(cartAvailability);
        }
      }
      if(deliveryTimeSlots){
        for (let i = 0; i < deliveryTimeSlots.length; i++) {
          let deliveryTimeSlot = deliveryTimeSlots[i].dataValues;
          let cartDeliveryTimeSlot = {};
          cartDeliveryTimeSlot["slot_from_time"] = deliveryTimeSlot["slot_from_time"];
          cartDeliveryTimeSlot["slot_to_time"] = deliveryTimeSlot["slot_to_time"];
          cartDeliveryTimeSlot["slot_label"] = deliveryTimeSlot["slot_label"];
          cartDeliveryTimeSlot["cartId"] = cartItem["cartId"];
          cartDeliveryTimeSlot["cartItemId"] = cartItem["cartItemId"];
          deliveryTimeSlotsResult.push(cartDeliveryTimeSlot);
        }
      }
      Promise.all([cartDAO.saveCartAvailability(availabilityResults),cartDAO.saveCartDeliveryTimeSlot(deliveryTimeSlotsResult)])
      .then(([avaData, deliveryTimeSlotData]) => {
        payload={};
        payload['savedAvailabilityDeliveryTimeSlot']=true;
        return cartDAO.updateCart(cart['cartId'],payload );
      }).then(res=>{
        resolve(res);
      }).catch(err => {
          reject(err);
      })
    }
  })
}

exports.createCartAndAddItem = (cartType, payload) => {
  return new Promise((resolve, reject) => {
    let menuItemId = payload["menu_item_id"];
    let varietyLinkId = payload["variety_link_id"];
    let qty = payload["qty"];
    let userId = payload["user_id"];
    let cartId = payload['cart_id'];
    cartDAO.findCartById(cartId).then(cart_res => {
      if(cart_res['cartType'] != cartType){
        throw new Error("Cart type  mismatched");
      }
      if(cart_res['cartType'] == 'order_now'){
        let cartItem=cart_res['cart_items'];
        if(cartItem.length >= 1){
          if(menuItemId!=cartItem[0]['menuItemId']){
            throw new Error("This cart has already added item");
          } 
        }
      }
      if(cart_res['deliveryDate'] && cart_res['delivery_time_slot']) {
        if(cart_res['deliveryDate'] != payload['delivery_date']) {
          throw new Error("The delivery date or slot of this item does not match with the items in the cart");
        }
        if(cart_res['delivery_time_slot'] != payload['delivery_time_slot']) {
          throw new Error("The delivery date or slot of this item does not match with the items in the cart");
        }
      }
      return Promise.all(([cart_res, menuItemService.fetchById(menuItemId)]));
    }).then(([cart, res]) => {
      let menuItem = res;
      if(cart['cart_items'].length >= 1) {
        let cartItem=cart['cart_items'];
        let chefId=cartItem[0]['chefId'];
        if(chefId != menuItem['chef_id']){
          throw new Error("Menu chef mismatched");
        }       
      }
      if(menuItem['has_variety']){
        if(varietyLinkId == '' || !varietyLinkId ){
          throw new Error("Menu item varietyLinkId not defined");
        }
        let MenuVariety=menuItem['varities'];
        for(let i=0; i<MenuVariety.length;i++){
          if(varietyLinkId==MenuVariety[i]["link_id"]){
            let price=MenuVariety[i]["unit_price"];
            menuItem["menu_item_price"] = price;
            menuItem["final_price"] = price+parseFloat(menuItem["yumma_price"]);
            break;
          }
        }
      }
      console.log(menuItem["menu_item_price"])      
      return Promise.all([cart, menuItem, qty, customerService.fetchCustomerByUserId(userId), menuItem['chef_id']]);
    }).then(([cart, menuItem, qty, customer, chefId]) => {
      return Promise.all([menuItem, qty, customer, chefService.fetchChefById(chefId), cart]);
    }).then(([menuItem, qty, customer, chef, cart]) => {
      return Promise.all([menuItem, customer, cart, calculateAndUpdateCartItem(menuItem, qty, chef, cart['cartId'],varietyLinkId),saveCartCustomer(cart['cartId'],customer)])
    }).then(([menuItem, customer, cart, cartItem]) => {
        return Promise.all([customer, cartDAO.findCartById(cartItem['cartId']),cartItem]);
    }).then(([customer, cart, cart_item]) => {
        let l_cart={};
        l_cart["count"] = cart['cart_items'].length;
        l_cart["item_count"] = cart["item_count"] + cart_item['qty'];
        l_cart["subtotal"] = parseFloat(cart["subtotal"]) + parseFloat(cart_item["rowTotal"]);        
        l_cart["chef_total_price"] = parseFloat(cart["chef_total_price"]) + parseFloat(cart_item["chef_total_price"]);
        l_cart["base_total"] =  parseFloat(cart["base_total"]) +  parseFloat(cart_item["rowTotal"]);
        l_cart["grand_total"] = l_cart["base_total"];
        l_cart["customer_id"] = customer["cust_id"];
        l_cart["deliveryDate"] = payload['delivery_date'];
        l_cart["delivery_time_slot"] = payload['delivery_time_slot'];
        return cartDAO.updateCart(cart['cartId'], l_cart);
      }).then(cart_res => {
        let result = parsecartItemData(cart_res);
        resolve(result);
      }).catch((err) => {
        reject(err);
      });
  });
};

function calculateAndUpdateCartItem(menuItem, qty, chef, cartId,varietyLinkId) {
  return new Promise((resolve, reject) => {
    let whereClause;
    if(varietyLinkId){
      whereClause={cartId: cartId, menuItemId: menuItem["menu_item_id"],variety_link_id:varietyLinkId}
    }else{
      whereClause={cartId: cartId, menuItemId: menuItem["menu_item_id"]}
    }
    cartDAO.findCartItemByCartIdAndMenuId(whereClause).then(item => {
      if(item) {
        return recalculateCartItem(cartId, item.cartItemId, qty);
      } else {
        let l_item = {};
        l_item["qty"] = qty;
        l_item["menuItemId"] = menuItem["menu_item_id"];
        if (menuItem['yumma_price']) {
          let yummaPrice=menuItem["yumma_price"];
          let menuItemPrice=menuItem["menu_item_price"];
          l_item["price"] = menuItemPrice+parseFloat(yummaPrice);
        }else{
          l_item["price"] = menuItem["menu_item_price"];
        }
        if (varietyLinkId) {
          l_item["variety_link_id"] = varietyLinkId;
        }
        l_item["chef_price"] = menuItem["menu_item_price"];
        l_item["chef_total_price"] = qty * l_item["chef_price"];
        l_item["rowTotal"] = qty * l_item["price"];        
        l_item["itemName"] = menuItem["menu_item_name"];
        l_item["chefFullName"] = chef["chef_fullname"];        
        l_item["chefkitchenImage"] = chef["kitchen_image"];
        l_item["minDish"] = menuItem["min_dish"];
        l_item["maxDish"] = menuItem["max_dish"];        
        l_item["kitchenTypeId"] = menuItem["kitchen_type_id"];
        l_item["kitchenType"] = menuItem["kitchen_type"];
        l_item["kitchenTypeCode"] = menuItem["kitchen_type_code"];
        l_item["menuItemImg"] = menuItem["menu_item_img"];
        l_item["foodTypeId"] = menuItem["food_type_id"];
        l_item["foodType"] = menuItem["foodType"];
        l_item["foodTypeCode"] = menuItem["food_type_code"];
        l_item["menuTypeId"] = menuItem["menu_type_id"];
        l_item["menuType"] = menuItem["menu_type"];
        l_item["menuTypeCode"] = menuItem["menu_type_code"];
        l_item["spiciness"] = menuItem["spiciness"];
        l_item["root_category_name"] = menuItem["root_category_name"];        
        l_item["chefId"] = chef['chef_id'];
        l_item["cartId"] = cartId;
        return cartDAO.createCartItem(l_item);
      }
    }).then(data => {
      resolve(data);
    }).catch(err => {
        reject(err);
    })
  })
}

function recalculateCartItem(cartId, itemId, qty) {
  return new Promise((resolve, reject) => {
    cartDAO.findById(cartId).then(res => {
      return Promise.all([res, qty, cartDAO.findCartItemById(itemId)]);
    }).then(([cart, qty, cartItem]) => {
        // update cart
        cart["item_count"] = cart["item_count"] - cartItem['qty'];
        cart["subtotal"] = parseFloat(cart["subtotal"]) - parseFloat(cartItem["rowTotal"]);
        cart["base_total"] =  parseFloat(cart["base_total"]) -  parseFloat(cartItem["rowTotal"]);
        cart["grand_total"] = cart["base_total"];
        cart["chef_total_price"] = parseFloat(cart["chef_total_price"]) - parseFloat(cartItem["chef_total_price"]);
        //update cart item
        cartItem["qty"] = qty;
        cartItem["rowTotal"] = qty * cartItem["price"];
        cartItem["chef_total_price"] = qty * cartItem["chef_price"];
        return Promise.all([cartDAO.updateCart(cart['cartId'], cart.dataValues), cartDAO.updateCartItem(cartItem['cartItemId'], cartItem.dataValues)]);
    }).then(([cart, cartItem]) => {
      resolve(cartItem);
    }).catch(err => {
      reject(err);
    })
  })
}

function saveCartCustomer(cartId,customer) {
  return new Promise((resolve, reject) => {
    customer['cart_id']=cartId;
    cartDAO.saveCartCustomer(cartId,customer)
    .then(data => {
      resolve(data);
    }).catch(err => {
        reject(err);
    })
  })
}

function parsecartItemData(data){
  if(data.cart_items){
    let cart_item = data.cart_items
    for(i=0;cart_item.length>i;i++){
      let row=cart_item[i];
      row.dataValues['menuItemImg_URL']=imageURL.menuItemImage_URL()+row['menuItemImg'];
      row.dataValues['chefkitchenImage_URL']=imageURL.setChefDocumentPath()+row['chefkitchenImage'];
    }
  }
  if(data.cart_customer){
    let row=data.cart_customer;
    row.dataValues['cust_profile_pic_URL']=imageURL.customerImage_URL()+row['cust_profile_pic'];
    row.dataValues['cust_cover_pic_URL']=imageURL.customerImage_URL()+row['cust_cover_pic'];
  }
  return data;
}

exports.deleteCartItem = (cartItemId,cartId) => {
  return new Promise((resolve, reject) => {
    cartDAO.deleteCartItem(cartItemId,cartId).then((res)=>{
      return recalculateCartItemBycartId(cartId)
    })
    .then((response) => {
        resolve(response);
      }).catch((err) => {
        reject(err);
      });
  });
};

exports.removeCart = (cartId) => {
  return new Promise((resolve, reject) => {
    cartDAO.deleteCart(cartId).then((response) => {
        resolve(response);
      }).catch((err) => {
        reject(err);
      });
  });
};

function recalculateCartItemBycartId(cartId) {
  return new Promise((resolve, reject) => {
    cartDAO.findCustomerActiveCart(cartId).then(cart => {
    let cartItem=cart['cart_items'];
    let updateCart={};
    updateCart["item_count"]=0;
    updateCart["subtotal"]=0;
    updateCart["base_total"]=0;
    updateCart["grand_total"]=0;
    updateCart["subtotal_addon"]=0;
    updateCart["chef_total_price"]=0;
    if(cartItem.length == 0){
      updateCart["deliveryDate"]=null;
      updateCart["delivery_time_slot"]=null;
    }
    for(i=0;cartItem.length > i; i++){
      updateCart["item_count"] =updateCart["item_count"]+cartItem[i].qty;
      updateCart["subtotal"] =   parseFloat(updateCart["subtotal"]) + parseFloat(cartItem[i].rowTotal)+parseFloat(cartItem[i].addonSubTotal);
      updateCart["base_total"] =  parseFloat(updateCart["base_total"]) +  parseFloat(cartItem[i].rowTotal);
      updateCart["grand_total"] =  updateCart["base_total"];
      updateCart["subtotal_addon"] =  parseFloat(updateCart["subtotal_addon"]) + parseFloat(cartItem[i].addonSubTotal);
      updateCart["chef_total_price"] =  parseFloat(updateCart["chef_total_price"]) + parseFloat(cartItem[i].chef_total_price);
    }
    return cartDAO.updateCart(cart['cartId'],updateCart) 
  }).then((response) => {
      resolve(response);
    }).catch((err) => {
      reject(err);
    });
});
}

exports.applyCartCoupon = (cart_id,couponCode) => {
  return new Promise((resolve, reject) => {
    couponDAO.fetchCouponCode(couponCode).then((response) => {
      let result=couponCalculatCartId(cart_id,response)
      resolve(result);
    }).catch((err) => {
      reject(err);
    });
  });
};


function couponCalculatCartId(cartId,coupon) {
  return new Promise((resolve, reject) => {
    cartDAO.findCustomerActiveCart(cartId).then(cart => {
      let discountPercentage=coupon['discount'];
      let total=cart['subtotal'];
      let discountAmount=((discountPercentage/ 100) * total).toFixed(2);
      let min_amount=coupon['minimum_amount'];
      let discount;
      if(discountAmount>=min_amount){
        console.log("min_amount",min_amount,"discountAmount",discountAmount)
        discount=min_amount;
      }else{
        console.log("discountAmount",discountAmount,"discountAmount",discountAmount)
        discount=discountAmount;
      }
      let updateCart={};      
      updateCart["discount"]=discount;
      updateCart["couponId"]=coupon["coupon_id"];
      updateCart["base_total"]=parseFloat(cart["subtotal"])-discount;
      updateCart["grand_total"] =  updateCart["base_total"];
    return cartDAO.updateCart(cart['cartId'],updateCart) 
  }).then((response) => {
      resolve(response);
    }).catch((err) => {
      reject(err);
    });
});
}


exports.createNewCart = (cartType, customer_id) => {
  return new Promise((resolve, reject) => {
    cartDAO.createEmptyCart(cartType, customer_id).then((response) => {
        resolve(response);
      }).catch((err) => {
        reject(err);
      });
  });
};


exports.addDeliveryChargeToCart = (cartId,customer_address_id) => {
  return new Promise((resolve, reject) => {
    cartDAO.findCartById(cartId).then(cart=>{
      let cartItem=cart['cart_items'];
      let chefId=cartItem[0]['chefId'];
      return Promise.all([
        customerAddressService.fetchCustomerAddress(customer_address_id),
        chefDAO.findChefById(chefId)
        ])
    }).then(([customerAddress,chefAddress]) => {
      let destinations=customerAddress['latitude']+','+customerAddress['longitude'];
      let origin=chefAddress['chef_lat']+','+chefAddress['chef_lang'];
      //let origin='12.017830'+','+'77.206393';
      return distanceCalculation.distance(destinations,origin);
    }).then((distanceKm) => {
      return applyDeliveryCharge(distanceKm)
    }).then((data) => {
      return Promise.all([data,taxDAO.fetchTax('gst')]);
    }).then(([deliveryChargeObj,Tax])=>{
      let result={"deliverCharge":deliveryChargeObj,"Tax":Tax};
      resolve(result);
    }).catch((err) => {
      reject(err);
    });
  });
};

function applyDeliveryCharge(distanceKm) {
  return new Promise((resolve, reject) => {
    deliveryChargeDAO.fetchDeliveryCharge().then(data => {
        let totalKm = parseFloat(distanceKm)        
        let minKms=data['minimum_kms'];
        let perKmPrice=data['delivery_charge_per_km'];
        let MinAmound=data['min_delivery_charge'];
        let result;
        if(totalKm >= minKms){
          result=((totalKm-minKms)*perKmPrice);
        }
        else if(totalKm<minKms){
          result=MinAmound;
        }
      let deliveryChargeObj={};        
      deliveryChargeObj["distance_km"]=distanceKm;    
      deliveryChargeObj["delivery_charge"]=MinAmound;
      deliveryChargeObj["distance_charge"]=result;
      resolve(deliveryChargeObj);
    }).catch((err) => {
      reject(err);
    });
});
}

exports.addonCalculateTotal = (cartId,payload) => {
  return new Promise((resolve, reject) => {
    cartDAO.deleteCartItemAddons(payload['cart_item_id'],cartId).then(data => {
      if(payload['addonId'].length==0){
        return data
      }else{
        return addonCalculatCartId(cartId,payload);
      }
    }).then((res) => {
      return addonCalculatCartItemId(payload['cart_item_id'])
    }).then((data) => {
      return recalculateCartItemBycartId(cartId)
    }).then(response => {
      resolve(response);
    }).catch((err) => {
      reject(err);
    });
  });
};

function addonCalculatCartId(cartId,payload) {
  return new Promise((resolve, reject) => {
    cartDAO.findCustomerActiveCart(cartId).then(cart => {
      return addonDAO.findAllAddonByAddonId(payload['addonId'])
    }).then((addons) => {
      let cartAddons=[];
      for(let i=0;i<addons.length;i++){
        let addonId=addons[i]['addon_id'];
        let addonPaylod=payload["addon_ids"]
        for(let i=0;i<addonPaylod.length;i++){
          if(addonId==addonPaylod[i]['addon_id']){
            let cartAddonPayload={};
            cartAddonPayload['addon_id']=addons[i]['addon_id'];
            cartAddonPayload['addon_name']=addons[i]['addon_name'];
            cartAddonPayload['addon_image']=addons[i]['addon_image'];
            cartAddonPayload['price']=addons[i]['price'];
            cartAddonPayload['qty']=addonPaylod[i]['qty'];
            cartAddonPayload['cartId']=cartId;
            cartAddonPayload['rowTotal']=addons[i]['price']*addonPaylod[i]['qty'];
            cartAddonPayload['cartItemId']=payload['cart_item_id'];
            cartAddons.push(cartAddonPayload);
          }
        }
      }
      return cartDAO.createCartAddons(cartAddons)    
    }).then((response) => {
      resolve(response);
    }).catch((err) => {
      reject(err);
    });
});
}

function addonCalculatCartItemId(cartItem_id) {
  return new Promise((resolve, reject) => {
    cartDAO.findAllCartAddons(cartItem_id).then(cartAddons => {
      let cartItemId;
      if(cartAddons.length==0){
        cartItemId=cartItem_id;
      }
      let addonTotal=0;      
      for (let i = 0; i < cartAddons.length; i++) {
        let addon=cartAddons[i]['dataValues'];
        cartItemId=addon['cartItemId'];
        addonTotal += parseFloat(addon['rowTotal']);
      }
      let updateCartItem={};
      updateCartItem["addonSubTotal"]=addonTotal;
      return cartDAO.updateCartItems(cartItemId,updateCartItem)
    }).then((response) => {
      resolve(response);
    }).catch((err) => {
      reject(err);
    });
  });
}


exports.addonDeleteCalculateTotal = (cartId,payload) => {
  return new Promise((resolve, reject) => {
    cartDAO.deleteCartItemAddonByCartAddonId(payload['cart_addon_id'],cartId).then(data => {      
      return addonCalculatCartItemId(payload['cart_item_id'])
    }).then((data) => {
      return recalculateCartItemBycartId(cartId)    
    }).then(res => {
      resolve(res);
    }).catch((err) => {
      reject(err);
    });
  });
};

// function addonDeleteCalculatCartId(cartId,addons) {
//   return new Promise((resolve, reject) => {
//     cartDAO.findCustomerActiveCart(cartId).then(cart => {
//       let addonTotal=0;
//       for (let i = 0; i < addons.length; i++) {
//         let addon=addons[i]['dataValues'];
//         addonTotal += parseFloat(addon['price']);
//       }      
//       let updateCart={};
//       updateCart["subtotal_addon"]=parseFloat(cart["subtotal_addon"])-parseFloat(addonTotal);
//       updateCart["subtotal"]=parseFloat(cart["subtotal"])-parseFloat(addonTotal);
//     return cartDAO.updateCart(cart['cartId'],updateCart)
//   }).then((response) => {
//       resolve(response);
//     }).catch((err) => {
//       reject(err);
//     });
// });
// }

exports.fetchCartDeliveryTimeSlot = (cartId) => {
  return new Promise((resolve, reject) => {
    cartDAO.findCartDeliveryTimeSlot(cartId).then((response) => {
      resolve(response);
    }).catch((err) => {
      reject(err);
    });
  });
};

exports.fetchCartAvailability = (cartId) => {
  return new Promise((resolve, reject) => {
    cartDAO.findCartAvailability(cartId).then((response) => {
        resolve(response);
      }).catch((err) => {
        reject(err);
      });
  });
};