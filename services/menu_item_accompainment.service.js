const model = require('../models');
const accompainmentModel = model.menu_items_accompainment;

exports.saveAccompainmentMenu_Item = (accompainmentData) => {    
  return new Promise((resolve, reject) => {        
    accompainmentModel.bulkCreate(accompainmentData).then(response => {                  
          console.log(response);
        resolve(response);
      })
      .catch(err => {                    
        reject({error:err});
      });
  });
};
exports.getAllAccompainment = () => {
  return new Promise((resolve, reject) => {
    accompainmentModel.findAll().then((data) => {
        console.log(data);
        resolve((data));
      },
      (err) => {
        reject({ error: err });
      }
    );
  });
};
let accompainmentData = (data) => {
  let res = [];
  data.map((row) => {
    res.push(row.accombaniment_menu_item_id);
  });
  return res;
};

exports.getAllAccompainmentByMenuItemId = (menuItemId) => {
  return new Promise((resolve, reject) => {
    accompainmentModel.findAll({ where: { menu_item_id: menuItemId } }).then((data) => {
      let response =accompainmentData((data))
      console.log(response)
        resolve(response);
      },
      (err) => {
        reject({ error: err });
      }
    );
  });
};