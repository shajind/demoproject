const model = require('../models');
const countryModel = model.country;


exports.saveCountry = (countrydata) => {    
    return new Promise((resolve, reject) => {
        countryModel.create(countrydata).then(response => {                  
            console.log(response);
        	resolve(response);
        },err => {                    
        	reject({error:err});            
        });
    })
}

exports.getAllCountries = () =>{    
    return new Promise((resolve,reject) => {               
        countryModel.findAll().then(data =>{
            console.log(data);            
            resolve(data);            
        },err => {                    
        	reject({error:err});
        });
    });
};