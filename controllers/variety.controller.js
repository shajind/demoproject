const varietyService = require("../services/variety.service");


exports.saveVariety = (req, res) => {
    let payload = req.body;
    let chefId=req.params.chef_id;
    payload['chef_id']=chefId;
    let resObj;
    varietyService.createVariety(payload).then((data) => {
        resObj = {
          status: true,
          response: {
                data: data,
                message: "Successfully save Variety",
            },
        };
        res.send(resObj);
    }).catch(err => {
        resObj = {
            status: false,
            response: {
                message: err,
            },
        };
        res.status(400).send(resObj);
    })
};

exports.findAllVariety = (req, res) => {
    let resObj;
    varietyService.findAllVariety().then((data) => {
        resObj = {
            status: true,
            response: {
                data: data,
                message: "Successfully view Variety",
            },
        };
        res.send(resObj);
    }).catch((err) => {
        resObj = {
            status: false,
            response: {
                message: err,
            },
        };
        res.status(400).send(resObj);
    });
};

exports.findVarietyByChef = (req, res) => {
    let resObj;
    let chefId=req.params.chef_id;
    varietyService.fetchVarietyByChef(chefId).then((data) => {
        resObj = {
            status: true,
            response: {
                data: data,
                message: "Successfully view Variety",
            },
        };
        res.send(resObj);
    }).catch((err) => {
        resObj = {
            status: false,
            response: {
                message: err,
            },
        };
        res.status(400).send(resObj);
    });
};