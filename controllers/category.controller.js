const categoryService = require("../services/category.service");

exports.saveCategory = (req, res) => {
  let categoryData = req.body;
  if (req && req.file && req.file.filename) {
    categoryData["category_image"] = req.file.filename;
  }
  console.log("categoryData :", categoryData);
  categoryService.saveCategory(categoryData)
    .then((result) => {
      let resObj = {
        status: true,
        response: {
          data: result,
          message: "Category added successfully",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      let resObj = {
        status: false,
        response: {
          error: err,
          message: "Failed to add Category",
        },
      };
      res.status(400).send(resObj);
    });
};

exports.getAllCategory = (req, res) => {
  let parentId = req.query.parent_id;  
  let filters = req.query.filters;
  categoryService.fetchAllCategories(filters,parentId)
  .then((result) => {
    let resObj = {
      status: true,
      response: {
        data: result,
        message: "Fetch all Category types successfully",
      },
    };
    res.send(resObj);
  })
  .catch((err) => {
    let resObj = {
      status: false,
      response: {
        error: err,
        message: "Failed to fetch all Category types",
      },
    };
    res.status(400).send(resObj);
  });
};

exports.getAllCategoryByParentId = (req, res) => {
  let parentId = req.query.parent_id;
  if(!parentId){
    parentId=0;
  }
  categoryService.fetchAllCategoriesByParentId(parentId)
  .then((result) => {
    let resObj = {
      status: true,
      response: {
        data: result,
        message: "Fetch all Category types successfully",
      },
    };
    res.send(resObj);
  })
  .catch((err) => {
    let resObj = {
      status: false,
      response: {
        error: err,
        message: "Failed to fetch all Category types",
      },
    };
    res.status(400).send(resObj);
  });
};
