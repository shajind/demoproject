const notificationService = require('../services/notification.service');

exports.saveNotification = (req, res) => {
    let payload = req.body;
    let resObj;
    notificationService.saveNotification(payload)
    .then(response => {
        console.log("data:",response);
        resObj = {
            status:true,
            "response":{
                "data":response,
                "message":"Successfully save Notification"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}


exports.getAllNotifications = (req,res) =>{
    let resObj;
    let filters = req.query.filters;
    notificationService.fetchAllNotifications(filters)
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view Notification"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

exports.getNotificationById = (req,res) =>{
    let id = req.params.notification_id;    
    let resObj;
    notificationService.fetchNotificationById(id)
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view Notification"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}