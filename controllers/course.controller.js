const courseService = require("../services/course.service");

exports.saveCourse = (req, res) => {
    let course = req.body;
    courseService.createCourse(course)
    .then((data) => {
    resObj = {
        status: true,
        response: {
            data: data,
            message: "Successfully save course",
        },
    };
    res.send(resObj);
    }).catch(err => {
    resObj = {
        status: false,
        response: {
            message: err,
        },
    };
    res.status(400).send(resObj);
    })
};

exports.findAllCourse = (req, res) => {
    courseService.findAllCourse().then((data) => {
      resObj = {
        status: true,
        response: {
          data: data,
          message: "Successfully view Course",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err,
        },
      };
      res.status(400).send(resObj);
    });
};

