const deliveryChargeService = require("../services/delivery_charge.service");

exports.saveDeliveryCharge = (req, res) => {
  let deliveryCharge = req.body;
  deliveryChargeService.createDeliveryCharge(deliveryCharge)
    .then((data) => {
      resObj = {
        status: true,
        response: {
          data: data,
          message: "Successfully save Delivery Charge",
        },
      };
      res.send(resObj);
    }).catch(err => {
      resObj = {
          status: false,
          response: {
            message: err,
          },
        };
        res.status(400).send(resObj);
    })
};

exports.updateDeliveryCharge = (req, res) => {
    let deliveryChargeID = req.params.delivery_charge_id;
    let deliveryCharge = req.body;
    deliveryChargeService.updateDeliveryCharge(deliveryChargeID,deliveryCharge)
      .then((data) => {
        resObj = {
          status: true,
          response: {
            data: data,
            message: "Successfully update Delivery Charge",
          },
        };
        res.send(resObj);
      }).catch(err => {
        resObj = {
            status: false,
            response: {
              message: err,
            },
          };
          res.status(400).send(resObj);
      })
};

exports.findAllDeliveryCharg = (req, res) => {
    deliveryChargeService.findAllDeliveryCharge()
    .then((data) => {
      resObj = {
        status: true,
        response: {
          data: data,
          message: "Successfully view Delivery Charge",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err,
        },
      };
      res.status(400).send(resObj);
    });
};