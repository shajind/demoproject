const orderStatusService = require('../services/order_status.service');

exports.createOrderStatus = (req, res) => {
    let payload = req.body;
    let resObj;
    orderStatusService.saveOrderStatus(payload).then(response => {
        resObj = {
            status:true,
            response:{
                "data":response
            },
            message: 'Successfully save Order Status'
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "message": err
        };
        res.status(400).send(resObj);
    })
}

exports.getAllOrderStatus = (req,res) =>{
    let resObj;
    orderStatusService.fetchAllOrderStatus()
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view Order Status"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

