const cityService = require('../services/city.service');


exports.saveCity = (req, res) => {
    let citydata = req.body;
    console.log("citydata :",citydata);
    let resObj;
    cityService.saveCity(citydata)
    .then(response => {
        console.log("data:",response);
        resObj = {
            status:true,
            "response":{
                "data":response,
                "message":"Successfully save city"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}


exports.getAllCityBystateId = (req,res) =>{
    let stateId=req.query.stateId;
    let resObj;
    cityService.getCityBystateId(stateId)
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view city"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

