const countryService = require('../services/country.service');



exports.saveCountry = (req, res) => {
    let userID = req.query.user_id;
    let countrydata = req.body;
    countrydata['user_id']=userID;
    console.log("countrydata :",countrydata);
    var resObj;
    countryService.saveCountry(countrydata)
    .then(response => {
        console.log("data:",response);
        resObj = {
            status:true,
            "response":{
                "data":response,
                "message":"Successfully save Country"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

exports.getAllCountries = (req,res) =>{
    var resObj;
    countryService.getAllCountries()
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view Country"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}
