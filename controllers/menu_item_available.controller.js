const menuService = require('../services/menu_item_available.service');

exports.saveMenu = (req, res) => {
    let resObj;
    let menuData = req.body;
    if(!menuData.item_id || menuData.item_id == '') {
    	resObj = {
			status: false,
			"response": {
				"message": "Please enter Menu Item"
			}
		};
		res.status(400).send(resObj);
		return;
    }    
    menuService.saveMenu(menuData)
    .then(response => {
        console.log("data:",response);
        resObj = {
            status:true,
            "response":{
                "data":response,
                "message":"Successfully save Menu"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}


exports.getAllMenuItemAvailable = (req,res) =>{
    let resObj;
    menuService.getAllMenuItemAvailable()
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view Menu Item and Available"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

exports.getMenuItemAvailableByMenuItemId = (req,res) =>{
    let MenuItemId=req.query.menu_item_id;
    console.log("MenuItemId", MenuItemId);
    let resObj;
    menuService.getMenuItemAvailableByMenuItemId(MenuItemId)    
    .then(data => {                     
        resObj = {                  
            status:true,
            "response":{
                "data":data,               
                "message":"Successfully view Menu"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

exports.getMenuItemAccompainmentsBymenuId = (req,res) =>{
    let itemId=req.params.id;
    console.log("item id", itemId);
    let resObj;
    menuService.getMenuItem(itemId)    
    .then(data => {                     
        resObj = {                  
            status:true,
            "response":{
                "data":data,               
                "message":"Successfully view Menu"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}