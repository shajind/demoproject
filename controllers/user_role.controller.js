const user_RoleService = require('../services/user_role.service');


exports.saveRole = (req, res) => {
    let user_role_data = req.body;
    console.log("user_role_data :",user_role_data);
    let resObj;
    user_RoleService.saveUser_Type(user_role_data)
    .then(response => {
        console.log("data:",response);
        resObj = {
            status:true,
            response:{
                "data":response
            },
            message: 'Successfully save user_type'
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "message": err
        };
        res.status(400).send(resObj);
    })
}


exports.getAllUser_Type = (req,res) =>{
    let resObj;
    user_RoleService.getUser_Type()
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view user_type"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

