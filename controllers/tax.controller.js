const taxService = require("../services/tax.service");


exports.saveTax = (req, res) => {
  let payload = req.body;
  console.log(payload);
  taxService.createTax(payload)
    .then((data) => {
      resObj = {
        status: true,
        response: {
          data: data,
          message: "Successfully save Tax",
        },
      };
      res.send(resObj);
    }).catch(err => {
      resObj = {
          status: false,
          response: {
            message: err,
          },
        };
        res.status(400).send(resObj);
    })
};


exports.findAllTax = (req, res) => {
    taxService.findAllTax().then((data) => {
      resObj = {
        status: true,
        response: {
          data: data,
          message: "Successfully view Tax",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err,
        },
      };
      res.status(400).send(resObj);
    });
};