var passwords = require("../utils/otp-generater");
const chefService = require("../services/chef.service");
const userService = require("../services/users.service");
const roles = require("../config/role.json");

exports.chefSendLoginOTP = (req, res) => {
  var resObj;
  let mobile = req.body.mobile;
  chefService.saveChefLoginOTP(mobile)
    .then((response) => {
      console.log("data:", response);
      resObj = {
        status: true,
        response: { data: response },
        message: "Successfully OTP Send",
      };
      res.send(resObj);
    }).catch((err) => {
      resObj = {
        status: false,
        message: err.message
      };
      res.status(400).send(resObj);
    });
};

exports.getChefProfile = (req, res) => {
  var resObj;
  //var id = req.query.user_id;
  var id = req.session.userId;
  //var user_type_id=req.session.user_type_id;
  console.log(id);
  chefService
    .getChefProfile(id)
    .then((response) => {
      resObj = {
        status: true,
        response: {
          data: response,
          message: "Chef Profile details  Received",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err,
        },
      };
      res.status(400).send(resObj);
    });
};

let userData = (data, otp) => {
  let userData = {};
  let chefProfileData = {};
  userData["user_name"] = data.chef_fullname;
  userData["user_otp"] = "00000"; //otp;
  userData["user_mobile"] = data.user_mobile;
  userData["user_email"] = data.user_email;
  userData["country_code"] = data.country_code;
  chefProfileData["chef_fullname"] = data.chef_fullname;
  chefProfileData["chef_gender"] = data.chef_gender;
  chefProfileData["chef_dob"] = data.chef_dob;
  chefProfileData["chef_address1"] = data.chef_address1;
  chefProfileData["chef_address2"] = data.chef_address2;
  chefProfileData["city_id"] = data.city_id;
  chefProfileData["state_id"] = data.state_id;
  chefProfileData["chef_lat"] = data.chef_lat;
  chefProfileData["chef_lang"] = data.chef_lang;
  chefProfileData["chef_zipcode"] = data.chef_zipcode;
  chefProfileData["country_code"] = data.country_code;
  chefProfileData["chef_cuisine"] = data.chef_cuisine;
  return { userData, chefProfileData };
};

exports.chefSignUp = (req, res) => {
  let otp = passwords.OTPpassword();
  let profilePayload = req.body;
  let payload = userData(profilePayload, otp);
  console.log("payload", payload);
  let resObj;
  chefService.createChef(profilePayload, roles.ROLE_CHEF)
    .then((response) => {
      console.log("data:", response);
      resObj = {
        status: true,
        response: {
          data: response,
          message: "Chef Signup Sucessfully ",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      console.log("hellooooooooooooooooo");
      console.log(err.message);
      resObj = {
        status: false,
        response: {},
        message: err.message,
      };
      res.status(400).send(resObj);
    });
};

exports.getAllChefs = (req, res) => {
  let filters = req.query.filters;
  let sortBy = req.query.sortby;
  let customerId = req.query.customer_id
  var resObj;
  chefService
    .getAllChefs(filters,sortBy,customerId)
    .then((data) => {
      resObj = {
        status: true,
        response: {
          data: data,
          message: "Chefs List Received Sucessfully",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err,
        },
      };
      res.status(400).send(resObj);
    });
};

exports.chefStatusInactive = (req, res) => {
  let chefId=req.query.chef_id;
  let payload={};
  payload['status']="inactive";  
  var resObj;
  chefService.chefStatusUpdate(payload,chefId).then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Sucessfully deactive",
      },
    };
    res.send(resObj);
  })
  .catch((err) => {
    resObj = {
      status: false,
      response: {
        message: err,
      },
    };
    res.status(400).send(resObj);
  });
};

exports.chefStatusActive = (req, res) => {
  let chefId=req.query.chef_id;
  let payload={};
  payload['status']="active";
  var resObj;
  chefService.chefStatusUpdate(payload,chefId).then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Sucessfully active",
      },
    };
    res.send(resObj);
  })
  .catch((err) => {
    resObj = {
      status: false,
      response: {
        message: err,
      },
    };
    res.status(400).send(resObj);
  });
};

exports.updateProfile = (req, res) => {
  let userId = req.session.userId;
  let chefId = req.session.chefId;
  let payload = req.body;
  let resObj;
  if(Object.keys(req.files).length > 0) {
    payload['chef_profile_pic'] = req.files.profile_image[0].filename;
    payload['chef_profile_cover_pic'] = req.files.profile_cover_image[0].filename;
  }
  chefService.updateChefProfile1(userId, chefId, payload).then(response => {
      resObj = {
        status: true,
        response: {
          data: response,
          message: "Chef update Sucessfully ",
        },
      };
      res.status(200).send(resObj);
  }).catch(err => {
    console.log("error ChefController -- updateProfile()", err);
    resObj = {
      status: false,
      response: {
        message: err,
      },
    };
    res.status(400).send(resObj);
  });
};

exports.uploadDocument = (req, res) => {
  let chefId = req.session.chefId;
  let payload = req.body;
  payload['file_name'] = req.files.file[0].filename;
  let resObj;
  console.log("uploadDocument()", payload);
  chefService.updateChefDocument(chefId, payload).then((response) => {
      console.log("data:", response);
      resObj = {
        status: true,
        response: {
          data: response,
          message: "Document Sucessfully updated",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      console.log("error ChefController -- updateProfile()", err);
      resObj = {
        status: false,
        response: {
          message: err,
        },
      };
      res.status(400).send(resObj);
    });
};

exports.login = (req, res) => {    
  var resObj;
  let userName = req.body.user_name;
  let passWord = req.body.password;   
  chefService.chefLogin(userName, passWord).then(data => {
      if(data['status']=='inactive'&& data['user_status']=='inactive'){
          res.send({"err":'admin is block your account'});
      }
      resObj = {
          status:true,
          response:{ data: data},
          message: "Successfully login"
      };
      res.send(resObj);
  })
  .catch(err=>{
      resObj={
          "status":false,
          "message":err.message, 
          "response":{}
      };
      res.status(400).send(resObj);
  })
}