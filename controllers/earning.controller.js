const earningService = require('../services/earning.service');

exports.getTotalEarnings = (req, res) => {
  let chefId = req.params.chef_id;
  let resObj;
  earningService.findTotalEarnings(chefId).then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully fetched earnings",
      },
    };
    res.status(200).send(resObj);
  })
  .catch((err) => {
    resObj = {
      status: false,
      response: {},
      message: err.message,
    };
    res.status(400).send(resObj);
  });
};

exports.getEarningDetails = (req, res) => {
  let chefId = req.params.chef_id;
  let filter=req.query.filter;
  let resObj;
  earningService.findEarningDetails(chefId,filter).then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully earning details",
      },
    };
    res.status(200).send(resObj);
  })
  .catch((err) => {
    resObj = {
      status: false,
      response: {},
      message: err.message,
    };
    res.status(400).send(resObj);
  });
};

exports.createWeekDate = (req, res) => {
  let weekYear=req.body.year;
  let resObj;
  earningService.saveWeekDate(weekYear).then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully create week",
      },
    };
    res.status(200).send(resObj);
  })
  .catch((err) => {
    resObj = {
      status: false,
      response: {},
      message: err.message,
    };
    res.status(400).send(resObj);
  });
};