const kitchenTypeService = require("../services/kitchen_type.service");

exports.findAllKitchenTypes = (req, res) => {
    kitchenTypeService.findAllKitchenTypes()
    .then((data) => {
      resObj = {
        status: true,
        response: {
          data: data,
          message: "Successfully view kitchenTypes",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err,
        },
      };
      res.status(400).send(resObj);
    });
};

exports.saveKitchenType = (req, res) => {
  let kitchenType = req.body;
  kitchenTypeService.createKitchenType(kitchenType)
    .then((data) => {
      resObj = {
        status: true,
        response: {
          data: data,
          message: "Successfully save kitchenType",
        },
      };
      res.send(resObj);
    }).catch(err => {
      resObj = {
          status: false,
          response: {
            message: err,
          },
        };
        res.status(400).send(resObj);
    })
};