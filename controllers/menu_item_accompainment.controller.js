const menuItemAccompainmentService = require("../services/menu_item_accompainment.service");
const menuItemAvailableService = require('../services/menu_item_available.service');


exports.saveAccompainment = (req, res) => {
  let accompainmentData = req.body;
  console.log(accompainmentData)
  let resObj;
  menuItemAccompainmentService.saveAccompainmentMenu_Item(accompainmentData)
    .then((response) => {
      console.log("data:", response);
      resObj = {
        status: true,
        response: {
          data: response,
          message: "Successfully save accompainment",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err,
        },
      };
      res.status(400).send(resObj);
    });
};

exports.getAllAccompainment = (req, res) => {
  let resObj;
  menuItemAccompainmentService.getAllAccompainment()
    .then((data) => {
      resObj = {
        status: true,
        response: {
          data: data,
          message: "Successfully view Accompainment",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err,
        },
      };
      res.status(400).send(resObj);
    });
};

exports.getAllAccompainmentByMenuItemId = (req, res) => {
  let menuItemId = req.params.menu_item_id;
  let resObj;
  menuItemAccompainmentService.getAllAccompainmentByMenuItemId(menuItemId)
  .then((data)=>{
    return menuItemAvailableService.getMenuItemAvailableAccompanimentByMenuItemId((data))
  })
    .then((data) => {
      resObj = {
        status: true,
        response: {
          data: data,
          message: "Successfully view Accompainment by MenuItemId",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err,
        },
      };
      res.status(400).send(resObj);
    });
};
