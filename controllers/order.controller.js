const orderService = require('../services/order.service');


exports.checkoutOrder = (req, res) => {
    let resObj;
    let payload = req.body;
    let cartId = payload['cart_id'];
    //let customer_id=req.session.customerId;
    orderService.doCheckout(cartId, payload).then(response => {
        console.log("data:",response);
        resObj = {
            status:true,
            "response":{
                "data":response,
                "message":"Successfully ordered"
            }
        };
        res.send(resObj);
    }).catch(err=>{
        console.log(err);
        resObj={
            "status":false,
            "response":{},
            "message":err.message
        };
        res.status(400).send(resObj);
    })
}


exports.getAllOrders = (req,res) => {
    let resObj;
    let filters = req.query.filters;
    orderService.fetchAllOrders(filters)
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully fetched all order"
            }
        };
        res.send(resObj);
    }).catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

exports.getAllOrderCustomer = (req,res) => {
    let resObj;
    let filters = req.query.filters;
    let fetchId={"customerId":req.session.customerId};
    orderService.fetchAllOrders(filters,fetchId)
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully fetched all order"
            }
        };
        res.send(resObj);
    }).catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

exports.getAllOrderChef = (req,res) => {
    let resObj;
    let envelop = req.query.envelop;
    let envelop_l = JSON.parse(envelop);
    console.log(envelop_l["filters"])
    let filters = envelop_l["filters"];
    let fetchId={"chefId":req.session.chefId};
    console.log(filters,fetchId)
    orderService.fetchAllOrders(filters,fetchId)
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully fetched all order"
            }
        };
        res.send(resObj);
    }).catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

exports.getOrder = (req,res) => {
    let resObj;
    let orderId = req.params.order_id;
    orderService.fetchOrderById(orderId).then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully fetched all order"
            }
        };
        res.send(resObj);
    }).catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

exports.acceptOrderHandler = (req,res) => {
    let resObj;
    let orderId = req.params.order_id;
    let chef_id = req.session.chefId;
    orderService.acceptOrderByChef(orderId, chef_id).then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message": "Successfully Order Accepted"
            }
        };
        res.send(resObj);
    }).catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err.message
            }
        };
        res.status(400).send(resObj);
    })
}

exports.rejectOrderHandler = (req,res) => {
    let resObj;
    let payload = req.body;
    let orderId = req.params.order_id;
    let chef_id = req.session.chefId;
    orderService.rejectOrderByChef(orderId, chef_id, payload).then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully Order rejected"
            }
        };
        res.send(resObj);
    }).catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err.message
            }
        };
        res.status(400).send(resObj);
    })
}

exports.completedOrderHandler = (req,res) => {
    let resObj;
    let orderId = req.params.order_id;
    let chef_id = req.session.chefId;
    orderService.completedOrderByChef(orderId, chef_id).then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully Order completed"
            }
        };
        res.send(resObj);
    }).catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err.message
            }
        };
        res.status(400).send(resObj);
    })
}



exports.outdeliveryOrderHandler = (req,res) => {
    let resObj;
    let orderId = req.params.order_id;
    let chef_id = req.session.chefId;
    orderService.outdeliveryOrderByChef(orderId, chef_id).then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully Order out_delivery"
            }
        };
        res.send(resObj);
    }).catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err.message
            }
        };
        res.status(400).send(resObj);
    })
}


exports.getAllOrderChefStatus = (req,res) => {
    let resObj;
    let chef_id=req.session.chefId;
    let filters = req.query.filters;
    orderService.fetchAllOrderChefStatus(chef_id,filters)
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully fetched all order chef"
            }
        };
        res.send(resObj);
    }).catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}
