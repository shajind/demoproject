const NotificationLogService = require('../services/notificationlog.service');

exports.saveNotificationLog = (req, res) => {
    let payload = req.body;
    let resObj;
    NotificationLogService.saveNotificationLog(payload)
    .then(response => {
        console.log("data:",response);
        resObj = {
            status:true,
            "response":{
                "data":response,
                "message":"Successfully save Notification"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}


exports.getAllNotificationLog = (req,res) =>{
    let resObj;
    NotificationLogService.getAllNotificationLog()
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view Notification"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

exports.getAllNotificationLogById = (req,res) =>{
    //let id = req.session.userId;
    let id = req.query.id;    
    let resObj;
    NotificationLogService.getAllNotificationLogByUserId(id)
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view Notification"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}