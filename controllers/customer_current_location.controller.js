const CustomerCurrentLocationService = require("../services/customer_current_location.service");



exports.saveCustomerCurrentLocation = (req, res) => {
    let customer_id = req.session.customerId;
    let payload = req.body;
    payload['customer_id']=customer_id
    CustomerCurrentLocationService.createCustomerCurrentLocation(payload,customer_id)
    .then((data) => {
        resObj = {
            status: true,
            response: {
                data: data,
                message: "Successfully save customer current location",
            },
        };
        res.send(resObj);
    }).catch(err => {
        resObj = {
            status: false,
            response: {
                message: err,
            },
        };
        res.status(400).send(resObj);
    })
};


exports.getCustomerCurrentLocationByCustomerId = (req, res) => {
    let customerId = req.params.customer_id;
    CustomerCurrentLocationService.fetchCustomerCurrentLocationByCustomerId(customerId)
    .then((data) => {
        resObj = {
            status: true,
            response: {
                data: data,
                message: "Successfully view customer current location",
            },
        };
        res.send(resObj);
    }).catch((err) => {
        resObj = {
            status: false,
            response: {
                message: err,
            },
        };
        res.status(400).send(resObj);
    });
};

