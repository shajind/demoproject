const DeliveryTimeSlotMstService = require('../services/delivery_time_slot_master.service');

let timeConvert= (time) =>{
    // Check correct time format and split into components
    time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
  
    if (time.length > 1) { // If time format correct
      time = time.slice (1);  // Remove full string match value
      time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join (''); // return adjusted time or original string
}
  

exports.saveDeliveryTimeSlotMst = (req, res) => {
    let deliveryTimeSlotData = req.body;
    let fromTime=timeConvert(deliveryTimeSlotData['slot_from_time']);    
    let toTime=timeConvert(deliveryTimeSlotData['slot_to_time']);
    deliveryTimeSlotData['slot_label']=fromTime +' to '+toTime;
    console.log("deliveryTimeSlotData :",fromTime,toTime,deliveryTimeSlotData);
    let resObj;
    DeliveryTimeSlotMstService.deliveryTimeSlotMstSave(deliveryTimeSlotData)
    .then(response => {
        console.log("data:",response);
        resObj = {
            status:true,
            "response":{
                "data":response,
                "message":"Successfully save delivery Time Slot"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}


exports.getAllDeliveryTimeSlotMst = (req,res) =>{
    let filters = req.query.filters;
    let resObj;
    DeliveryTimeSlotMstService.getAllDeliveryTimeSlot(filters)
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view Delivery Time Slot"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

