const cartService = require("../services/cart.service");

exports.addItemToCart = (req, res) => {
  let cartType = req.query.cart_type;
  let payload = req.body;
  let resObj;
  if(!cartType) {
    resObj = {
      status: false,
      response: {},
      message: "Query parameter cart_type missing",
    };
    res.status(400).send(resObj);
    return;
  }
  if(cartType != 'checkout' && cartType != 'order_now') {
    resObj = {
      status: false,
      response: {},
      message: "Query parameter cart_type must be either checkout or order_now value",
    };
    res.status(400).send(resObj);
    return;
  }
  if(!payload['cart_id']) {
    resObj = {
      status: false,
      response: {},
      message: "Cart Id missing",
    };
    res.status(400).send(resObj);
    return;
  }
  payload['user_id'] = req.session.userId;
  cartService.createCartAndAddItem(cartType, payload).then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully added to cart",
      },
    };
    res.send(resObj);
  })
  .catch((err) => {
    resObj = {
      status: false,
      response: {},
      message: err.message,
    };
    res.status(200).send(resObj);
  });
};

exports.getCartList = (req, res) => {
  let cartId = req.query.cart_id;
  let resObj;
  let customerId = req.session.customerId;
  cartService.fetchCartList(customerId,cartId).then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully added to cart",
      },
    };
    res.send(resObj);
  })
  .catch((err) => {
    resObj = {
      status: false,
      response: {},
      message: err.message,
    };
    res.status(400).send(resObj);
  });
};


exports.deleteCartItemById = (req, res) => {
  let resObj;
  let cartItemId=req.params.item_id;
  let cartId=req.params.cart_id;
  cartService.deleteCartItem(cartItemId,cartId).then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully delete Cart Item",
      },
    };
    res.send(resObj);
  })
  .catch((err) => {
    resObj = {
      status: false,
      response: {},
      message: err.message,
    };
    res.status(400).send(resObj);
  });
};

exports.deleteCartById = (req, res) => {
  let resObj;
  let cartId=req.params.cart_id;
  cartService.removeCart(cartId).then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully cart deleted",
      },
    };
    res.send(resObj);
  })
  .catch((err) => {
    resObj = {
      status: false,
      response: {},
      message: err.message,
    };
    res.status(400).send(resObj);
  });
};

exports.applyCoupon = (req, res) => {
  let resObj;
  let cart_id = req.body.cartId;
  let couponCode = req.body.coupon_code;
  cartService.applyCartCoupon(cart_id,couponCode).then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully Add Coupon",
      },
    };
    res.send(resObj);
  })
  .catch((err) => {
    resObj = {
      status: false,
      response: {},
      message: err.message,
    };
    res.status(400).send(resObj);
  });
};

exports.createNewCustomerCart = (req, res) => {
  let resObj;
  let customer_id = req.session.customerId;
  let cartType = req.query.cart_type;
  if(!cartType) {
    resObj = {
      status: false,
      response: {},
      message: "Query parameter cart_type missing",
    };
    res.status(400).send(resObj);
    return;
  }
  if(cartType != 'checkout' && cartType != 'order_now') {
    resObj = {
      status: false,
      response: {},
      message: "Query parameter cart_type must be either checkout or order_now value",
    };
    res.status(400).send(resObj);
    return;
  }
  cartService.createNewCart(cartType, customer_id).then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully Empty Cart",
      },
    };
    res.send(resObj);
  }).catch(err => {
    resObj = {
      status: false,
      response: {
        message: err,
      },
    };
    res.status(400).send(resObj);
  })
};

exports.applyDeliveryCharge = (req, res) => {
  let resObj;
  let cart_id = req.params.cart_id;
  let customer_address_id = req.body.customer_address_id;
  cartService.addDeliveryChargeToCart(cart_id,customer_address_id).then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully Add Delivery Charge",
      },
    };
    res.send(resObj);
  })
  .catch((err) => {
    resObj = {
      status: false,
      response: {},
      message: err.message,
    };
    res.status(400).send(resObj);
  });
};

exports.calculateAddon = (req, res) => {
  let resObj;
  let cart_id = req.params.cart_id;
  let payload = req.body;
  let addons = req.body.addon_ids;
  // if(addons.length == 0||addons=='') {
  //   resObj = {
  //     status: false,
  //     response: {},
  //     message: "please select your addon",
  //   };
  //   res.status(400).send(resObj);
  //   return;
  // }
  let addonId=[];
  for(let i=0;i < addons.length;i++){    
    addonId.push(addons[i]['addon_id']);
  }
  payload['addonId']=addonId;
  console.log(payload);
  cartService.addonCalculateTotal(cart_id,payload).then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully Add addon calculate",
      },
    };
    res.send(resObj);
  })
  .catch((err) => {
    resObj = {
      status: false,
      response: {},
      message: err.message,
    };
    res.status(400).send(resObj);
  });
};

exports.calculateDeleteAddon = (req, res) => {
  let resObj;
  let cart_id = req.params.cart_id;
  let payload = req.body;
  if(payload['cart_addon_id'].length == 0||payload['cart_addon_id']=='') {
    resObj = {
      status: false,
      response: {},
      message: "please select your cart_addon_id",
    };
    res.status(400).send(resObj);
    return;
  }
  cartService.addonDeleteCalculateTotal(cart_id,payload).then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully Add addon calculate",
      },
    };
    res.send(resObj);
  })
  .catch((err) => {
    resObj = {
      status: false,
      response: {},
      message: err.message,
    };
    res.status(400).send(resObj);
  });
};

exports.findCartDeliveryTimeSlot = (req, res) => {
  let resObj;
  let cartId=req.params.cart_id;
  cartService.fetchCartDeliveryTimeSlot(cartId).then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully view Cart Delivery Time Slot",
      },
    };
    res.send(resObj);
  }).catch(err => {
    resObj = {
      status: false,
      response: {
        message: err,
      },
    };
    res.status(400).send(resObj);
  })
};

exports.findCartAvailability = (req, res) => {
  let resObj;
  let cartId=req.params.cart_id;
  cartService.fetchCartAvailability(cartId).then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully Cart Availability",
      },
    };
    res.send(resObj);
  }).catch(err => {
    resObj = {
      status: false,
      response: {
        message: err,
      },
    };
    res.status(400).send(resObj);
  })
};
