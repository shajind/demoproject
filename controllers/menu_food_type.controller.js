const menuFoodTypeService = require('../services/menu_food_type.service');

exports.saveMenuFoodType = (req, res) => {
    let menuFoodTypeData = req.body;
    let resObj;
    menuFoodTypeService.createMenuFoodType(menuFoodTypeData)
    .then(response => {
        console.log("data:",response);
        resObj = {
            status:true,
            "response":{
                "data":response,
                "message":"Successfully save Menu Food Type"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}


exports.getAllMenuFoodType = (req,res) =>{
    let resObj;
    menuFoodTypeService.findAllMenuFoodType()
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view Menu Food type"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}