const menuItemService = require("../services/menu_item.service");
const menuItemAvailableService = require('../services/menu_item_available.service');
const menuTypeService = require("../services/menu_type.service");
const userDAO = require("../helpers/menuItemhelpingTables.dao");
const menuItemDAO=require("../dao/menu_item.dao");

exports.saveMenuItem = (req, res) => {
  let menuItemData = req.body;
  if (req && req.file && req.file.filename) {
    menuItemData["menu_item_img"] = req.file.filename;
  }
  let resObj;
  if(!menuItemData['yumma_price']){
    menuItemData['yumma_price']='0';
  }
  let chef_id = req.session.chefId;
  if(!menuItemData['chef_id']){
    menuItemData['chef_id']=chef_id;
  }
  console.log("menuItemData :", menuItemData);
  menuItemService.createMenuItem(menuItemData,chef_id)
  .then((response) => {
    resObj = {
      status: true,
      response: {
        data:response,
      },
      message: "Successfully save"
    };
    res.send(resObj);
  }).catch((err) => {
    resObj = {
      status: false,
      response: {},
      message: err.message,
    };
    res.status(400).send(resObj);
  });
};

exports.updateMenuItem = (req, res) => {
  let itemId = req.params.menu_item_id;
  let menuItemData = req.body;
  if (req && req.file && req.file.filename) {
    menuItemData["menu_item_img"] = req.file.filename;
  }
  console.log("menuItemData :", menuItemData);
    //let chef_id = req.session.chefId;
  let resObj;
  menuItemService.updateMenu_Item(menuItemData, itemId)
    .then((response) => {
      console.log("data:", response);
      resObj = {
        status: true,
        response: {
          //data: response,
          message: "Successfully update menu Item",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err,
        },
      };
      res.status(400).send(resObj);
    });
};

exports.updateYummapriceMenuItem = (req, res) => {
  let payload = req.body;
  let menuItemId = req.params.menu_item_id;
  let resObj;
  if(!payload['yummaprice'] || payload['yummaprice'] == '') {
    resObj = {
      status: false,
      "response": {
        "message": "Please enter Menu Item yummaprice"
      }
    };
    res.status(400).send(resObj);
    return;
  }
  let menuItemData={'yumma_price':payload['yummaprice']};
  menuItemService.updateMenuItemYummaprice(menuItemData, menuItemId).then((response) => {
    resObj = {
      status: true,
      response: {
        data: response,
        message: "Successfully update menu Item yummaprice",
      },
    };
    res.send(resObj);
  }).catch((err) => {
    resObj = {
      status: false,
      response: {
        message: err,
      },
    };
    res.status(400).send(resObj);
  });
};

// function menuItemvalues(data){
//   let results = [];
//   if(!data) {
//     return results;
//   }
//   data.map((row) => {
//     results.push(row.menu_item_id);
//   });
//   return results;
// };
exports.getAllMenuItem = (req, res) => {
  let filters = req.query.filters;
  let sortBy = req.query.sortby;
  let customerId = req.query.customer_id
  menuItemService.fetchMenuItems(filters,sortBy,customerId)
  .then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully view Menu Item",
      },
    };
    res.send(resObj);
  })
  .catch((err) => {
    resObj = {
      status: false,
      response: {
        message: err,
      },
    };
    res.status(400).send(resObj);
  });
};

exports.getMenuItemByMenuItemId = (req, res) => {
  let itemId = req.params.menu_item_id;
  let resObj;
  menuItemService.fetchById(itemId)
    .then((response) => {
      resObj = {
        status: true,
        response: {
          data: response,
          message: "Successfully view menu Item",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err.message,
        },
      };
      res.status(400).send(resObj);
    });
};

exports.getAllMenuItemByChef = (req, res) => {
  let filters = req.query.filters;
  let sortBy = req.query.sortby;
  let chefId=req.session.chefId;
  menuItemService.fetchMenuItemByChef(filters,sortBy,chefId)
  .then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully view Menu Item",
      },
    };
    res.send(resObj);
  })
  .catch((err) => {
    resObj = {
      status: false,
      response: {
        message: err,
      },
    };
    res.status(400).send(resObj);
  });
};

exports.updateMenuItemVisibility = (req, res) => {
  let itemId = req.params.menu_item_id;
  let menu_items = req.body;
  let payload={};
  payload["is_visible"]=menu_items["visibility"];
  let chef_id = req.session.chefId;
  let resObj;
  menuItemService.updateMenuItemVisibility(payload,itemId,chef_id)
    .then((response) => {
      console.log("data:", response);
      resObj = {
        status: true,
        response: {
          data: response,
          message: "Successfully update menu Item Visibility",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err.message,
        },
      };
      res.status(400).send(resObj);
    });
};

exports.updateMenuItemChefVisibility = (req, res) => {
  let menuItemData = req.body;
  let payload={};
  payload["is_visible"]=menuItemData["visibility"]; 
  let chef_id = req.session.chefId;
  let resObj;
  menuItemService.updateMenuItemChefVisibility(payload,chef_id)
    .then((response) => {
      console.log("data:", response);
      resObj = {
        status: true,
        response: {
          data: response,
          message: "Successfully update Menu Item Visibility Chef",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err.message,
        },
      };
      res.status(400).send(resObj);
    });
};

exports.deleteMenuItem = (req, res) => {
  let itemId = req.params.menu_item_id;
  let menuItemData = {"is_deleted": true};
  let chef_id = req.session.chefId;
  let resObj;
  menuItemService.updateMenuItemVisibility(menuItemData, itemId,chef_id)
    .then((response) => {
      console.log("data:", response);
      resObj = {
        status: true,
        response: {
          data: response,
          message: "Successfully delete menu Item",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err.message,
        },
      };
      res.status(400).send(resObj);
    });
};

exports.getMenuItemBymenuType = (req, res) => {
  let menuTypeCode = req.query.menu_type;
  let resObj;
  menuTypeService.getMenuTypeByCode(menuTypeCode).then((data)=>{ 
    console.log(data['menu_type_id']);
    return menuItemService.getMenu_ItemBymenuTypeId(data['menu_type_id'])
  })
  .then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully view MenuItem",
      },
    };
    res.send(resObj);
  })
  .catch((err) => {
    resObj = {
      status: false,
      response: {
        message: err,
      },
    };
    res.status(400).send(resObj);
  });
};

exports.getMenuItemAccompanimentBychef = (req, res) => {
  let chefId = req.query.chef_id;
  let resObj;
  menuItemService.getMenuItemAccompanimentBychefId(chefId).then((data)=>{
    return menuItemAvailableService.getMenuItemAvailableAccompanimentByMenuItemId((data))
  })
  .then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully view MenuItem Accompaniment",
      },
    };
    res.send(resObj);
  })
  .catch((err) => {
    resObj = {
      status: false,
      response: {
        message: err,
      },
    };
    res.status(400).send(resObj);
  });
};

exports.getDeliverySlot = (req, res) => {
  let menuItemIds = req.query.menu_item_ids;
  let resObj;
  menuItemService.fetchDeliverySlot(menuItemIds)
  .then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully view MenuItem delivery slot",
      },
    };
    res.send(resObj);
  })
  .catch((err) => {
    resObj = {
      status: false,
      response: {
        message: err,
      },
    };
    res.status(400).send(resObj);
  });
};