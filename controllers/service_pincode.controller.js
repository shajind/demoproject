const ServicePincodeService = require("../services/service_pincode.service");

exports.saveServicePincode = (req, res) => {
    let ServicePincode = req.body;
    ServicePincodeService.createServicePincode(ServicePincode).then((data) => {
        resObj = {
          status: true,
          response: {
                data: data,
                message: "Successfully save ServicePincode",
            },
        };
        res.send(resObj);
    }).catch(err => {
        resObj = {
            status: false,
            response: {
                message: err,
            },
        };
        res.status(400).send(resObj);
    })
};

exports.findServicePincodeByPincode = (req, res) => {
    let pincodeNo = req.params.pincode;
    ServicePincodeService.fetchServicePincodeByPincode(pincodeNo)
    .then((data) => {
      resObj = {
        status: true,
        response: {
          data: data,
          message: "This pincode is servicable",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: "This pincode is not servicable",
        },
      };
      res.status(400).send(resObj);
    });
};


exports.findAllServicePincode = (req, res) => {
    ServicePincodeService.findAllServicePincode()
    .then((data) => {
      resObj = {
        status: true,
        response: {
          data: data,
          message: "Successfully view ServicePincode",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err,
        },
      };
      res.status(400).send(resObj);
    });
};

