const customerService = require('../services/customer.service');
const userService = require("../services/users.service");
const tokenGenerator = require('../utils/token-generator');

let userData = (data, otp) => {
    let userData = {};
    let customerdata = {};
    let customerAddress = {};
    userData["user_name"] = data.cust_fullname;
    userData["user_email"] = data.user_email;
    userData["country_code"] = data.country_code;
    customerdata["cust_fullname"] = data.cust_fullname;
    customerdata["cust_gender"] = data.cust_gender;
    customerdata["cust_dob"] = data.cust_dob;
    customerdata["cust_fpreference"] = data.cust_fpreference;
    customerdata["cust_address1"] = data.cust_address1;
    customerdata["cust_address2"] = data.cust_address2;
    customerdata["city_id"] = data.city_id;
    customerdata["state_id"] = data.state_id;
    customerdata["cust_zipcode"] = data.cust_zipcode;
    customerdata["country_code"] = data.country_code;
    customerAddress["address_line1"] = data.cust_address1;
    customerAddress["address_line2"] = data.cust_address2;
    customerAddress["latitude"] = data.latitude;
    customerAddress["longitude"] = data.longitude;
    customerAddress["pincode"] = data.cust_zipcode;
    customerAddress["address_type"] = "home";
    customerAddress["city_id"] = data.city_id;
    customerAddress["state_id"] = data.state_id;
    return { userData, customerdata ,customerAddress};
};
  

exports.saveCustomerProfile = (req, res) => {
    console.log("saveCustomerProfile files", req.files);
    let body = req.body;
    let payload = userData(body);
    let userId=req.session.userId;
    payload.customerdata["user_id"]=userId;
    if(req.files) {
        if(req.files.profile_image) {
            payload.customerdata["cust_profile_pic"]=req.files.profile_image[0].filename;
        }
        if(req.files.profile_cover_image) {
            payload.customerdata["cust_cover_pic"]=req.files.profile_cover_image[0].filename;
        }
    }    
    let resObj;
    customerService.updateCustomerProfileData(userId, payload)
    .then(response => {
        resObj = {
            status:true,
            "response":{
                "data":response,
                "message":"Successfully save Customers"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

exports.getAllCustomers = (req,res) =>{
    var resObj;
    customerService.getAllCustomers()
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view All Customers"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

exports.getCustomerProfile = (req,res) =>{
    let userId = req.session.userId;
    var resObj;
    customerService.fetchCustomerByUserId(userId)
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view Customers profile"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

exports.customerStatusInactive = (req, res) => {
    let customerId=req.query.customer_id;
    let payload={};
    payload['status']="inactive";
    console.log("payload",payload,customerId)  
    var resObj;
    customerService.customerUpdate(payload,customerId)
    .then((data) => {
        return customerService.getCustomersByCustomersId(customerId);
    })
    .then((data) => {
        return userService.userUpdate(payload,data['user_id']);
    })
    .then((data) => {
        resObj = {
            status: true,
            response: {
            data: data,
            message: "Sucessfully deactive",
            },
        };
        res.send(resObj);
    })
    .catch((err) => {
        resObj = {
            status: false,
            response: {
            message: err,
            },
        };
        res.status(400).send(resObj);
    });
};

exports.customerStatusActive = (req, res) => {
    let customerId=req.query.customer_id;
    let payload={};
    payload['status']="active";
    console.log("payload",payload,customerId)  
    let resObj;
    customerService.customerUpdate(payload,customerId)
    .then((data) => {
        return customerService.getCustomersByCustomersId(customerId);
    })
    .then((data) => {
        return userService.userUpdate(payload,data['user_id']);
    })
    .then((data) => {
        resObj = {
            status: true,
            response: {
                data: data,
                message: "Sucessfully active",
            },
        };
        res.send(resObj);
    })
    .catch((err) => {
        resObj = {
            status: false,
            response: {
                message: err,
            },
        };
        res.status(400).send(resObj);
    });
};

exports.customerLogin = (req, res) => {    
    var resObj;
    customerService.validateLogin(req.body).then(data => {
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully login",
                "access_token": tokenGenerator.genToken(data.user_id)
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        console.log(err);
        resObj={
            "status":false,
            "response":{
                "message":err,                
                "message":"wrong password"
            }
        };
        res.status(400).send(resObj);
    })
}