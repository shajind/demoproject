const AdvanceBookingMstService = require('../services/advance_booking_master.service');


exports.saveAdvanceBookingMst = (req, res) => {
    let advanceBookingData = req.body;
    console.log("advanceBookingData :",advanceBookingData);
    let resObj;
    AdvanceBookingMstService.advanceBookingMstSave(advanceBookingData)
    .then(response => {
        console.log("data:",response);
        resObj = {
            status:true,
            "response":{
                "data":response,
                "message":"Successfully save Advance Booking"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}


exports.getAllAdvanceBookingMst = (req,res) =>{
    let resObj;
    AdvanceBookingMstService.getAllAdvanceBooking()
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view Advance Booking"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

