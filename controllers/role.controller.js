const roleService = require('../services/role.service');

exports.createNewRole = (req, res) => {
    let data = req.body;
    console.log("user_role_data :",data);
    let resObj;
    roleService.saveRole(data).then(response => {
        console.log("data:",response);
        resObj = {
            status:true,
            response:{
                "data":response
            },
            message: 'Successfully save user_type'
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "message": err
        };
        res.status(400).send(resObj);
    })
}


exports.getAllRoles = (req,res) =>{
    let resObj;
    roleService.fetchAllRoles()
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view user_type"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

