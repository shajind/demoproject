const testimonialService = require('../services/testimonial.service');

exports.saveTestimonial = (req, res) => {
    if (!req.file) {
        res.status(500);
        console.log(req.file);
        return res.send("plz upload image");
    }
    let payload = req.body;    
    payload['testimonial_photo_name']=req.file.filename;
    let resObj;
    testimonialService.saveTestimonial(payload)
    .then(response => {
        console.log("data:",response);
        resObj = {
            status:true,
            "response":{
                "data":response,
                "message":"Successfully save testimonial"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}


exports.getAllTestimonial = (req,res) =>{
    let resObj;
    testimonialService.getAllTestimonial()
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view testimonial"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

exports.getTestimonialById = (req,res) =>{
    //let id = req.session.userId;
    let id = req.query.id;    
    let resObj;
    testimonialService.getTestimonialById(id)
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view testimonial"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}