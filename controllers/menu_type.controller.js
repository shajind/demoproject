const menuTypeService = require("../services/menu_type.service");

exports.saveMenuType = (req, res) => {
  let menuType = req.body;
  menuTypeService.saveMenuType(menuType)
    .then((data) => {
      resObj = {
        status: true,
        response: {
          data: data,
          message: "Successfully view MenuTypes",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err,
        },
      };
      res.status(400).send(resObj);
    });
};

exports.getAllMenuType = (req, res) => {
  menuTypeService.getAllMenuType()
    .then((data) => {
      resObj = {
        status: true,
        response: {
          data: data,
          message: "Successfully view MenuTypes",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err,
        },
      };
      res.status(400).send(resObj);
    });
};
