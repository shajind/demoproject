const couponService=require("../services/coupon.service");


exports.saveCoupon = (req, res) => {
    let couponData = req.body;
    console.log("couponData :",couponData);
    let resObj;
    couponService.saveCoupon(couponData)
    .then(response => {
        resObj = {
            status:true,
            "response":{
                "data":response,
                "message":"Coupon created Successfully"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

exports.getAllCoupon = (req,res) =>{
    let resObj;
    couponService.getCoupon()
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view Coupon"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}
