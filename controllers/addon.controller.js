const addonService = require("../services/addon.service");


exports.saveAddon = (req, res) => {
  let resObj;
  let addon = req.body;
  if (req && req.file && req.file.filename) {
    addon["addon_image"] = req.file.filename;
  }
  console.log("addon :", addon);
  addonService.createAddon(addon).then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully save Addon",
      },
    };
    res.send(resObj);
  }).catch(err => {
    resObj = {
      status: false,
      response: {
        message: err,
      },
    };
    res.status(400).send(resObj);
  })
};

exports.findAllAddon = (req, res) => {
  let resObj;
  addonService.findAllAddon().then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully view Addon",
      },
    };
    res.send(resObj);
  }).catch((err) => {
    resObj = {
      status: false,
      response: {
        message: err,
      },
    };
    res.status(400).send(resObj);
  });
};

exports.findMenuItemAddon = (req, res) => {
  let resObj;
  let menuItemId=req.params.menu_item_id;
  addonService.fetchAddonMenuItem(menuItemId).then((data) => {
    resObj = {
      status: true,
      response: {
        data: data,
        message: "Successfully view MenuItem Addon",
      },
    };
    res.send(resObj);
  }).catch((err) => {
    resObj = {
      status: false,
      response: {
        message: err,
      },
    };
    res.status(400).send(resObj);
  });
};

