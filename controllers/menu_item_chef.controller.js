const MenuItemChefService = require('../services/menu_item_chef.service');


exports.saveMenuItemChef = (req, res) => {
    let menuItemChefdata = req.body;
    if (req && req.file && req.file.filename) {
        menuItemChefdata["item_img"] = req.file.filename;
    }
    console.log("menuItemChefdata :",menuItemChefdata);
    let resObj;
    let menuItemChefId;
    let MenuItemChefResponse;
    MenuItemChefService.saveMenuItemChef(menuItemChefdata)
    // .then((data)=>{
    //     MenuItemChefResponse=(data);
    //     menuItemChefId=data["menu_item_chef_id"];
    //     console.log(menuItemChefId)
    //     return MenuItemChefService.saveMenuItemChefInclusion(menuItemChefdata.inclusion,menuItemChefId);
    // })
    .then(response => {
        //console.log("data:",response);
        resObj = {
            status:true,
            "response":{
                "data":response,
                "message":"Successfully save Menu Item Chef"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}


exports.getAllMenuItemChef = (req,res) =>{
    let resObj;
    MenuItemChefService.getMenuItemChef()
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view MenuItemChef"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}
