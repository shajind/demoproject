const peopleServiceMstService = require('../services/people_service_master.service');


exports.savePeopleServiceMst = (req, res) => {
    let PeopleServiceMstData = req.body;
    console.log("PeopleServiceMstData :",PeopleServiceMstData);
    let resObj;
    peopleServiceMstService.peopleServiceMstSave(PeopleServiceMstData)
    .then(response => {
        console.log("data:",response);
        resObj = {
            status:true,
            "response":{
                "data":response,
                "message":"Successfully save People Service"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}


exports.getAllPeopleServiceMst = (req,res) =>{
    let resObj;
    peopleServiceMstService.getAllPeopleService()
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view People Service"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

