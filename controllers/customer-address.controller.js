const customerService = require("../services/customer-address.service");

exports.saveAddresses = (req, res) => {
  let address = req.body;
  address["user_id"] = req.session.userId;
  let resObj;
  customerService
    .saveAddresses(address)
    .then((response) => {
      resObj = {
        status: true,
        response: {
          data: response,
          message: "Successfully save address",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err,
        },
      };
      res.status(400).send(resObj);
    });
};

exports.getAllAddress = (req, res) => {  
  let userId = req.session.userid;
  let resObj;
  customerService
    .getAllAddress(userId)
    .then((response) => {
      resObj = {
        status: true,
        "response": {
          "data": response,
          "message": "Successfully fetch addresses",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      console.log("address controller catch ", err);
      resObj = {
        status: false,
        "response": {
          "message": err,
        },
      };
    res.status(400).send(resObj);
  });  
};

exports.deleteAddress = (req, res) => {
  let addressId = req.params.address_id;

  customerService
    .deleteAddress(addressId)
    .then((response) => {
      resObj = {
        status: true,
        response: {
          data: response,
          message: "Successfully delete address",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err,
        },
      };
      res.status(400).send(resObj);
    });
};

exports.updateAddress = (req, res) => {
  let address = req.body;
  let addressId = req.params.address_id;
  let resObj;
  customerService
    .updateAddress(addressId, address)
    .then((response) => {
      resObj = {
        status: true,
        response: {
          data: response,
          message: "Successfully update address",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err,
        },
      };
      res.status(400).send(resObj);
    });
};
