const userService = require('../services/users.service');
var tokenGenerator = require('../utils/token-generator');
const chefService = require("../services/chef.service");
const user_RoleService = require('../services/user_role.service');
const roleConstant = require("../config/role.json");

exports.generateCustomerLoginOTP = (req, res) => {
    var userdata = req.body;
    console.log("userdata :",userdata);
    var resObj;
    userService.generateUserOTP(userdata, roleConstant.ROLE_CUSTOMER)
    .then(response => {
        console.log("data:",response);
        resObj = {
            status:true,
            "response":{
                "data": response,
                "message":"Successfully  Save Mobile Number"
            }
        };

        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

exports.adminLogin= (req, res) => {    
    var resObj;
    let userName = req.body.user_name;
    let passWord = req.body.password;   
    let userTypeRole = roleConstant.ROLE_ADMIN;
    console.log(userName,passWord,userTypeRole)
    user_RoleService.getUserTypeByCode(userTypeRole)
    .then(data => {
        return userService.adminlogin(userName,passWord,data['role_id']);
    })
    .then(data => {
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully login",
                "access_token": tokenGenerator.genToken(data.user_id)
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err,                
                "message":"wrong password"
            }
        };
        res.status(400).send(resObj);
    })
}

// exports.signUp= (req, res) => {
//     let otp=generaterOTP.OTPpassword();    
//     var user_data = req.body;
//     user_data["user_otp"]=otp;    
//     console.log("user_data :",user_data);
//     var resObj;
//     userService.Signup(user_data)
//     .then(response => {
//         console.log("data:",response);
//         resObj = {
//             status:true,
//             "response":{
//                 "data":response,
//                 "message":"User Sucessfully Registered"
//             }
//         };
//         res.send(resObj);
//     })
//     .catch(err=>{
//         resObj={
//             "status":false,
//             "response":{
//                 "message":err
//             }
//         };
//         res.status(400).send(resObj);
//     })
// }
// userService.login(userName, passWord, roleId).then(data => {
//     findUserData(data).then(response => {
//         console.log("helooooooooooo",response);
//         console.log("heloooodata",data);
//         response["access_token"]= tokenGenerator.genToken(data.user_id);
//         response["user_name"] =data.user_name
//         response["user_email"] =data.user_email
//         response["user_mobile"] =data.user_mobile
//         response["status"] =data.status      
//         //let response=Object.assign(userResponse.dataValues,transporteResponse.dataValues);        
//         resObj = {
//             status: true,
//             "response":{
//                 "data":response,
//                 "access_token": tokenGenerator.genToken(data.user_id)
//             }
//         };
//         res.send(resObj);
//     }, err => {

//     })

exports.add_device_token = (req,res) =>{
    var userdata = req.body;    
    //let id = req.session.userId;
    let id = req.query.userId;
    userdata["user_id"] =id;
    console.log('userdata ',userdata)
    var resObj;
    userService.device_token(userdata)
    .then(response => {        
        resObj = {
            status:true,
            "response":{   
                "data":response,             
                "message":"Successfully save device_token"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}




