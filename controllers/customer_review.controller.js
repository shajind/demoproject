const customerReviewService = require("../services/customer_review.service");

exports.saveCustomerReview = (req, res) => {
    let payload = req.body;
    if(req.file) {
        payload['image_or_video_path']=req.file.filename;
    }
    let resObj;
    customerReviewService.createCustomerReview(payload)
    .then((data) => {
    resObj = {
        status: true,
        response: {
            data: data,
            message: "Successfully save Customer Review",
        },
    };
    res.send(resObj);
    }).catch(err => {
    resObj = {
        status: false,
        response: {
            message: err,
        },
    };
    res.status(400).send(resObj);
    })
};

exports.findAllCustomerReview = (req, res) => {
    let resObj;
    let filters = req.query.filters;
    customerReviewService.findAllCustomerReview(filters).then((data) => {
      resObj = {
        status: true,
        response: {
          data: data,
          message: "Successfully view Customer Review",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err,
        },
      };
      res.status(400).send(resObj);
    });
};

exports.getCustomerReview = (req, res) => {
    let resObj;    
    let orderId=req.query.order_id
    customerReviewService.fetchCustomerReview(orderId).then((data) => {
      resObj = {
        status: true,
        response: {
          data: data,
          message: "Successfully view Customer Review",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err,
        },
      };
      res.status(400).send(resObj);
    });
};
