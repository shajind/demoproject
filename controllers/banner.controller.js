const bannerService = require('../services/banner.service');

exports.saveBanner = (req, res) => {
    if (!req.file) {
    res.status(500);
    console.log(req.file);
    return res.send("plz upload image");
    }
    let payload = req.body;
    payload['banner_image']=req.file.filename;
    console.log(payload);
    let resObj;
    bannerService.saveBanner(payload)
    .then(response => {
        console.log("data:",response);
        resObj = {
            status:true,
            "response":{
                "data":response,
                "message":"Successfully save Banner"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

exports.updateBanner = (req, res) => {    
    let banner_id = req.query.bannerId;
    let payload = req.body;
    if (req.file.filename) {
        payload['banner_image']=req.file.filename;
    }    
    console.log(payload);
    let resObj;
    bannerService.updateBanner(payload,banner_id)
    .then(response => {
        console.log("data:",response);
        resObj = {
            status:true,
            "response":{
                "data":response,
                "message":"Successfully save Banner"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}



exports.getAllBanner = (req,res) =>{
    let resObj;
    bannerService.getAllBanner()
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view Banner"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}

exports.getAllBannerByType = (req,res) =>{
    //let id = req.session.userId;
    let bannerType = req.query.bannerType;    
    let resObj;
    bannerService.getAllBannerByType(bannerType)
    .then(data => {        
        resObj = {
            status:true,
            "response":{
                "data":data,                
                "message":"Successfully view Banner"
            }
        };
        res.send(resObj);
    })
    .catch(err=>{
        resObj={
            "status":false,
            "response":{
                "message":err
            }
        };
        res.status(400).send(resObj);
    })
}