const yummaCommissioService = require("../services/yumma_commission.service");


exports.saveYummaCommission = (req, res) => {
  let payload = req.body;
  yummaCommissioService.createYummaCommission(payload)
    .then((data) => {
      resObj = {
        status: true,
        response: {
          data: data,
          message: "Successfully save Yumma Commission",
        },
      };
      res.send(resObj);
    }).catch(err => {
      resObj = {
          status: false,
          response: {
            message: err,
          },
        };
        res.status(400).send(resObj);
    })
};


exports.findAllYummaCommission = (req, res) => {
    yummaCommissioService.findAllYummaCommission().then((data) => {
      resObj = {
        status: true,
        response: {
          data: data,
          message: "Successfully view Yumma Commission",
        },
      };
      res.send(resObj);
    })
    .catch((err) => {
      resObj = {
        status: false,
        response: {
          message: err,
        },
      };
      res.status(400).send(resObj);
    });
};