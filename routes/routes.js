const usersCtrl = require("../controllers/users.controller");
const countryCtrl = require("../controllers/country.controller");
const customerCtrl = require("../controllers/customer.controller");
const chefController = require("../controllers/chef.controller");
const user_typeCtrl = require("../controllers/user_role.controller");
const menuItemavAilableCtrl = require("../controllers/menu_item_available.controller");
const menuItemCtrl = require("../controllers/menu_item.controller");
const menuFoodTypeCtrl = require("../controllers/menu_food_type.controller");
const categoryController = require("../controllers/category.controller");
const menuTypeController = require("../controllers/menu_type.controller");
const notificatCtrl = require("../controllers/notification.controller");
const notificatlogCtrl = require("../controllers/notificationlog.controller");
const cityCtrl = require("../controllers/city.controller.js");
const stateCtrl = require("../controllers/state.controller.js");
const bannerCtrl = require("../controllers/banner.controller");
const testimonialCtrl = require("../controllers/testimonial.controller");
const cartController = require("../controllers/cart.controller");
const orderController = require("../controllers/order.controller");
const roleCtrl = require("../controllers/role.controller");
const courseCtrl = require("../controllers/course.controller");
const deliveryChargeCtrl = require("../controllers/delivery_charge.controller");
const addonCtrl = require("../controllers/addon.controller");
const orderStatusController = require("../controllers/order_status.controller");
const taxController = require("../controllers/tax.controller");
const yummaCommissionController = require("../controllers/yumma_commission.controller");
const customerReviewCtrl = require("../controllers/customer_review.controller");
const varietyCtrl = require("../controllers/variety.controller");


const advanceBookingMasterController = require("../controllers/advance_booking_master.controller");
const deliveryTimeSlotMasterController = require("../controllers/delivery_time_slot_master.controller");
const peopleServiceMasterController = require("../controllers/people_service_master.controller");

const couponController=require("../controllers/coupon.controller");

const customerAddressController = require("../controllers/customer-address.controller");
const customerCurrentLocation = require("../controllers/customer_current_location.controller");


const middleware = require("../middleware/auth.middleware");

const uploadImage = require("../middleware/image.middleware");
const menuItemImage = require("../middleware/menu_item_img.middleware");
const menuItemChefImage = require("../middleware/menu_item_chef_img.middleware");
const uploadChefImage = require("../middleware/chefimage.middleware");
const categoryImage = require("../middleware/category.middleware");

const bannerImage = require("../middleware/bannerImage.middleware");
const testimonialImage = require("../middleware/testimonial.middleware");
const fileUpload = require("../helpers/fileupload.helper");
const menuItemAccompainmentController=require("../controllers/menu_item_accompainment.controller");
const kitchenTypeController=require("../controllers/kitchen_type.controller");
const menuItemChefCtrl=require("../controllers/menu_item_chef.controller");
const PayUMoneypaymentCtrl = require('../payment/payumoney.controller');
const paymentCtrl = require('../payment/payment.controller');
const servicePincodeCtrl=require("../controllers/service_pincode.controller");

const chefImageMiddleware = require("../middleware/chefimage2.middleware");
const chefDocumentMiddleware = require("../middleware/chef_document.middleware");
const earningController = require('../controllers/earning.controller');
const chefReviewMiddleware = require("../middleware/review.middleware");


module.exports = (app) => {
  app.post("/api/v1/customers/generate_otp", usersCtrl.generateCustomerLoginOTP);
  app.post("/api/v1/login/admin", usersCtrl.adminLogin);
  app.post("/api/v1/chefs/login", chefController.login);
  app.post("/api/v1/customers/login", customerCtrl.customerLogin);

  // //registration user
  // app.post("/api/v1/user/signUp", usersCtrl.signUp);
  app.post("/api/v1/device_token", middleware, usersCtrl.add_device_token); //device tookan

  app.post("/api/v1/customers", middleware, uploadImage, customerCtrl.saveCustomerProfile);
  app.get("/api/v1/customers", customerCtrl.getAllCustomers);
  //app.post("/api/v1/customer/profileImage",uploadImage,customerCtrl.saveCustomerProfileImage);
  app.get("/api/v1/customer", middleware, customerCtrl.getCustomerProfile);
  app.put("/api/v1/customer/deactive", customerCtrl.customerStatusInactive);
  app.put("/api/v1/customer/active", customerCtrl.customerStatusActive);
  
  //for country
  app.post("/api/v1/country", countryCtrl.saveCountry);
  app.get("/api/v1/countries", countryCtrl.getAllCountries);

  //for city
  app.post("/api/v1/city", cityCtrl.saveCity);
  app.get("/api/v1/cities", cityCtrl.getAllCityBystateId);

  //for states
  app.post("/api/v1/state", stateCtrl.saveState);
  app.get("/api/v1/states", stateCtrl.getAllState);

  // for chef
  app.post("/api/v1/chefs", chefController.chefSignUp);
  app.post("/api/v1/chefs/generate_otp", chefController.chefSendLoginOTP);
  app.get("/api/v1/chef/getChefById",middleware, chefController.getChefProfile); //chef profile
  app.get("/api/v1/chefs", chefController.getAllChefs);
  app.put("/api/v1/chef/deactive", chefController.chefStatusInactive);
  app.put("/api/v1/chef/active", chefController.chefStatusActive);
  app.put('/api/v1/chef/updateChef', middleware, chefImageMiddleware, chefController.updateProfile);
  app.put('/api/v1/chef/uploadDocument', middleware, chefDocumentMiddleware, chefController.uploadDocument);


  //app.get('/api/v1/item/getAllItemByChef/', chefController.getChefItems);  //menu
  //app.post('api/v1/item/addItemByChef',chefController.saveItemByChef);   //addItems

  //for role
  app.post("/api/v1/roles", roleCtrl.createNewRole);
  app.get("/api/v1/roles", roleCtrl.getAllRoles);

  //for menu item available
  app.post("/api/v1/menu", menuItemavAilableCtrl.saveMenu);
  //app.get("/api/v1/menuItem/available", menuItemavAilableCtrl.gemenuItemTypeCtrltAllMenuItemAvailable);
  app.get("/api/v1/menu_item/available", menuItemavAilableCtrl.getMenuItemAvailableByMenuItemId);
  //app.get("/api/v1/menu_items/:id/accompainments", menuItemCtrl.getAllMenuItem);
  //app.post("/api/v1/menu_items/:id/accompainments", menuItemCtrl.saveMenuItemAccompaintments);

  //for coupons
  app.post("/api/v1/coupons",couponController.saveCoupon);
  app.get("/api/v1/coupons", couponController.getAllCoupon);

  //for menutype
  app.post("/api/v1/menuType", menuTypeController.saveMenuType);
  app.get("/api/v1/menuType", menuTypeController.getAllMenuType);

  //for menu_items
  app.post("/api/v1/menuItem",middleware, menuItemImage, menuItemCtrl.saveMenuItem);
  app.put("/api/v1/menu_items/:menu_item_id/yummaprice", menuItemCtrl.updateYummapriceMenuItem);
  app.put("/api/v1/menu_items/:menu_item_id/visibility",middleware, menuItemCtrl.updateMenuItemVisibility);
  app.put("/api/v1/menu_items/visibility",middleware, menuItemCtrl.updateMenuItemChefVisibility);
  app.put("/api/v1/menu_items/:menu_item_id",menuItemImage, menuItemCtrl.updateMenuItem);
  app.get("/api/v1/menu_items/chef",middleware, menuItemCtrl.getAllMenuItemByChef);
  app.get("/api/v1/menuItem/menuType", menuItemCtrl.getMenuItemBymenuType);
  app.get("/api/v1/menuItem/accompaniments", menuItemCtrl.getMenuItemAccompanimentBychef);
  app.get("/api/v1/menu_item/delivery_slot", menuItemCtrl.getDeliverySlot);
  app.get("/api/v1/menu_items/:menu_item_id", menuItemCtrl.getMenuItemByMenuItemId);
  app.get("/api/v1/menuItems", menuItemCtrl.getAllMenuItem);
  app.delete("/api/v1/menu_items/:menu_item_id",middleware, menuItemCtrl.deleteMenuItem);

  //for menu_item_chef
  app.post("/api/v1/menuItemChef",menuItemChefImage, menuItemChefCtrl.saveMenuItemChef);
  app.get("/api/v1/menuItemChefs", menuItemChefCtrl.getAllMenuItemChef);

  // for kitchen_type
  app.get("/api/v1/kitchen_types", kitchenTypeController.findAllKitchenTypes);
  app.post("/api/v1/kitchen_types", kitchenTypeController.saveKitchenType);
  
  // for variety
  app.post("/api/v1/chefs/:chef_id/varieties", varietyCtrl.saveVariety);
  app.get("/api/v1/chefs/:chef_id/varieties", varietyCtrl.findVarietyByChef);
  app.get("/api/v1/varieties", varietyCtrl.findAllVariety);

  // for course
  app.post("/api/v1/courses", courseCtrl.saveCourse);
  app.get("/api/v1/courses", courseCtrl.findAllCourse);

  // for customer review
  app.post("/api/v1/customerReview", chefReviewMiddleware,customerReviewCtrl.saveCustomerReview);
  app.get("/api/v1/customerReview", customerReviewCtrl.getCustomerReview);
  app.get("/api/v1/customerReviews", customerReviewCtrl.findAllCustomerReview);

  // for addon
  app.post("/api/v1/addon",menuItemImage, addonCtrl.saveAddon);
  app.get("/api/v1/addon", addonCtrl.findAllAddon);

  app.get("/api/v1/menuItem/:menu_item_id/addon", addonCtrl.findMenuItemAddon);
  
  // for delivery_charge
  app.post("/api/v1/deliveryCharge", deliveryChargeCtrl.saveDeliveryCharge);
  app.get("/api/v1/deliveryCharge", deliveryChargeCtrl.findAllDeliveryCharg);
  app.put("/api/v1/deliveryCharge/:delivery_charge_id", deliveryChargeCtrl.updateDeliveryCharge);

  //for menu_items_accompainment
  app.post("/api/v1/accompainmentmenuItems",  menuItemAccompainmentController.saveAccompainment);
  app.get("/api/v1/accompainmentmenuItems", menuItemAccompainmentController.getAllAccompainment);
  app.get("/api/v1/menuItems/:menu_item_id/accompainments", menuItemAccompainmentController.getAllAccompainmentByMenuItemId);

  //for menu_food_type
  app.post("/api/v1/menuFoodType", menuFoodTypeCtrl.saveMenuFoodType);
  app.get("/api/v1/menuFoodTypes", menuFoodTypeCtrl.getAllMenuFoodType);

  //for category
  app.post("/api/v1/category",categoryImage, categoryController.saveCategory);
  app.get("/api/v1/category", categoryController.getAllCategory);
  app.get("/api/v1/parent/category", categoryController.getAllCategoryByParentId);

  //for notification
  app.post("/api/v1/notifications", notificatCtrl.saveNotification);
  app.get("/api/v1/notifications", notificatCtrl.getAllNotifications);
  app.get("/api/v1/notifications/:notification_id",notificatCtrl.getNotificationById);

  //for notificationlog
  app.post("/api/v1/notificationslog", notificatlogCtrl.saveNotificationLog);
  app.get("/api/v1/notificationslog", notificatlogCtrl.getAllNotificationLog);
  app.get("/api/v1/notificationslog/:notification_id",notificatlogCtrl.getAllNotificationLogById);

  //for banner
  app.post("/api/v1/banner", bannerImage, bannerCtrl.saveBanner);
  app.put("/api/v1/banner", bannerImage, bannerCtrl.updateBanner);
  app.get("/api/v1/banner", bannerCtrl.getAllBannerByType);
  app.get("/api/v1/banners", bannerCtrl.getAllBanner);

  //for testimonial
  app.post("/api/v1/testimonial",testimonialImage,testimonialCtrl.saveTestimonial);
  app.get("/api/v1/testimonial", testimonialCtrl.getTestimonialById);
  app.get("/api/v1/testimonials", testimonialCtrl.getAllTestimonial);
  
  
  //cart delivery slot and availablity
  app.get("/api/v1/carts/:cart_id/deliverytimeslot", cartController.findCartDeliveryTimeSlot);
  app.get("/api/v1/carts/:cart_id/availability", cartController.findCartAvailability);
  

  //for cart
  app.post("/api/v1/carts",middleware, cartController.addItemToCart);
  app.get("/api/v1/carts",middleware, cartController.getCartList);
  //app.delete("/api/v1/carts/:cart_id/cartItem/:cart_item_id",middleware, cartController.deleteCartItemById);//remove
  app.delete("/api/v1/carts/:cart_id/items/:item_id",middleware, cartController.deleteCartItemById); //keep
  app.post("/api/v1/carts/apply_coupon", cartController.applyCoupon);
  app.post("/api/v1/carts/:cart_id/apply_delivery_charge", cartController.applyDeliveryCharge);
  app.post("/api/v1/create_cart", middleware, cartController.createNewCustomerCart);
  app.delete("/api/v1/carts/:cart_id", middleware, cartController.deleteCartById);
  app.post("/api/v1/carts/:cart_id/addon", cartController.calculateAddon);
  app.post("/api/v1/carts/:cart_id/addonDelete", cartController.calculateDeleteAddon);


  // for orders
  app.post("/api/v1/checkout",middleware, orderController.checkoutOrder);
  //app.get("/api/v1/orders", middleware, orderController.getAllOrders);
  app.get("/api/v1/orders/chefStatus",middleware,orderController.getAllOrderChefStatus);
  app.get("/api/v1/orders/chef",middleware,orderController.getAllOrderChef);
  app.get("/api/v1/orders/customer",middleware, orderController.getAllOrderCustomer);
  app.get("/api/v1/orders",orderController.getAllOrders);

  app.get("/api/v1/orders/:order_id", orderController.getOrder);
  app.post("/api/v1/generate_hash", PayUMoneypaymentCtrl.generatePayUMoneyHash);
  
  app.post("/api/v1/payment/razorpay", paymentCtrl.generateRazorpay);

  // for order_chef_status
  app.post("/api/v1/orders/:order_id/accept", middleware, orderController.acceptOrderHandler);
  app.post("/api/v1/orders/:order_id/reject", middleware, orderController.rejectOrderHandler);
  app.post("/api/v1/orders/:order_id/completed", middleware, orderController.completedOrderHandler);
  app.post("/api/v1/orders/:order_id/outdelivery", middleware, orderController.outdeliveryOrderHandler);

  // for order_status
  app.post("/api/v1/orderStatus", orderStatusController.createOrderStatus);
  app.get("/api/v1/orderStatus", orderStatusController.getAllOrderStatus);


  //for quote
  app.post("/api/v1/advanceBooking",advanceBookingMasterController.saveAdvanceBookingMst);
  app.get("/api/v1/AdvanceBooking", advanceBookingMasterController.getAllAdvanceBookingMst);

  //for quote
  app.post("/api/v1/deliveryTimeSlot",deliveryTimeSlotMasterController.saveDeliveryTimeSlotMst);
  app.get("/api/v1/deliveryTimeSlot", deliveryTimeSlotMasterController.getAllDeliveryTimeSlotMst);
  
  //for quote
  app.post("/api/v1/peopleService",peopleServiceMasterController.savePeopleServiceMst);
  app.get("/api/v1/peopleService", peopleServiceMasterController.getAllPeopleServiceMst);

  //for quote
  app.post("/api/v1/tax",taxController.saveTax);
  app.get("/api/v1/tax", taxController.findAllTax);

  //for yumma commission
  app.post("/api/v1/yummaCommission",yummaCommissionController.saveYummaCommission);
  app.get("/api/v1/yummaCommission", yummaCommissionController.findAllYummaCommission);


  // for customer address
  app.post("/api/v1/customerAddress", middleware,customerAddressController.saveAddresses);
  app.put("/api/v1/customerAddress/:address_id",customerAddressController.updateAddress);
  app.get("/api/v1/customerAddress", middleware, customerAddressController.getAllAddress);
  app.delete("/api/v1/customerAddress/:address_id",customerAddressController.deleteAddress);


  //for customer current location
  app.post("/api/v1/customerCurrentLocation", middleware,customerCurrentLocation.saveCustomerCurrentLocation);
  app.get("/api/v1/customerCurrentLocation/:customer_id", customerCurrentLocation.getCustomerCurrentLocationByCustomerId);


  //earnings
  app.get("/api/v1/chefs/:chef_id/earnings", earningController.getTotalEarnings);
  app.get("/api/v1/chefs/:chef_id/earningDetails", earningController.getEarningDetails);

  app.post("/api/v1/weekdate", earningController.createWeekDate);


  //for servicepincode
  app.post("/api/v1/servicepincode", servicePincodeCtrl.saveServicePincode);
  app.get("/api/v1/servicepincode", servicePincodeCtrl.findAllServicePincode);
  app.get("/api/v1/service/:pincode/pincode", servicePincodeCtrl.findServicePincodeByPincode);

};
