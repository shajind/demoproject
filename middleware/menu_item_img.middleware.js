const multer  = require('multer');




let storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null,'./image/menuItemImg');
    },
    filename: (req, file, cb) => {
      console.log("file :",file)
      let type=file.originalname.split(".")
      let filetype=type[type.length-1];
      let menuData = req.body;
      let menuName;
      if(menuData.menu_item_name){
        menuName=menuData.menu_item_name;
      }else if(menuData.addon_name){
        menuName=menuData.addon_name;
      }
      cb(null,menuName+'_'+Date.now()+'.' + filetype);
    }
});
let upload = multer({storage: storage});
   

module.exports = upload.single('image');
