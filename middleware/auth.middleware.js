var jwt = require('jwt-simple');

var userService = require('../services/users.service');
const rolesConstant = require('../config/role.json');
const customerService = require("../services/customer.service");
const chefService = require("../services/chef.service");

module.exports = function(req, res, next) {
    try{
      var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['access-token'] || req.headers['access_token'];
      var key = (req.body && req.body.x_key) || (req.query && req.query.x_key) || req.headers['x-key'];
    
    
    
      if (token || key) {
        try {
          var decoded = jwt.decode(token, require('../config/secret.js')());
          console.log('decoded ', decoded)
          var userid = decoded.user_id;
          
          req.session.userid = decoded.user_id;
          if (decoded.exp <= Date.now()) {
            res.status(400);
            res.json({
              "status": 400,
              "message": "Token Expired"
            });
            return;
          }
          userService.validateUser(userid).then((dbUser) =>{
            //console.log(dbUser);
            if(!dbUser) {
              throw new Error("User not found");
            }
            req.session.userId = dbUser.user_id;
            let roleCode = decoded.role_code;
            if(roleCode == rolesConstant.ROLE_CUSTOMER) {
              customerService.fetchCustomerByUserId(dbUser.user_id).then(data => {
                if(data){
                req.session.customerId = data['cust_id'];
                }
                next();
              }).catch(err => {
                res.status(401);
                res.json({
                  "status": 401,
                  "message": err.message
                });
              })
              return;
            }
            if(roleCode == rolesConstant.ROLE_CHEF) {
              chefService.fetchChefByUserId(dbUser.user_id).then(data => {
                if(data){
                  req.session.chefId = data['chef_id'];
                }
                next();
              }).catch(err => {
                res.status(401);
                res.json({
                  "status": 401,
                  "message": err.message
                });
              });
              return;
            }
            if(roleCode == rolesConstant.ROLE_ADMIN) {
              req.session.adminId = 0;
            } else {
              next();
            }
        }).catch(err =>{
            res.status(401);
            res.json({
              "status": 401,
              "message": err
            });
            return; 
          })
        } catch (err) {
            res.status(500);
            res.json({
              "status": 500,
              "message": "Oops something went wrong",
              "error": err
            });
          }
        } else {
          throw new Error("Missing token in the header");
        }
    }
    catch(err){
      res.status(401);
        res.json({
          "status": 401,
          "message": err.message
        });
    }
};    