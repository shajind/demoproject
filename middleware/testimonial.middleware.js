const multer  = require('multer');




let storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null,'./image/testimonial');
    },
    filename: (req, file, cb) => {
      console.log("file :",file)
      let type=file.originalname.split(".")
      let filetype=type[type.length-1];
      let payload = req.body;
      let testimonialType=payload.testimonial_type;
      cb(null,testimonialType+'_'+Date.now()+'.' + filetype);
    }
});
let upload = multer({storage: storage});
   

module.exports = upload.single('image');
