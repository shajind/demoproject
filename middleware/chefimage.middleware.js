const multer  = require('multer');


// let filter = (file) =>{
//   // if(file.mimetype === 'image/gif') {
//   //   filetype = 'gif';
//   // }
//   if(file.mimetype === 'image/png') {
//     filetype = 'png';
//     return filetype
//   }
//   if(file.mimetype === 'image/jpeg') {
//     filetype = 'jpg';
//     return filetype
//   }
//   else {
//     cb("Please upload only images jpeg/png", false);
//   }
// }

let storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null,'./image/chefProfile');
    //  cb(null,'/image');
    },
    filename: (req, file, cb) => {
      console.log("file :",file)
      //let filetype=filter(file)
      let type=file.originalname.split(".")
      let filetype=type[type.length-1];
      var chefProfile = req.body;
      console.log("chefProfile middleware:",chefProfile);
      let name=chefProfile.chef_fullname;
      let userId=req.query.user_id;
      cb(null, name+'_userId_'+userId+'.' + filetype);
    }
});
let upload = multer({storage: storage});
   

module.exports = upload.single('image');
