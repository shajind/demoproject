const multer  = require('multer');

// let filter = (file) =>{
//   // if(file.mimetype === 'image/gif') {
//   //   filetype = 'gif';
//   // }
//   if(file.mimetype === 'image/png') {
//     filetype = 'png';
//     return filetype
//   }
//   if(file.mimetype === 'image/jpeg') {
//     filetype = 'jpg';
//     return filetype
//   }
//   else {
//     cb("Please upload only images jpeg/png", false);
//   }
// }

let storage = multer.diskStorage({
    destination: (req, files, cb) => {;
      cb(null,'./image/customerProfile');
    },
    filename: (req, files, cb) => {
      let userId=req.session.userId;
      let timeStamp=Date.now();
      if(files.fieldname=='profile_image'){
        let type=files.originalname.split(".")
        let filetype=type[type.length-1];      
      cb(null, 'profileImage_userId_'+userId+'_'+timeStamp+'.' + filetype);
      }
      if(files.fieldname=='profile_cover_image'){
        let type=files.originalname.split(".")
        let filetype=type[type.length-1];      
        cb(null, 'coverImage_userId_'+userId+'_'+timeStamp+'.' + filetype);
      }
    }
});


let upload = multer({storage: storage, limits: { fieldSize: 2 * 1024 * 1024 }});
module.exports = upload.fields([{ name: 'profile_image'},{ name: 'profile_cover_image'}]);
