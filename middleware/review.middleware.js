const multer  = require('multer');

let storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null,'./image/customer_review_file');
    },
    filename: (req, file, cb) => {
      console.log("file :",file)
      let type=file.originalname.split(".")
      let filetype=type[type.length-1];
      cb(null,"customer_review"+'_'+Date.now()+'.' + filetype);
    }
});

//let upload = multer({storage: storage,limits: { fieldSize: 2 * 1024 * 1024 }});

let upload = multer({storage: storage});

module.exports = upload.single('file');
