const model = require("../models");
const categoryMenuItemModel=model.category_menu_item;
const dayAvailableMenuItemModel=model.day_available_menu_item;
const weekday=['sun',"mon","tue","wed","thu","fri","sat"];

exports.getcategoryId = (filters) => {
    let whereClause = {};
    if (filters) {
    let where = {};
    let conditions = JSON.parse(filters);
    for (let i = 0; i < conditions.length; i++) {
      let field = conditions[i]["field"];
      let predicate = conditions[i]["predicate"];
      let value = conditions[i]["value"];
      if(field == 'category_ids') {
        where["category_id"] = value;
      }
    }
    whereClause["where"] = where;
    }  
    console.log(whereClause);
    return new Promise((resolve, reject) => {
      categoryMenuItemModel.findAll(whereClause).then((data) => {
        let response=categoryMenuItemData((data));
        console.log("response",response);
        resolve(response);
      })
      .catch((err) => {
        reject({ error: err });
      });
    });
};
  
  
let categoryMenuItemData = (data) => {
    let res = [];
    data.map((row) => {
        res.push(row.menu_item_id);
    });
    return res;
};



exports.getdayAvailableId = (filters) => {
    let whereClause = {};
    if (filters) {
    let where = {};
    let conditions = JSON.parse(filters);
    for (let i = 0; i < conditions.length; i++) {
      let field = conditions[i]["field"];
      let predicate = conditions[i]["predicate"];
      let value = conditions[i]["value"];
      // if(field == 'days_availables') {
      //   where["available_day"] = value;
      // }else 
      if(field == 'day_available'&&value=='today') {
        let currentDay = new Date();        
        let value = weekday[currentDay.getDay()];
        console.log(value);
        where["available_day"] = value;
      }else if(field == 'day_available'&&value == 'tomorrow') {
        let currentDay = new Date();
        let tomorrow=new Date(currentDay.setDate(currentDay.getDate() + 1));
        let value = weekday[tomorrow.getDay()];
        where["available_day"] = value;
      }
    }
    whereClause["where"] = where;
    }  
    console.log(whereClause);
    return new Promise((resolve, reject) => {
    dayAvailableMenuItemModel.findAll(whereClause).then((data) => {
      let response=categoryMenuItemData((data));
      console.log(response);
      resolve(response);
    })
    .catch((err) => {
      reject({ error: err });
    });
  });
};