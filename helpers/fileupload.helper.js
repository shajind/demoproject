const multer  = require('multer');

exports.uploadFile = function(fileType, folderName) {
    let storage = multer.diskStorage({
        destination: (req, file, cb) => {
            const dir = '/var/www/html/yummafood/'+folderName;
            fs.exists(dir, exist => {
                if (!exist) {
                    fs.mkdir(dir, error => cb(error, dir));
                };
                cb(null, dir);
            })
        },
        filename: (req, file, cb) => {
          console.log("file :",file)
          let type=file.originalname.split(".")
          let filetype=type[type.length-1];
          let name=req.body.cust_fullname;
          let userId=req.query.user_id;
          cb(null, name+'_userId_'+userId+'.' + filetype);
        }
    });
    return multer({storage: storage});
}


//UPDATE user_types SET user_type_code = "role_admin" where user_type_id=1;
//UPDATE user_types SET user_type_code ="role_chef" where user_type_id=2;
//UPDATE user_types SET user_type_code = "role_customer" where user_type_id=3;