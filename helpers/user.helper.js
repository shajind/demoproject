exports.formatRoles = (user, role_code) => {
    let role = undefined;
    let roles = user.roles;
    for(let i =0;i < roles.length;i++) {
        if(roles[i].role_code == role_code) {
            role = roles[i];
            addCurrentRole(user, roles[i]);
            break;
        }
    }
    return role;
}

function addCurrentRole(user, role) {
    user.dataValues['current_role'] = role;
    user.dataValues['roles'] = [];
}

exports.addCurrentRoleField = (user, role) => {
    addCurrentRole(user, role);
}