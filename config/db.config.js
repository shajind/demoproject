require("dotenv").config();

module.exports = {
  development: {
    DB: "yummadb",
    USER: "root",
    PASSWORD: "root",
    //PASSWORD: "1bityumma",
    //PASSWORD: "mysql",
    HOST: "localhost",
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  },

  test: {
    DB: "yummadb",
    USER: "mysql",
    PASSWORD: "root",
    HOST: "localhost",
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  },

  production: {
    DB: process.env.DB_NAME,
    USER: process.env.DB_USER,
    PASSWORD: process.env.DB_PASS,
    HOST: "localhost",
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  },
};
